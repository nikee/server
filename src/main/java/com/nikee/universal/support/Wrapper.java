package com.nikee.universal.support;

public class Wrapper<T, Y> {

    private T payload1;
    private Y payload2;

    /**
     * @return the payload1
     */
    public T getPayload1() {
        return payload1;
    }

    /**
     * @param payload1 the payload1 to set
     */
    public void setPayload1(T payload1) {
        this.payload1 = payload1;
    }

    /**
     * @return the payload2
     */
    public Y getPayload2() {
        return payload2;
    }

    /**
     * @param payload2 the payload2 to set
     */
    public void setPayload2(Y payload2) {
        this.payload2 = payload2;
    }

    /**
     * 
     */
    public Wrapper() {
    }

    /**
     * @param payload1
     * @param payload2
     */
    public Wrapper(T payload1, Y payload2) {
        this.payload1 = payload1;
        this.payload2 = payload2;
    }
}