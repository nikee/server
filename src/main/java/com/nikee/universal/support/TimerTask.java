package com.nikee.universal.support;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.nikee.universal.config.UploadSetting;
import com.nikee.universal.dao.interfaces.IAccount;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Mail;
import com.nikee.universal.service.EmailService;
import com.nikee.universal.service.interfaces.IAccountService;
import com.nikee.universal.service.interfaces.IReportService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import net.sf.jasperreports.engine.JRException;
import reactor.core.scheduler.Scheduler;

@Component
public class TimerTask {

    @Autowired
    private IReportService reportService;

    @Autowired
    private IAccountService accountService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;

    @Autowired
    private UploadSetting setting;
    
    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        return new ThreadPoolTaskScheduler();
    }

    private Account acc;

    private String format;

    public void startCron(Account acc, String format) {
        this.acc = acc;
        this.format = format;
        threadPoolTaskScheduler.schedule(new AutoExportReport(), new CronTrigger("0 0 8 1 * ?"));
    }

    private class AutoExportReport implements Runnable{
        @Override
        public void run() {
            if (acc != null) {
                Account boss = accountService.getBoss(acc);

                try {
                    reportService.getCashFlowReport(acc, format);
                    // reportService.getGstReport(acc, format);
                    System.out.println("Generated Report Successfully..!");
                } catch (FileNotFoundException | JRException e) {
                    e.printStackTrace();
                }

                File companyFolder = new File(setting.getDefReportPath() + "\\" + acc.getCompany());
                File[] attachment = companyFolder.listFiles();

                Mail mail = new Mail();
                mail.setFrom("no-reply@nikeeworld.com");
                mail.setTo(boss.getEmail());
                mail.setSubject("Monthly Reports");

                Map<String, Object> model = new HashMap<>();
                model.put("user", boss);
                model.put("signature", "no-reply@nikeeworld.com");
                mail.setModel(model);
                emailService.sendEmail(mail, "email-report-template", attachment);
                System.out.println("Email Sent Successfully..!");
            }
        }
    }
    
    // // Another way to do the Timer
    // @Scheduled(cron = "*/5 * * * * *") // interception 5s
    // public void autoExportReport() throws FileNotFoundException, JRException {

    //     System.out.println("Hello ********************************************************************");

    // }
}