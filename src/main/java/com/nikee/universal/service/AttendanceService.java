package com.nikee.universal.service;

import java.util.List;

import com.nikee.universal.dao.interfaces.IAttendance;
import com.nikee.universal.entity.Attendance;
import com.nikee.universal.service.interfaces.IAttendanceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AttendanceService implements IAttendanceService {

    @Autowired
    private IAttendance attendanceDao;
    
    @Override
    public void create(Attendance attendance) {
        attendanceDao.create(attendance);
    }

    @Override
    public boolean delete(Attendance attendance) {
        return attendanceDao.delete(attendance);
    }

    @Override
    public boolean update(Attendance attendance) {
        return attendanceDao.update(attendance);
    }

    @Override
    public Attendance find(Attendance attendance) {
        return attendanceDao.find(attendance);
    }

    @Override
    public Attendance findById(String accountId) {

        return attendanceDao.find(new Attendance(accountId));
    }

    @Override
    public List<Attendance> findAll() {
        return attendanceDao.findAll();
    }
    
}
