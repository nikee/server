package com.nikee.universal.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nikee.universal.config.UploadSetting;
import com.nikee.universal.dao.interfaces.IClient;
import com.nikee.universal.dao.interfaces.IReport;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.client.Client;
import com.nikee.universal.service.interfaces.IReportService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;

@Service
public class ReportService implements IReportService {

    @Autowired
    private IClient clientDao;
    @Autowired 
    private IReport reportDao;

    @Autowired
    private UploadSetting setting;

    /*below method is for testing purposes only*/
    @Override
    public String exportReport(String reportFormat) throws FileNotFoundException, JRException {
        List<Client> listClients = clientDao.getAllClients();

        String pathOfGeneratedReports = setting.getDefReportPath();

        // Load file and compile it
        try {
            /// TO-DO
            // check if file folder can be created programmatically
            // currently, if folder is not created beforehand "FileNotFoundException" will
            /// be thrown
            // possible solutions
            // [1]. popup file saving dialog from frontend. Get path from there
            File file = ResourceUtils.getFile("classpath:client.jrxml");
            JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(listClients);
            Map<String, Object> parameters = new HashMap<>();
            parameters.put("createdBy", "Oshan");
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
            if (reportFormat.equalsIgnoreCase("html")) {
                JasperExportManager.exportReportToHtmlFile(jasperPrint, pathOfGeneratedReports + "\\clients.html");
            }
            if (reportFormat.equalsIgnoreCase("pdf")) {
                JasperExportManager.exportReportToPdfFile(jasperPrint, pathOfGeneratedReports + "\\clients.pdf");
            }
            if (reportFormat.equalsIgnoreCase("xls"))  {
                File destFile = new File(pathOfGeneratedReports + "\\clients.xls");
                JRXlsExporter exporter = new JRXlsExporter();
                exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(destFile));
                SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
                configuration.setOnePagePerSheet(true);
                exporter.setConfiguration(configuration);
                exporter.exportReport();
            }  
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            e.toString();
        } catch (JRException e) {
            e.printStackTrace();
        }
        return "report generated in " + pathOfGeneratedReports;
    }

    @Override
    public void getGstReport(Account acc, String format)  throws FileNotFoundException, JRException {
        reportDao.getGstReport(acc ,format);

    }

    @Override
    public void getCashFlowReport(Account acc, String format) throws FileNotFoundException, JRException {
        reportDao.getCashFlowReport(acc, format);
    }
}