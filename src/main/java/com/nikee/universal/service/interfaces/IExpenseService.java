package com.nikee.universal.service.interfaces;

import java.util.List;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.account.Expenses;

public interface IExpenseService {

	public Expenses add(Account acc, Expenses expense);

	public List<Expenses> findAllExpenses(Account acc);


    
}