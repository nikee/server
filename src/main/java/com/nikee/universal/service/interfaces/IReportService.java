package com.nikee.universal.service.interfaces;

import java.io.FileNotFoundException;

import com.nikee.universal.entity.Account;

import net.sf.jasperreports.engine.JRException;

public interface IReportService {
    
    public String exportReport(String format) throws FileNotFoundException, JRException;

	public void getGstReport(Account acc, String format) throws FileNotFoundException, JRException;

	public void getCashFlowReport(Account acc, String format) throws FileNotFoundException, JRException;

}