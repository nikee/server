package com.nikee.universal.service.interfaces;

import java.util.List;

import com.nikee.universal.entity.Access;

public interface IAccessService {
    
    public void create(Access access);

	public boolean delete(Access access);

    public boolean update(Access access);
    
	public Access find(Access access);

	public List<Access> findAll();
	
	/**
     * @param accountId: the account id
     * @param accessName: the name of the module to access, e.g., job, stock
     */
	public Integer identifyAccess(String accountId, String accessName);
    
}
