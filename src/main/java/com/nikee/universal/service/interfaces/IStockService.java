package com.nikee.universal.service.interfaces;

import java.util.List;

import com.nikee.universal.entity.Stock;

public interface IStockService {
    public void createStock(Stock stock);

	public Stock findStock(Stock stock);

	public boolean updateStock(Stock stock);

	public List<Stock> findAllStock();

	public boolean expireStock(Stock stock);

	public boolean cleanStock(Stock stock);
}
