package com.nikee.universal.service.interfaces;

import java.util.List;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.employee.Employee;

public interface IEmployeeService {

	public Employee registerNew(Account acc, Employee employee);

	public Employee findEmployee(Account acc, String empId);

	public boolean update(Employee emp);

	public List<Employee> findAllEmployees(Account acc);

	public boolean deleteEmployee(Employee original);
    
}