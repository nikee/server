package com.nikee.universal.service.interfaces;

import java.util.List;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Services;
import com.nikee.universal.errors.TransactionFailureException;

public interface IServiceService {

    public Services getService(String id);

    /**
     * Create a service as a child to another service
     * 
     * @param toBeCreated
     * @param parent
     * @param who         MUST ensure this account is a t2 account.
     * @return
     */
    public Services create(Services toBeCreated, String parentId, Account t2);

    public Services createRoot(Services toBeCreated, Account t2);

    public Services whosYourDaddy(String id);

    public List<Services> findByName(Account owner, String name);

    public List<Services> findByOwner(Account owner);

    public void cut(Services service) throws TransactionFailureException;

    public void deleteBranchSinceNode(Services node) throws TransactionFailureException;

    public void insert(Services original, Services toBeInserted);

    public Services update(Services _update);

    public void setRoot(Services service);

}