package com.nikee.universal.service.interfaces;

import java.util.List;

import com.nikee.universal.entity.Job;

public interface IJobService {
    public void postJob(Job job);

	public Job findJob(Job job);

	public boolean updateJob(Job job);

	public List<Job> findAllJob();

	public boolean discardJob(Job job);

}
