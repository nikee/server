package com.nikee.universal.service.interfaces;

import java.util.List;

import com.nikee.universal.entity.Attendance;

public interface IAttendanceService {
    
    public void create(Attendance attendance);

	public boolean delete(Attendance attendance);

	public boolean update(Attendance attendance);

    public Attendance find(Attendance attendance);
    
    public Attendance findById(String accountId);

    public List<Attendance> findAll();
}
