package com.nikee.universal.service.interfaces;

import java.util.List;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.leave.Leave;

public interface ILeaveService {

	public Leave addLeave(Account acc, Leave leave);

	public Leave findLeave(Leave leave);

	public boolean update(Leave leave);

	public List<Leave> findAllLeaves(String empId);

	public boolean grantLeave(Leave leave);
    
}