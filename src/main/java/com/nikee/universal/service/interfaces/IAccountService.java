package com.nikee.universal.service.interfaces;

import java.util.List;
import java.util.Map;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Company;
import com.nikee.universal.entity.account.CalendarEvent;

import org.springframework.dao.DuplicateKeyException;

public interface IAccountService {

    /***
     * 
     * @param email
     * @param password
     * @return
     */
    public Map<String, Object> login(String email, String password);

    public String test(Account account);

    public Account findByEmail(String email);

    /**
     * Allow to change password
     * 
     * @param newPwd
     * @param account
     * @return True if the change has been acknoledged
     */
    public boolean changePassword(String newPwd, Account account);

    public List<Account> findAllEmployeeByBoss(Account boss);

    public Account updateProfile(Account modified, Account source);

    public Account createT2Account(Account toBeCreated, Company company) throws DuplicateKeyException;

    public Account createT3Account(Account accountToCreate, Account boss) throws DuplicateKeyException;

    public Account createT4Account(Account accountToCreate, Account operator) throws DuplicateKeyException;

    /**
     * allow T2 to update their company data.
     * 
     * @param account
     * @param company
     * @return
     */
    public Account updateCompany(Account account, Company company);

    public void transferOwnership(Account from, Account to);

    public List<Account> findAllEmployeeByBossEmail(String email);

    public Account getBoss(Account requestor);

    public boolean addNewCalendarEvent(CalendarEvent calendarEvent, Account acc);

	public boolean updateCalendar(CalendarEvent updatedcalendarEvent);

}