package com.nikee.universal.service.interfaces;

import java.util.List;

import com.mongodb.client.result.DeleteResult;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.contract.Deal;

public interface IContractService {

    public Deal findById(String id);

    public List<Deal> findByClient(String id);

    public List<Deal> findByService(String id);

    public List<Deal> findByBusiness(Account boss);

    public Deal prepareToSave(Deal uploaded, Deal inDB, Account byWho);

    public Deal create(Deal deal, Account who);

    /**
     * check the createdBy field in controller. That field will not be validated in
     * this service
     * 
     * @param deal
     * @param byWho
     * @return
     */
    public Deal save(Deal deal, Account byWho);

    public DeleteResult remove(Deal deal);

    /**
     * validate if the critical information in deal is valid
     * 
     * @param deal
     * @param who  the account that requested
     * @return true if is valid
     */
    public boolean validate(Deal deal, Account who);

}