package com.nikee.universal.service.interfaces;

import java.time.ZonedDateTime;
import java.util.List;

import com.mongodb.lang.Nullable;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.client.Action;
import com.nikee.universal.entity.client.Client;
import com.nikee.universal.entity.client.Note;

public interface IClientService {

        public Client registerNew(Account operator, Client clientData);

        public List<Client> findByBusiness(Account boss);

        public List<Client> findClients(Account operator, @Nullable String fn, @Nullable String ln,
                        @Nullable String email, @Nullable String phone);

        /***
         * find client by the given Client ID
         * 
         * @param operator check if the result matches your business owner id
         * @param id
         * @return
         */
        public Client findClient(Account operator, String id);

        /**
         * 
         * @param client
         * @param payload
         * @param byWho
         * @return
         */
        public boolean addNewNote(Client client, String payload, Account byWho);

        /**
         * Only call this when the requester is identified as T2 or T3
         * 
         * @param client
         * @param idToDelete
         * @return
         */
        public boolean deleteNote(Client client, String idToDelete);

        /**
         * 
         * @param client
         * @param toModify
         * @param payload
         * @param who
         * @return
         */
        public boolean updateNote(Client client, Note toModify, String payload, Account who);

        /**
         * do not change the business owner
         * 
         * @deprecated in favor of updateBasic
         * @param toSave
         * @return
         */
        public Client updateClient(Client toSave);

        /**
         * Update firstName, lastName, email and phone of document identified by
         * client.id
         * 
         * @param toSave
         * @return
         */
        public boolean updateBasic(Client toSave);

        public boolean tickAction(Account byWho, Client whichClient, String actionId);

        public boolean unTickAction(Account byWho, Client whichClient, String actionId);

        public boolean addAction(Client whichClient, String desc, List<Account> assigned, Account who,
                        ZonedDateTime deadline);

        public boolean assignStaffToAction(Client whichClient, String actionId, Account whoToAssign);

        // public Client changeActionDesc(Client whichClient, int actionIndex, String
        // newDesc);

        public boolean updateAction(Client whichClient, Action newAction);// only update the field
                                                                          // wanted

	

}