package com.nikee.universal.service;

import java.util.List;

import com.nikee.universal.dao.interfaces.IAccess;
import com.nikee.universal.entity.Access;
import com.nikee.universal.service.interfaces.IAccessService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccessService implements IAccessService {

    @Autowired
    private IAccess accessDao;
    
    @Override
    public void create(Access access) {
        accessDao.create(access);
    }

    @Override
    public boolean delete(Access access) {
        return accessDao.delete(access);
    }

    @Override
    public boolean update(Access access) {
        return accessDao.update(access);
    }

    @Override
    public Access find(Access access) {
        return accessDao.find(access);
    }

    @Override
    public List<Access> findAll() {
        return accessDao.findAll();
    }

    @Override
    public Integer identifyAccess(String accountId, String accessName) {
        Access access = accessDao.find(new Access(accountId, null));
        if(access.getAccess().get(accessName) == null)
            return 0;
        else
            return access.getAccess().get(accessName);
    }
    
}
