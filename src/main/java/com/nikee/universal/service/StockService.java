package com.nikee.universal.service;

import java.util.List;

import com.nikee.universal.dao.interfaces.IStock;
import com.nikee.universal.entity.Stock;
import com.nikee.universal.service.interfaces.IStockService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockService implements IStockService {

    @Autowired
    private IStock stockDao;
    
    @Override
    public void createStock(Stock stock) {
        stockDao.createStock(stock);
    }

    @Override
    public Stock findStock(Stock stock) {
        return stockDao.findStock(stock);
    }

    @Override
    public boolean updateStock(Stock stock) {
        return stockDao.updateStock(stock);
    }

    @Override
    public List<Stock> findAllStock() {
        return stockDao.findAllStock();
    }

    @Override
    public boolean expireStock(Stock stock) {
        return stockDao.expireStock(stock);
    }
    
    @Override
    public boolean cleanStock(Stock stock) {
        return stockDao.cleanStock(stock);
    }
}
