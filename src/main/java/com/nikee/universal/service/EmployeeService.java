package com.nikee.universal.service;

import java.util.List;
import java.util.stream.Collectors;

import com.nikee.universal.dao.interfaces.IEmployee;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.employee.Employee;
import com.nikee.universal.service.interfaces.IEmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService implements IEmployeeService {

    @Autowired
    private IEmployee employeeDao;
    
    @Override
    public Employee registerNew(Account acc, Employee employee) {
        
        return employeeDao.registerNew(acc, employee);   
    }

    @Override
    public Employee findEmployee(Account acc, String empId) {
        List<Employee> list = this.employeeDao.findEmployeeByCompanyId(acc.getCompany());
        list = list.stream().filter(x->x.getCompany().equals(acc.getCompany()))
                .filter(y->y.getId().equalsIgnoreCase(empId)).collect(Collectors.toList());
        list = list.stream().filter(y->y.getId().equalsIgnoreCase(empId)).collect(Collectors.toList());
        if(list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    @Override
    public boolean update(Employee emp) {
        return this.employeeDao.update(emp);
    }

    @Override
    public List<Employee> findAllEmployees(Account acc) {
        
        return this.employeeDao.findAllEmployees(acc);
    }

    @Override
    public boolean deleteEmployee(Employee employee) {
        return this.employeeDao.deleteEmployee(employee);
    }
}