package com.nikee.universal.service;

import java.util.ArrayList;
import java.util.List;

import com.nikee.universal.dao.CompanyDAO;
import com.nikee.universal.dao.interfaces.*;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Company;
import com.nikee.universal.entity.Services;
import com.nikee.universal.entity.account.Tax;
import com.nikee.universal.errors.TransactionFailureException;
import com.nikee.universal.service.interfaces.IServiceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceService implements IServiceService {

    @Autowired
    private IService serviceDao;

    @Autowired
    private IAccount accountDao;
    @Autowired
    private CompanyDAO companyDAO;

    @Override
    public Services create(Services toBeCreated, String parentId, Account t2) {
        Services _s = toBeCreated;
        Company c = companyDAO.find(t2.getCompany());
        _s.setCompanyId(t2.getCompany());
        _s.setId(null);
        _s.setSubService(null);
        if (c != null && toBeCreated.getAppliedTax() == null) {
            Tax tax = c.getTax();
            if (tax != null) {
                _s.setAppliedTax(tax);
            }
        }
        return this.serviceDao.Amend(_s, parentId);
    }

    @Override
    public Services createRoot(Services toBeCreated, Account t2) {
        Services _s = toBeCreated;
        _s.setCompanyId(t2.getCompany());
        _s.setId(null);
        _s.setSubService(null);
        // if company exist & no tax policy in input
        if (toBeCreated.getAppliedTax() == null) {
            Tax tax = companyDAO.find(t2.getCompany()).getTax();
            if (tax != null) {
                _s.setAppliedTax(tax);
            }
        }
        return this.serviceDao.Amend(_s);
    }

    @Override
    public Services whosYourDaddy(String id) {
        return this.serviceDao.whosYourDaddy(id);
    }

    @Override
    public List<Services> findByName(Account owner, String name) {

        if (owner == null) { // if the owner is null, then just return an empty list
            List<Services> result = new ArrayList<>();
            return result;
        }
        if (name == null) { // if the owner is not null but the service name is null, then look for all
                            // services under the same owner name
            return this.findByOwner(owner);
        }

        return this.serviceDao.findByName(owner, name);
    }

    @Override
    public List<Services> findByOwner(Account owner) {
        Account acc = this.accountDao.findFirstAccountByEmail(owner.getEmail());
        return this.serviceDao.findAllByOwner(acc);
    }

    @Override
    public void cut(Services service) throws TransactionFailureException {
        this.serviceDao.cut(service);
    }

    @Override
    public void deleteBranchSinceNode(Services node) throws TransactionFailureException {
        this.serviceDao.delete(node);

    }

    @Override
    public Services getService(String id) {
        return this.serviceDao.getInstance(id);
    }

    @Override
    public void insert(Services original, Services toBeInserted) {
        this.serviceDao.insert(original, toBeInserted);

    }

    @Override
    public Services update(Services _update) {
        return this.serviceDao.update(_update);

    }

    @Override
    public void setRoot(Services service) {
        this.serviceDao.setRoot(service);

    }

}