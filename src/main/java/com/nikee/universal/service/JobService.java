package com.nikee.universal.service;

import java.util.List;

import com.nikee.universal.dao.interfaces.IJob;
import com.nikee.universal.entity.Job;
import com.nikee.universal.service.interfaces.IJobService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JobService implements IJobService {

    @Autowired
    private IJob jobDao;

    @Override
    public void postJob(Job job) {
        jobDao.postJob(job);
    }

    @Override
    public Job findJob(Job job) {
        return jobDao.findJob(job);
    }

    @Override
    public boolean updateJob(Job job) {
        return jobDao.updateJob(job);
    }

    @Override
    public List<Job> findAllJob() {
        return jobDao.findAllJob();
    }

    @Override
    public boolean discardJob(Job job) {
        return jobDao.discardJob(job);
    }
    
}
