package com.nikee.universal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;

import com.nikee.universal.entity.Mail;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private SpringTemplateEngine templateEngine;

    public final static String FORGET_PASSWORD = "email-forgetPwd-template";
    public final static String TRANSFER_OWNERSHIP = "email-ownership-template";
    public final static String INVITATION = "email-invitation-template";

    public void sendEmail(Mail mail) {
        this.sendEmail(mail, FORGET_PASSWORD);
    }

    /* Wenzel */
    public void sendEmail(Mail mail, String template) {
        this.sendEmail(mail, template, null);
    }

    /**
     * 
     * @param mail
     * @param template check static field for possible templates
     * @param attachmentList list of attachment
     */
    public void sendEmail(Mail mail, String template, File[] attachmentList) {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            Context context = new Context();
            context.setVariables(mail.getModel());
            String html = templateEngine.process(template, context);

            helper.setTo(mail.getTo());
            helper.setText(html, true);
            helper.setSubject(mail.getSubject());
            helper.setFrom(mail.getFrom());

            /* Wenzel */
            if (attachmentList != null) {
                for (File attachment: attachmentList) {
                    helper.addAttachment(attachment.getName(), attachment);
                }
            }

            emailSender.send(message);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

}