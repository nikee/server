package com.nikee.universal.service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.nikee.universal.dao.interfaces.IClient;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.client.Action;
import com.nikee.universal.entity.client.Client;
import com.nikee.universal.entity.client.Note;
import com.nikee.universal.service.interfaces.IClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientService implements IClientService {

    @Autowired
    private IClient clientDao;
    Logger logger = LoggerFactory.getLogger(ClientService.class);

    @Override
    public Client registerNew(Account operator, Client clientData) {
        Client client = clientData;
        client.markTimeStamp().setCompanyId(operator.getCompany());
        client.setInCharge(operator.getId());
        return this.clientDao.create(client);
    }

    @Override
    public List<Client> findByBusiness(Account boss) {
        return this.clientDao.findByCompanyID(boss.getCompany());
    }

    @Override
    public List<Client> findClients(Account operator, String fn, String ln, String email, String phone) {
        List<Client> list = this.clientDao.findByCompanyID(operator.getCompany());
        Stream<Client> stream = list.stream();
        if (fn != null)
            stream = stream.filter(c -> c.getFirstName().equalsIgnoreCase(fn.trim()));
        if (ln != null)
            stream = stream.filter(c -> c.getLastName().equalsIgnoreCase(ln.trim()));
        if (email != null)
            stream = stream.filter(c -> c.getEmail().equalsIgnoreCase(email.trim()));
        if (phone != null)
            stream = stream.filter(c -> c.getPhone().equalsIgnoreCase(phone.trim()));
        return stream.collect(Collectors.toList());
    }

    /**
     * look for a paticualr client ID if it is inside the domain of requestor
     */
    @Override
    public Client findClient(Account operator, String id) {
        logger.debug("Look up Client ID " + id);
        List<Client> list = this.clientDao.findByCompanyID(operator.getCompany());
        list = list.stream().filter(c -> c.getCompanyId().equals(operator.getCompany()))
                .filter(c -> c.getId().equalsIgnoreCase(id)).collect(Collectors.toList());
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    @Override
    public boolean addNewNote(Client client, String payload, Account byWho) {
        /*
         * set up createdAt, modifiedBy and modifiedAt field ID field will be taken care
         * of in DAO
         */
        ZonedDateTime now = ZonedDateTime.now();
        Note note = new Note(now, now, byWho.getId(), payload);
        return this.clientDao.addNote(client, note);
    }

    @Override
    public boolean deleteNote(Client client, String idToDelete) {
        return this.clientDao.removeNote(client, idToDelete);

    }

    @Override
    public boolean updateNote(Client client, Note toModify, String payload, Account who) {
        Note note = toModify;
        note.setLastModifiedAt(ZonedDateTime.now());
        note.setLastModifiedBy(who.getId());
        note.setSummary(payload);
        return this.clientDao.updateNote(client, note);
    }

    @Override
    public Client updateClient(Client toSave) {
        return this.clientDao.update(toSave);
    }

    @Override
    public boolean updateBasic(Client toSave) {
        return this.updateBasic(toSave);
    }

    @Override
    public boolean tickAction(Account byWho, Client whichClient, String actionId) {
        return this.clientDao.tickAction(whichClient, actionId, byWho);
    }

    @Override
    public boolean unTickAction(Account byWho, Client whichClient, String actionId) {
        return this.clientDao.unTickAction(whichClient, actionId, byWho);
    }

    @Override
    public boolean addAction(Client whichClient, String desc, List<Account> assigned, Account who,
            ZonedDateTime deadline) {
        return this.clientDao.addAction(whichClient, assigned, deadline, who, desc);
    }

    @Override
    public boolean assignStaffToAction(Client whichClient, String actionId, Account whoToAssign) {
        return this.clientDao.assignStaffToAction(whichClient, actionId, whoToAssign);
    }

    @Override
    public boolean updateAction(Client whichClient, Action newAction) {
        return this.clientDao.updateAction(whichClient, newAction);
    }


    

}
