package com.nikee.universal.service;

import java.time.ZonedDateTime;

import com.nikee.universal.dao.interfaces.IAccount;
import com.nikee.universal.dao.interfaces.ILicense;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.license.Extension;
import com.nikee.universal.entity.license.License;
import com.nikee.universal.service.interfaces.IAccountService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LicenseService {
    Logger logger = LoggerFactory.getLogger(LicenseService.class);
    @Autowired
    private ILicense licenseDAO;
    @Autowired
    private IAccountService accountService;

    /**
     * Extend the license of the requested account's company to the specified
     * Expiration date, no matter if there is a previous record or not
     * 
     * @param companyID The company ID
     * @param extendTo  New expiration Date
     * @return updated License
     */
    public License purchase(String companyID, ZonedDateTime extendTo) throws IllegalArgumentException {
        License license = this.licenseDAO.findByCompany(companyID);
        // if license does not exist
        if (license == null) {
            license = new License();
            license.setGrantTo(companyID);
            license.setExpireDateTime(ZonedDateTime.now());
        }
        if (extendTo.isBefore(license.getExpireDateTime())) // if the new extension date is backward(x days before the
                                                            // orignal date)
        {
            throw new IllegalArgumentException("The input expiration date should be later than the original one.");
        }
        license.setExpireDateTime(extendTo);
        license.getPurchaseHistory().add(new Extension(extendTo, ZonedDateTime.now()));
        logger.info("Extending license of UID " + companyID + " to " + extendTo.toString());
        return this.licenseDAO.save(license);
    }

    /**
     * Find the license record of the company you are currently in
     * 
     * @param me who is asking, can be T2 to T4
     * @return
     */
    public License myLicense(Account me) {
        Account boss = this.accountService.getBoss(me);
        return this.licenseDAO.findByCompany(boss.getCompany());
    }

    /**
     * check if the current account has a valid license attached.
     * 
     * @param me
     * @return True if the account is clear
     */
    public boolean validate(Account me) {
        License license = this.myLicense(me);
        if (license == null)
            return false;
        return license.isExpired();
    }

}