package com.nikee.universal.service;

import java.time.ZonedDateTime;
import java.util.List;

import com.nikee.universal.dao.interfaces.IExpense;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.account.Expenses;
import com.nikee.universal.service.interfaces.IExpenseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExpenseService implements IExpenseService {

    @Autowired
    private IExpense expenseDao;
    private Expenses copy;

    @Override
    public Expenses add(final Account acc, final Expenses expense) {
        this.copy = expense;
        copy.setId(null);
        copy.setDate(ZonedDateTime.now());
        copy.setEnteredBy(acc.getId());
        return expenseDao.addExpense(copy);
    }

    @Override
    public List<Expenses> findAllExpenses(Account acc) {
        return expenseDao.findAllExpenses(acc);
    }

    
    
}