package com.nikee.universal.service;

import java.time.ZonedDateTime;

import com.nikee.universal.dao.AccountImpl;
import com.nikee.universal.dao.MessagerDAO;
import com.nikee.universal.dao.interfaces.IAccount;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.account.Message.Envelope;
import com.nikee.universal.entity.account.Message.MailList;
import com.nikee.universal.entity.account.Message.Msg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MessagerService {

    @Autowired
    private MessagerDAO dao;
    @Autowired
    private IAccount accountDao;

    /**
     * Step 2 check if the sender has the right to do send. When sending private,
     * check "to" field; when sending public, check "domain". Set domain when
     * appliable
     * 
     * @param e
     * @return
     */
    public boolean validateScope(Envelope e) {
        if (e.getFrom() == null)
            return false;
        Account from = accountDao.findById(e.getFrom());
        if (from == null)
            return false;
        if (e.getFlag() == Envelope.PRIVATE) {
            if (e.getTo() == null)
                return false;
            else {
                Account to = accountDao.findById(e.getTo());
                if (to == null)
                    return false;
                if (from.getLevel() == 1) {
                    e.getBody().setScope(to.getCompany());
                    return true;
                }
                if (from.getCompany() == null)
                    return false;
                if (!from.getCompany().equalsIgnoreCase(to.getCompany())) { // common user cannot reply to admin(who
                                                                            // does not have a company)
                    e.setDomain(from.getCompany());
                    return false;
                } else {
                    e.getBody().setScope(from.getCompany());
                    return true;
                }
            }
        } else if (e.getFlag() == Envelope.PUBLIC) {
            if (e.getDomain() == null)
                return false;
            else {
                if (from.getLevel() == 1) // allow sending as admin
                {
                    e.getBody().setScope(e.getDomain());
                    return true;
                }

                if (e.getDomain().equalsIgnoreCase(from.getCompany())) // otherwise requires the same company ID
                {
                    e.getBody().setScope(e.getDomain());
                    return true;
                } else
                    return false;
            }
        } else if (e.getFlag() == Envelope.GLOBAL) {
            if (from.getLevel() != 1) // only allow sending global msg as admin
                return false;
            else {
                e.getBody().setScope(null);
                return true;
            }

        } else
            return false;
    }

    /**
     * Step 1 populate meta data. Set id, when, unread flag, and set flag to private
     * 
     * @param e
     * @param to
     * @return
     */
    public Envelope preparePrivate(Envelope e, String to) {
        e.setId(null);
        e.setWhen(ZonedDateTime.now());
        e.setUnread(true);
        e.setPrivate(to);
        return e;
    }

    public Envelope preparePublic(Envelope e, String domain) {
        e.setId(null);
        e.setWhen(ZonedDateTime.now());
        e.setUnread(true);
        e.setPublic(domain);
        return e;
    }

    public Envelope prepareGlobal(Envelope e) {
        e.setId(null);
        e.setWhen(ZonedDateTime.now());
        e.setUnread(true);
        e.setGlobal();
        return e;
    }

    /**
     * Step 3 Save Msg and write Msg ID into envelope, inform inbox
     * 
     * @param e
     * @return
     */
    public Envelope persist(Envelope e) {
        Msg saved = dao.save(e.getBody());
        e.setBodyId(saved.getId());
        Envelope en = dao.send(e);
        dao.addToInbox(en);
        return en;
    }

}