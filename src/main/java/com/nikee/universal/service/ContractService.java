package com.nikee.universal.service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import com.mongodb.client.result.DeleteResult;
import com.nikee.universal.dao.CompanyDAO;
import com.nikee.universal.dao.interfaces.IContract;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Company;
import com.nikee.universal.entity.Services;
import com.nikee.universal.entity.account.Tax;
import com.nikee.universal.entity.client.Client;
import com.nikee.universal.entity.contract.Deal;
import com.nikee.universal.entity.contract.Installment;
import com.nikee.universal.entity.contract.Refund;
import com.nikee.universal.service.interfaces.IAccountService;
import com.nikee.universal.service.interfaces.IClientService;
import com.nikee.universal.service.interfaces.IContractService;
import com.nikee.universal.service.interfaces.IServiceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContractService implements IContractService {
    Logger logger = LoggerFactory.getLogger(ContractService.class);

    @Autowired
    private IContract contractDao;
    @Autowired
    private IServiceService serviceService;
    @Autowired
    private IAccountService accountService;
    @Autowired
    private IClientService clientService;
    @Autowired
    private CompanyDAO companyDAO;

    @Override
    public Deal findById(String id) {
        return this.contractDao.findById(id);
    }

    @Override
    public List<Deal> findByClient(String id) {
        return this.contractDao.findByClient(id);
    }

    @Override
    public List<Deal> findByService(String id) {
        return this.contractDao.findByService(id);
    }

    @Override
    public Deal save(Deal deal, Account byWho) {
        return this.contractDao.save(mark(deal, byWho.getId()));
    }

    @Override
    public Deal prepareToSave(Deal uploaded, Deal inDB, Account byWho) {
        Deal upload = uploaded;
        Deal goal = inDB;
        upload.setCreatedBy(goal.getCreatedBy());
        upload.setIssuedAt(goal.getIssuedAt());
        // process Refund
        if (upload.getRefund() != null) {
            // if there was no refund before -> then create
            if (goal.getRefund() == null) {
                upload.initRefund(byWho.getId());
            }
            // if there has been a refund -> alter
            else {
                // overrdde createdBy/At -> never allow to change create meta info
                upload.getRefund().setCreatedAt(goal.getRefund().getCreatedAt());
                upload.getRefund().setCreatedBy(goal.getRefund().getCreatedBy());
                // if the two refund are the same ignoring last modification info
                if (!upload.getRefund().equalsIgnoreLM(goal.getRefund())) {
                    // not the same
                    // mark last modified info
                    upload.getRefund().setLastModifiedAt(ZonedDateTime.now());
                    upload.getRefund().setLastModifiedBy(byWho.getId());
                    // if submitted refund is paid while the previous record is pending
                    if (upload.getRefund().isPaid() == Refund.PAID && goal.getRefund().isPaid() == Refund.PENDING) {
                        // process payment date
                        ZonedDateTime paymentDate = upload.getRefund().getPaymentDate() == null ? ZonedDateTime.now()
                                : upload.getRefund().getPaymentDate();
                        upload.doRefund(byWho.getId(), paymentDate);
                    }

                } else
                // only the last modification info is changed, should be ignored.
                {
                    // undo the change in submission, orginal document will be unchanged.
                    upload.getRefund().setLastModifiedAt(goal.getRefund().getLastModifiedAt());
                    upload.getRefund().setLastModifiedBy(goal.getRefund().getLastModifiedBy());
                }

            }

        }

        return upload;
    }

    /**
     * Mark input deal's lastModifiedBy as input WHO, lastmodifiedAt as now
     * 
     * @param deal
     * @param who
     * @return generated new Deal instance
     */
    private Deal mark(Deal deal, String who) {
        Deal d = deal;
        d.setLastModifiedBy(who);
        ZonedDateTime zdt = ZonedDateTime.now();
        d.setLastModifiedAt(zdt);
        return d;
    }

    @Override
    public DeleteResult remove(Deal deal) {
        return this.contractDao.remove(deal);
    }

    @Override
    public Deal create(Deal deal, Account who) {
        Deal copy = deal;
        ZonedDateTime zdt = ZonedDateTime.now();
        List<Installment> list = copy.getInstallments();
        list = list.stream().map(i -> {
            i.setConfirmedBy(null);
            i.setPaymentDate(null);
            return i;
        }).collect(Collectors.toList());
        copy.setInstallments(list);
        copy.setCreatedBy(who.getId());
        copy.setLastModifiedBy(who.getId());
        copy.setIssuedAt(zdt);
        copy.setLastModifiedAt(zdt);
        Services _service = this.serviceService.getService(deal.getService());
        Tax tax = deal.getAppliedTax();
        if (deal.getAppliedTax() == null) // use parent data only tax is not provided
        {
            if (_service != null && _service.getAppliedTax() != null) { // if tax exists in service definition
                tax = _service.getAppliedTax();
            } else {
                Account boss = this.accountService.getBoss(who);
                Company c = companyDAO.find(boss.getCompany());
                tax = c.getTax();
            }
        }
        copy.setAppliedTax(tax);
        return this.contractDao.create(copy);
    }

    @Override
    public List<Deal> findByBusiness(Account boss) {
        return this.contractDao.findByBusiness(boss);
    }

    @Override
    public boolean validate(Deal deal, Account who) {
        // must provide clientID and serviceID
        if (deal.getClient() == null || deal.getService() == null)
            return false;
        logger.debug("provide clientID and serviceID");
        Services service = this.serviceService.getService(deal.getService());
        //
        if (service == null)
            return false;
        logger.debug("service must exist");
        // client must exist
        Client client = this.clientService.findClient(who, deal.getClient());
        if (client == null)
            return false;
        else
            logger.debug(client.getId());
        logger.debug("client must exist");
        Account boss = this.accountService.getBoss(who);
        // service must share the same boss info with account
        if (!service.getCompanyId().equals(boss.getCompany()))
            return false;
        logger.debug("service must share the same company info with account");
        // get the business owner ID of client and compare with boss
        if (!client.getCompanyId().equals(boss.getCompany()))
            return false;
        logger.debug(" get the business owner ID of client and compare with boss");
        return deal.isBalanced();
    }

}