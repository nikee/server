package com.nikee.universal.service;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mongodb.client.result.UpdateResult;
import com.nikee.universal.dao.CompanyDAO;
import com.nikee.universal.dao.interfaces.IAccount;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Company;
import com.nikee.universal.entity.account.CalendarEvent;
import com.nikee.universal.errors.AccountValidationError;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.service.interfaces.IAccountService;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

@Service
public class AccountService implements IAccountService {
    @Autowired
    IAccount accountDAO;
    Logger logger = LoggerFactory.getLogger(AccountService.class);
    @Autowired
    private CompanyDAO companyDAO;
    @Autowired
    private LicenseService licenseService;

    @Override
    public Map<String, Object> login(String email, String password) {
        Map<String, Object> result = new HashMap<>(2);
        try {
            emailValidation(email);
            List<Account> list = this.accountDAO.findAccountByEmail(email);
            if (list == null || list.size() == 0) {
                result.put("status_code", genericError.USER_NOT_FOUND);
                result.put("payload", null);
                return result;
            }
            Account user = list.get(0);
            if (Account.validatePassword(user.getPassword(), password)) {
                result.put("status_code", 200);
                result.put("payload", user);
                return result;
            } else {
                result.put("status_code", genericError.INCORRECT_PASSWORD);
                result.put("payload", null);
                return result;
            }
        } catch (genericError e) {
            result.put("status_code", e.getErrorCode());
            result.put("payload", null);
            return result;
        }
    }

    @Override
    public String test(final Account account) {
        Account temp = account;
        temp.setPassword(Account.hashPassword(account.getPassword()));
        // account.setCompanyBody(companyDAO.create(account.getCompanyBody()));
        // account.setCompany(account.getCompanyBody().getId());
        return this.accountDAO.createAccountTest(temp);
    }

    /**
     * validate the format of given email string
     * 
     * @param email
     * @throws AccountValidationError if the string failed to match regex
     *                                string(being illegel)
     */
    private void emailValidation(final String email) throws AccountValidationError {
        final String regex = "^[a-zA-Z0-9_!#$%&'*+=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(email);
        if (!matcher.matches())
            throw new AccountValidationError("The email is not in a valid format", genericError.INVALID_EMAIL);
    }

    /**
     * Update the company the person who is requesting belongs to. Authenticate and
     * authorize before calling this method!
     * 
     * @param company
     * @param who     the person's companyID will be used
     * @return
     */
    public Company updateCompany(Company company, Account who) {
        Company c = company;
        c.setId(who.getCompany());
        return this.companyDAO.save(c);
    }

    @Override
    public boolean changePassword(String newPwd, Account account) {
        UpdateResult result = this.accountDAO.changePassword(account.getEmail(), Account.hashPassword(newPwd));
        return result.wasAcknowledged();
    }

    @Override
    public Account updateProfile(Account modified, Account source) {
        Account dummy = modified;
        if (modified.getFirstName() == null)
            dummy.setFirstName(source.getFirstName());
        if (modified.getLastName() == null)
            dummy.setLastName(source.getLastName());
        if (modified.getMobile() == null)
            dummy.setMobile(source.getMobile());
        if (modified.getSecondaryEmail() == null)
            dummy.setSecondaryEmail(source.getSecondaryEmail());
        if (modified.getSecondaryContactNumber() == null)
            dummy.setSecondaryContactNumber(source.getSecondaryContactNumber());
        dummy.setEmail(source.getEmail()).setLevel(source.getLevel()).setId(source.getId());
        dummy.setCompany(source.getCompany());
        if (source.getLevel() != 2) {
            if (source.getLevel() > 2)
                dummy.setEmployer(source.getEmployer());
        } else {
            dummy.setEmployer(null);
        }
        dummy.setPassword(source.getPassword());
        dummy.setDiscarded(false);
        return this.accountDAO.updateAccount(dummy);
    }

    @Override
    public Account createT2Account(Account toBeCreated, Company company) throws DuplicateKeyException {
        Company saved = this.companyDAO.create(company);

        Account acc = this.accountDAO.createAccount(toBeCreated.setCompany(saved.getId()).setLevel(2)
                .setDiscarded(false).setLevel(2).setEmployer(null).setCompanyBody(saved));
        licenseService.purchase(saved.getId(), ZonedDateTime.now().plusDays(15));
        return acc;
    }

    @Override
    public Account createT4Account(Account toBeCreated, Account operator) throws DuplicateKeyException {
        Account employer = operator;
        logger.debug("Creating T4 Account, operater level is " + operator.getLevel());
        if (operator.getLevel() == 3)
            employer = this.accountDAO.findFirstAccountByEmail(operator.getEmployer().getEmail()); // if the caller is a
                                                                                                   // t3 account,
        // then find his boss
        logger.debug("employer is " + employer.getEmail());
        return this.accountDAO.createAccount(
                toBeCreated.setEmployer(employer).setCompany(operator.getCompany()).setLevel(4).setDiscarded(false));
    }

    @Override
    /**
     * Set Level, employer, company and discarded fields.
     */
    public Account createT3Account(Account accountToCreate, Account boss) throws DuplicateKeyException {
        return this.accountDAO.createAccount(accountToCreate.setLevel(3).setEmployer(boss) // set employer
                .setCompany(boss.getCompany()) // t3 shall not have company
                .setDiscarded(false)); // not discarded by default);
    }

    @Override
    public Account updateCompany(Account account, Company company) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Account findByEmail(String email) {

        return this.accountDAO.findFirstAccountByEmail(email);
    }

    @Override
    public void transferOwnership(Account from, Account to) {
        UpdateResult result = this.accountDAO.transferOwnership(from, to);
        logger.warn(result.getModifiedCount() + " documents modified changing employer from email " + from.getEmail()
                + " to email " + to.getEmail());
    }

    /**
     * find all employee accounts with given boss email
     * 
     * @return a list of account hired by the provided account, or an empty list
     *         otherwise
     */
    @Override
    public List<Account> findAllEmployeeByBossEmail(String email) {
        Account boss = this.accountDAO.findFirstAccountByEmail(email);
        return this.accountDAO.findByEmployer(boss);
    }

    @Override
    public List<Account> findAllEmployeeByBoss(Account boss) {
        return this.accountDAO.findByEmployer(boss);
    }

    @Override
    public Account getBoss(Account requestor) {
        Account boss;
        if (requestor.getLevel() == 1)
            return null;
        if (requestor.getLevel() == 2)
            boss = requestor;
        else {
            // boolean isNull = requestor.getEmployer() == null;
            // boolean idIsNull = true;
            // if (!isNull) {
            // idIsNull = requestor.getEmployer().getId() == null;
            // }

            // logger.info("requestor " + requestor.getEmail() + ": employer is "
            // + (isNull ? "null" : (idIsNull ? "Unknow ID" :
            // requestor.getEmployer().getId().toHexString())));

            boss = this.accountDAO.findById(requestor.getEmployer().getId());
        }
        return boss;
    }

    @Override
    public boolean addNewCalendarEvent(CalendarEvent calendarEvent, Account acc) {
        CalendarEvent event = calendarEvent;
        event.setId("" + ObjectId.get());
        event.setCreatedAt(ZonedDateTime.now());
        event.setLastModifiedAt(ZonedDateTime.now());
        return this.accountDAO.addCalenderEvent(event, acc);
    }

    @Override
    public boolean updateCalendar(CalendarEvent updatedcalendarEvent) {
        CalendarEvent event = updatedcalendarEvent;
        event.setLastModifiedAt(ZonedDateTime.now());
        return this.accountDAO.updateCalendar(updatedcalendarEvent);

    }


    
}