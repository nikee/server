package com.nikee.universal.service;

import java.util.List;

import com.nikee.universal.dao.interfaces.ILeave;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.leave.Leave;
import com.nikee.universal.service.interfaces.ILeaveService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LeaveService implements ILeaveService {

    @Autowired
    private ILeave leaveDao;

    @Override
    public Leave addLeave(Account acc, Leave leave) {
        return leaveDao.addLeave(acc, leave);
    }

    @Override
    public Leave findLeave(Leave leave) {
        return leaveDao.findLeave(leave);
    }

    @Override
    public boolean update(Leave leave) {        
        return leaveDao.updateLeave(leave);
    }

    @Override
    public List<Leave> findAllLeaves(String empId) {        
        return leaveDao.findAllLeaves(empId);
    }

    @Override
    public boolean grantLeave(Leave leave) {
        return leaveDao.grantLeave(leave);
        
    }
    
}