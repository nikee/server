package com.nikee.universal.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.employee.Employee;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.service.interfaces.IAccountService;
import com.nikee.universal.service.interfaces.IEmployeeService;
import com.nikee.universal.structure.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    javax.servlet.http.HttpServletRequest HttpServletRequest;    
    @Autowired
    private IEmployeeService empService;
    @Autowired
    private IAccountService accountService;

    @ResponseBody
    @PostMapping("/requestleave")
    public Response<Employee> create(@RequestBody Employee employee) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            //T4 Users (Common Employees) can not perform this function 
            if (acc.getLevel() == 4){
                return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank..!");
            }
            else{
                    //Create employee and return new Employee in a list
                    Employee created = this.empService.registerNew(acc, employee);
                    List<Employee> list = new ArrayList<>();
                    list.add(created);
                    return new Response<Employee>(genericError.OK, "Employee created with ID " + created.getId(), list);
            }
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PutMapping("update")
    public Response<Employee> update(@RequestBody Employee emp){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        Account boss;
        if(obj != null){
            Account acc = (Account) obj;
            if(acc.getLevel() == 4){
                return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank..!");
            }
            else{
                boss = this.accountService.getBoss(acc);
                Employee original = this.empService.findEmployee(acc, emp.getId());
                if(original == null)
                    return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested employee does not exists..!");                
                if(!original.getCompany().equals(boss.getCompany()))
                    return new Response<>(genericError.ILLEGAL_DOMAIN,"You don't have access to this data..!");
                if(!this.empService.update(emp))
                    return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,"Update operation was not successful..!");
                return new Response<Employee>(genericError.OK, "Update was successful..!");                
            }
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }

    @ResponseBody
    @GetMapping("/find/{empId}")
    public Response<Employee> findEmployee(@PathVariable String empId){

        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        Account boss;
        if(obj != null){
            Account acc = (Account) obj;
            if(acc.getLevel() == 4){
                return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank..!");
            }
            else{
                boss = this.accountService.getBoss(acc);
                Employee original = this.empService.findEmployee(acc, empId);
                if(original == null)
                    return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested employee does not exists..!");                
                if(!original.getCompany().equals(boss.getCompany()))
                    return new Response<>(genericError.ILLEGAL_DOMAIN,"You don't have access to this data..!");
                else{
                    List<Employee> list = new ArrayList<>();
                    list.add(original);
                    return new Response<Employee>(genericError.OK, "Employee Found..!", list);   
                }                             
            }
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }


    @ResponseBody
    @GetMapping("/findAll")
    public Response<Employee> findEmployee(){

        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        Account boss;
        if(obj != null){
            Account acc = (Account) obj;
            if(acc.getLevel() == 4){
                return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank..!");
            }
            else{
                boss = this.accountService.getBoss(acc);
                List<Employee> listEmp = this.empService.findAllEmployees(acc);
                if(listEmp == null)
                    return new Response<>(genericError.ILLEGAL_ARGUMENT, "No employees found..!");               
                else{
                    List<Employee> list = new ArrayList<>();
                    list.addAll(listEmp);
                    return new Response<Employee>(genericError.OK, list.size() + " Employee(s) Found..!", list);   
                }                             
            }
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }

    @ResponseBody
    @GetMapping("/delete")
    public Response<Employee> deleteEmployee(@RequestBody Employee emp){

        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        Account boss;
        if(obj != null){
            Account acc = (Account) obj;
            if(acc.getLevel() == 4){
                return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank..!");
            }
            else{
                boss = this.accountService.getBoss(acc);
                Employee original = this.empService.findEmployee(acc, emp.getId());
                if(original == null)
                    return new Response<>(genericError.ILLEGAL_ARGUMENT, "Employee does not exists..!");                
                if(!original.getCompany().equals(boss.getCompany()))
                    return new Response<>(genericError.ILLEGAL_DOMAIN,"You don't have access to this data..!");
                if (!this.empService.deleteEmployee(original))
                    return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,
                            "Delete operation was not successful.");
                return new Response<>(genericError.OK, "Deletion was successful.");           
            }
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }

    









    
}