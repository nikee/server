package com.nikee.universal.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.nikee.universal.dao.CompanyDAO;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Services;
import com.nikee.universal.entity.account.Tax;
import com.nikee.universal.errors.TransactionFailureException;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.service.interfaces.IAccountService;
import com.nikee.universal.service.interfaces.IServiceService;
import com.nikee.universal.structure.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/services")
public class ServiceController {

    @Autowired
    IAccountService accountService;
    @Autowired
    IServiceService serviceService;
    @Autowired
    HttpServletRequest HttpServletRequest;
    @Autowired
    private CompanyDAO cDao;
    Logger logger = LoggerFactory.getLogger(ServiceController.class);

    @ResponseBody
    @PostMapping("/amend")
    /***
     * Allow t2, t3, t4 to create service T2 T3
     */
    public Response amendRoot(@RequestBody Services s) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            if (acc.getLevel() == 4)
                // must not be T4 user
                return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank to do this.");
            Account boss = this.accountService.getBoss(acc);
            Services created = this.serviceService.createRoot(s, boss);
            List<Services> list = new ArrayList<>();
            list.add(created);
            return new Response<Services>(genericError.OK, "Root service created.", list);
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");

    }

    @ResponseBody
    @PostMapping("/amendNode")
    /**
     * T2 - T4 T2 T3
     * 
     * @param s
     * @param parentId
     * @return
     */
    public Response amendChild(@RequestBody Services s, @RequestParam("parent") String parentId) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            if (acc.getLevel() == 4)
                // must not be T4 user
                return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank to do this.");
            Account boss = accountService.getBoss(acc);
            Services service = this.serviceService.getService(parentId);
            if (service == null) // what if cannot find service instance with given id?
                return new Response<>(genericError.ILLEGAL_ARGUMENT,
                        "It seems that the service ID( " + parentId + " ) you provided is not valid.");
            if (!service.getCompanyId().equals(boss.getCompany())) { // what if the found service instance does not
                                                                     // belong to your
                // domain?(not owned by the same owner)
                return new Response<>(genericError.ILLEGAL_DOMAIN, "You cannot operate data beyond your domain.");
            }
            Services created = this.serviceService.create(s, parentId, boss);
            List<Services> list = new ArrayList<>();
            list.add(created);
            return new Response<Services>(genericError.OK, "Service node created.", list);
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PostMapping("/cutNode")
    /**
     * cut a single node from the tree T2 T3
     * 
     * @param id
     * @return
     */
    public Response cutNode(@RequestParam("oid") String id) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            if (acc.getLevel() == 4)
                return new Response<>(genericError.LEVEL_3_REQUIRED, "You must have a higher rank to do this.");
            Services service = this.serviceService.getService(id);
            if (service == null) {
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "Service node with oid " + id + " not found.");
            }
            Account boss = this.accountService.getBoss(acc);
            if (!service.getCompanyId().equals(boss.getCompany())) { // what if the found service instance does not
                                                                     // belong to your
                // domain?(not owned by the same owner)
                return new Response<>(genericError.ILLEGAL_DOMAIN, "You cannot operate data beyond your domain.");
            }
            try {
                this.serviceService.cut(service);
                return new Response<>(genericError.OK, "Service node with oid " + id + " removed.");
            } catch (TransactionFailureException e) {
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED, e.getMessage(), e);
            }
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PostMapping("/cutBranch")
    /**
     * cut the following branches since one node T2 T3
     * 
     * @param id
     * @return
     */
    public Response cutBranch(@RequestParam("oid") String id) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            if (acc.getLevel() == 4)
                // must not be T4 user
                return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank to do this.");
            Services service = this.serviceService.getService(id);
            if (service == null) {
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "Service node with oid " + id + " not found.");
            }
            Account boss = this.accountService.getBoss(acc);
            if (!service.getCompanyId().equals(boss.getCompany())) { // what if the found service instance does not
                                                                     // belong to your
                // domain?(not owned by the same owner)
                return new Response<>(genericError.ILLEGAL_DOMAIN, "You cannot operate data beyond your domain.");
            }
            try {
                this.serviceService.deleteBranchSinceNode(service);
                return new Response<>(genericError.OK, "Service branches since node oid " + id + " removed.");
            } catch (TransactionFailureException e) {
                return new Response<>(genericError.ILLEGAL_ARGUMENT, e.getMessage(), e);
            }
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @GetMapping("/all")
    /**
     * T2 -T4
     * 
     * @return
     */
    public Response findAll() {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            if (acc.getLevel() == 4)
                // must not be T4 user
                return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank to do this.");
            Account boss = this.accountService.getBoss(acc);
            List<Services> list = this.serviceService.findByOwner(boss);
            return new Response<Services>(genericError.OK, "All services of your business fetched.", list);
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PostMapping("/insert")
    /**
     * T2- T4 T2 T3
     * 
     * @param toBeInserted
     * @param id
     * @return
     */
    public Response insert(@RequestBody Services toBeInserted, @RequestParam("base") String id) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            if (acc.getLevel() == 4)
                // must not be T4 user
                return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank to do this.");
            Account boss = this.accountService.getBoss(acc);
            Services content = toBeInserted;
            Services baseNode = this.serviceService.getService(id);
            if (baseNode == null) {
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "Service node with oid " + id + " not found.");
            }
            if (!baseNode.getCompanyId().equals(boss.getCompany())) { // what if the found service instance does not
                                                                      // belong to your
                // domain?(not owned by the same owner)
                return new Response<>(genericError.ILLEGAL_DOMAIN, "You cannot operate data beyond your domain.");
            }
            if (boss.getCompany() != null && content.getAppliedTax() == null) {
                Tax tax = cDao.find(boss.getCompany()).getTax();
                if (tax != null) {
                    content.setAppliedTax(tax);
                }
            }
            this.serviceService.insert(baseNode, content);
            return new Response<Services>(genericError.OK, "Content inserted.");
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PostMapping("update")
    /**
     * Allows T2-T4 T2 T3
     * 
     * @param _update
     * @return
     */
    public Response update(@RequestBody Services _update) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                // must not be T1 user
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            if (acc.getLevel() == 4)
                // must not be T4 user
                return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank to do this.");
            Account boss = this.accountService.getBoss(acc);
            Services baseNode = this.serviceService.getService(_update.getId());
            if (baseNode == null) {
                // node not found
                return new Response<>(genericError.ILLEGAL_ARGUMENT,
                        "Service node with oid " + _update.getId() + " not found.");
            }
            if (!baseNode.getCompanyId().equals(boss.getCompany())) {
                // what if the found service instance does not belong to your
                // domain?(not owned by the same owner)
                return new Response<>(genericError.ILLEGAL_DOMAIN, "You cannot operate data beyond your domain.");
            }
            _update.setId(baseNode.getId());
            this.serviceService.update(_update);
            return new Response<Services>(genericError.OK, "Content updated.");
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PostMapping("setRoot")
    /**
     * t2-t4 T2 T3
     * 
     * @param id
     * @return
     */
    public Response setRoot(@RequestParam String id) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                // must not be T1 user
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            if (acc.getLevel() == 4)
                // must not be T4 user
                return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank to do this.");
            Account boss = this.accountService.getBoss(acc);
            Services baseNode = this.serviceService.getService(id);
            if (baseNode == null) {
                // node not found
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "Service node with oid " + id + " not found.");
            }
            if (!baseNode.getCompanyId().equals(boss.getCompany())) {
                // what if the found service instance does not belong to your
                // domain?(not owned by the same owner)
                return new Response<>(genericError.ILLEGAL_DOMAIN, "You cannot operate data beyond your domain.");
            }
            this.serviceService.setRoot(baseNode);
            return new Response<Services>(genericError.OK, "Service node " + id + " is now a root node.");
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @GetMapping("/whosYourDaddy")
    public Object whosYourDaddy(@RequestParam String id) {

        Services s = this.serviceService.whosYourDaddy(id);
        List<Services> list = new ArrayList<>();
        list.add(s);
        return new Response<Services>(genericError.OK, "Response.", list);
    }

}