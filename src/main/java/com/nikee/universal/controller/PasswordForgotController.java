package com.nikee.universal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.nikee.universal.dao.PasswordResetTokenRepository;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Mail;
import com.nikee.universal.entity.PasswordResetToken;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.service.EmailService;
import com.nikee.universal.service.interfaces.*;
import com.nikee.universal.structure.Response;
import com.nikee.universal.support.dto.PasswordForgotDto;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.springframework.web.bind.annotation.RequestBody;

//http://localhost:8080/forgot-password

// ref:
// https://memorynotfound.com/spring-security-forgot-password-send-email-reset-password/
@RestController
@RequestMapping("/forgot-password")
public class PasswordForgotController {

    @Autowired
    private IAccountService userService;
    @Autowired
    private PasswordResetTokenRepository tokenRepository;
    @Autowired
    private EmailService emailService;

    // @ModelAttribute("forgotPasswordForm")
    // public PasswordForgotDto forgotPasswordDto() {
    // return new PasswordForgotDto();
    // }

    // @GetMapping
    // public String displayForgotPasswordPage() {
    // return "forgot-password";
    // }

    // @PostMapping
    // public String processForgotPasswordForm(@ModelAttribute("forgotPasswordForm")
    // @Valid PasswordForgotDto form,
    // BindingResult result, HttpServletRequest request) {

    @GetMapping
    @ResponseBody
    public Response request(@RequestParam String email, HttpServletRequest request) {

        Account user = userService.findByEmail(email);
        if (user == null) {
            return new Response<>(genericError.INVALID_EMAIL, "We cannot find the required email.");
        }
        PasswordResetToken token = new PasswordResetToken();
        token.setId(user.getEmail());
        token.setToken(UUID.randomUUID().toString());
        token.setUser(user);
        token.setExpiryDate(30);
        tokenRepository.save(token);

        Mail mail = new Mail();
        mail.setFrom("no-reply@nikeeworld.com");
        mail.setTo(user.getEmail());
        mail.setSubject("Password reset request");

        Map<String, Object> model = new HashMap<>();
        model.put("token", token);
        model.put("user", user);
        model.put("signature", "no-reply@nikeeworld.com");
        String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        // put correct RUL here
        model.put("resetUrl", url + "/reset-password?token=" + token.getToken());
        mail.setModel(model);
        emailService.sendEmail(mail);
        return new Response<>(genericError.OK, "An email has been sent to the required address.");
    }

    @PostMapping
    public Response callback(@RequestParam String token, @RequestParam String password) {
        PasswordResetToken resetToken = tokenRepository.findByToken(token);
        if (resetToken == null) { // cannot find a match of the given token in database
            // return "redirect:" + url + "errorPage"; // redirect to error in angular
            return new Response<>(genericError.ILLEGAL_ARGUMENT,
                    "We cannot find your token, or your token is invalid.");
        }
        // step 1: check if the token has expired
        if (resetToken.isExpired()) {
            // if the token has expired, redirect to error page
            // return "redirect:" + url + "errorPage"; // redirect to error in angular
            return new Response<>(genericError.ILLEGAL_ARGUMENT, "Your token has expired.");
        }
        // setp 2: regular expression validation of new password
        {
            // skipped

            // * 密码至少包含 数字和英文，长度6-20 include digit and alpha, length 6- 20
            // */
            // String reg = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$";

            // /**
            // * 密码包含 数字,英文,字符中的两种以上，长度6-20
            // */
            // String reg =
            // "^(?![0-9]+$)(?![a-z]+$)(?![A-Z]+$)(?!([^(0-9a-zA-Z)])+$).{6,20}$";

            // /**
            // * 至少包含数字跟字母，可以有字符
            // */
            // String reg =
            // "(?=.*([a-zA-Z].*))(?=.*[0-9].*)[a-zA-Z0-9-*/+.~!@#$%^&*()]{6,20}$";

        }
        // step 3: reset
        boolean succ = userService.changePassword(password, resetToken.getUser());
        if (succ)
            return new Response<>(genericError.OK, "Password is now changed.");
        else
            return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED, "Failed to write the change.");

    }

}