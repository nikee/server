package com.nikee.universal.controller;

import java.io.FileNotFoundException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.client.Action;
import com.nikee.universal.entity.client.Client;
import com.nikee.universal.entity.client.Note;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.service.ReportService;
import com.nikee.universal.service.interfaces.IAccountService;
import com.nikee.universal.service.interfaces.IClientService;
import com.nikee.universal.service.interfaces.IReportService;
import com.nikee.universal.structure.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import net.sf.jasperreports.engine.JRException;

@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private IClientService clientService;
    @Autowired
    private IAccountService accountService;
    @Autowired
    HttpServletRequest HttpServletRequest;
    Logger logger = LoggerFactory.getLogger(ClientController.class);
    @Autowired
    private IReportService reportService;

    @ResponseBody
    @PostMapping("/new")
    public Response<Client> create(@RequestBody Client client) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            // boss = this.accountService.getBoss(acc);
            // if (acc.getLevel() == 4)
            // // must not be T4 user
            // return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank
            // to do this.");
            Client created = this.clientService.registerNew(acc, client);
            List<Client> list = new ArrayList<>();
            list.add(created);
            return new Response<Client>(genericError.OK, "Client created with ID " + created.getId(), list);
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @GetMapping("/all")
    public Response<Client> getClientsOfBusiness() {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        Account boss;
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            boss = this.accountService.getBoss(acc);
            // if (acc.getLevel() == 4)
            // // must not be T4 user
            // return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank
            // to do this.");
            List<Client> list = this.clientService.findByBusiness(boss);
            String msg = "Client data fetched.";
            try {
                msg = "Clients of business (" + boss.getCompany() + ") fetched.";
            } catch (NullPointerException e) {

            }
            return new Response<Client>(genericError.OK, msg, list);
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @GetMapping("/find")
    /**
     * Behave the same as getClientsOfBusiness() if no param is provided
     */
    public Response<Client> searchClient(@RequestParam(required = false, name = "id") String id,
            @RequestParam(required = false, name = "fn") String fn,
            @RequestParam(required = false, name = "ln") String ln,
            @RequestParam(required = false, name = "email") String email,
            @RequestParam(required = false, name = "phone") String phone) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        Account boss;
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            boss = this.accountService.getBoss(acc);
            List<Client> list = new ArrayList<>();
            String msg;
            // if (acc.getLevel() == 4)
            // // must not be T4 user
            // return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank
            // to do this.");
            if (id != null) { // if id param is not null, then find by id
                Client client = this.clientService.findClient(acc, id);
                if (client != null && client.getCompanyId().equals(boss.getCompany()))
                    list.add(client);
                msg = "Searched by ID " + id;
            } else if (fn == null && ln == null && email == null && phone == null) { // if all query params are null,
                                                                                     // then find all of the business
                list = this.clientService.findByBusiness(boss);
                msg = "As no param was provided, searched all clients of your business.";

            } else { // otherwise find by query params
                List<Client> all = this.clientService.findByBusiness(boss);
                msg = "Searched based on provided params: ";
                StringBuilder sb = new StringBuilder(msg);
                Stream<Client> stream = all.stream();
                if (fn != null) {
                    stream = stream.filter(c -> c.getFirstName().trim().equalsIgnoreCase(fn));
                    sb.append("First Name as ").append(fn).append("\n");
                }
                if (ln != null) {
                    stream = stream.filter(c -> c.getLastName().trim().equalsIgnoreCase(ln));
                    sb.append("Last Name as ").append(ln).append("\n");
                }
                if (email != null) {
                    stream = stream.filter(c -> c.getEmail().trim().equalsIgnoreCase(email));
                    sb.append("Email as ").append(email).append("\n");
                }
                if (phone != null) {
                    stream = stream.filter(c -> c.getPhone().trim().equalsIgnoreCase(phone));
                    sb.append("Phone as ").append(phone).append("\n");
                }
                list = stream.collect(Collectors.toList());
            }
            return new Response<Client>(genericError.OK, msg + list);
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PostMapping("/updateBasic")
    /**
     * update basic 4 fields
     */
    public Response<Client> updateBasic(@RequestBody Client client) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        Account boss;
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            boss = this.accountService.getBoss(acc);
            // if (acc.getLevel() == 4)
            // // must not be T4 user
            // return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank
            // to do this.");
            Client original = this.clientService.findClient(acc, client.getId());
            if (original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested client does not exist.");
            if (!original.getCompanyId().equals(boss.getCompany()))
                return new Response<>(genericError.ILLEGAL_DOMAIN, "You don't have access to this data.");
            if (!this.clientService.updateBasic(client))
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,
                        "Update operation was not successful.");
            return new Response<>(genericError.OK, "Update was successful..!");
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    /**
     * 
     * @param id
     * @param description
     * @param dt          when passing date, it should be in the following format
     *                    2015-09-26T01:30:00.000%2B04:00 or
     *                    2015-09-26T01:30:00.000-04:00 when passing "+" in
     *                    parameter, make sure you replace it with %2B so it can be
     *                    correctly parsed
     * @param staffIds
     * @return
     */
    @ResponseBody
    @PostMapping("/createAction")
    public Response<Client> createAction(@RequestParam String id, @RequestParam String description,
            @RequestParam(name = "deadline") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) ZonedDateTime dt,
            @RequestParam(name = "assigned_to", required = false) List<String> staffIds) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        Account boss;
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            boss = this.accountService.getBoss(acc);
            // if (acc.getLevel() == 4)
            // // must not be T4 user
            // return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank
            // to do this.");
            Client original = this.clientService.findClient(acc, id);
            if (original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested client does not exist.");
            if (!original.getCompanyId().equals(boss.getCompany()))
                return new Response<>(genericError.ILLEGAL_DOMAIN, "You don't have access to this data.");
            List<String> strIds = staffIds == null ? new ArrayList<>() : staffIds;
            List<Account> allStaffs = this.accountService.findAllEmployeeByBoss(boss); // all staff
                                                                                       // belonging to
                                                                                       // the business
                                                                                       // in DB
                                                                                       // boolean flag =
                                                                                       // mock.stream().allMatch(a
                                                                                       // ->
                                                                                       // allStaffs.contains(a));
                                                                                       // // Are all
                                                                                       // ids found in
                                                                                       // exsiting data?
            List<Account> selected = allStaffs.stream()
                    .filter(a -> strIds.stream().anyMatch(str -> a.getId().equalsIgnoreCase(str)))
                    .collect(Collectors.toList()); // filtered
            if (selected.size() != strIds.size())
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "One of the input account id is not found.");
            boolean flag = this.clientService.addAction(original, description, selected, acc, dt);
            if (!flag)
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,
                        "Update operation was not successful.");
            return new Response<>(genericError.OK, "Action created and assigned to " + selected.size() + " people.");
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    /**
     * 
     * @param tick
     * @param id
     * @param actionId
     * @return
     */
    @PostMapping("/tick")
    @ResponseBody
    public Response<Client> tick(@RequestParam(name = "tick", required = true) Boolean tick,
            @RequestParam("client") String id, @RequestParam("action") String actionId) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        Account boss;
        StringBuilder sb = new StringBuilder();
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            boss = this.accountService.getBoss(acc);
            Client client = this.clientService.findClient(acc, id);
            if (client == null) {
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested client does not exist.");
            }
            if (!client.getCompanyId().equals(boss.getCompany()))
                return new Response<>(genericError.ILLEGAL_DOMAIN, "You do not have access to this data.");
            Boolean tickFlag = null;
            // do tick/untick
            if (tick) {
                tickFlag = this.clientService.tickAction(acc, client, actionId);
                sb.append("Checkbox of account ID " + acc.getId() + " ticked "
                        + (tickFlag ? "Successfully." : "with failure."));
            } else {
                tickFlag = this.clientService.unTickAction(acc, client, actionId);
                sb.append("Checkbox of account ID " + acc.getId() + " unticked "
                        + (tickFlag ? "Successfully." : "with failure."));
            }
            if (tickFlag)
                return new Response<>(genericError.OK, sb.toString());
            else
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED, sb.toString());

        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");

    }

    @ResponseBody
    @PostMapping("/updateAction")
    /**
     * Update Action desc and/or deadline
     * 
     * @param description optional to change client.description
     * @param dt          Optional to change client.deadline
     * @param id          Must provide. Client ID.
     * @param actionId    Must provide, Action ID
     * @return
     */
    public Response<Client> updateAction(@RequestParam(name = "description", required = false) String description,
            @RequestParam(name = "deadline", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) ZonedDateTime dt,
            @RequestParam("client") String id, @RequestParam("action") String actionId) {

        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");

        if (description == null && dt == null)
            return new Response<>(genericError.OK, "No change applied.");

        Account boss;
        StringBuilder sb = new StringBuilder();
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            boss = this.accountService.getBoss(acc);
            Client client = this.clientService.findClient(acc, id);
            if (client == null) {
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested client does not exist.");
            }
            if (!client.getCompanyId().equals(boss.getCompany()))
                return new Response<>(genericError.ILLEGAL_DOMAIN, "You do not have access to this data.");
            // pre check finished
            Boolean commonFlag = null;

            Action action = new Action();
            action.setId(actionId);
            int index = client.getActions().indexOf(action);
            if (index == -1) {
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested action does not exist.");
            }
            action = client.getActions().get(index);
            if (description != null)
                action.setDescription(description);
            if (dt != null)
                action.setDeadline(dt);
            commonFlag = this.clientService.updateAction(client, action);
            sb.append(commonFlag ? "description & deadline updated." : "Update of description & deadline failed. ");
            return new Response<>(commonFlag ? genericError.OK : genericError.DATABASE_CHANGE_NOT_CONFIRMED,
                    sb.toString());
        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");

    }

    @ResponseBody
    @PostMapping("/note")
    public Response<Client> addNote(@RequestParam("client") String clientID, @RequestParam("message") String payload) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        Account boss;
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            boss = this.accountService.getBoss(acc);
            // if (acc.getLevel() == 4)
            // // must not be T4 user
            // return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank
            // to do this.");
            Client original = this.clientService.findClient(acc, clientID);
            if (original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested client does not exist.");
            if (!original.getCompanyId().equals(boss.getCompany()))
                return new Response<>(genericError.ILLEGAL_DOMAIN, "You don't have access to this data.");
            if (!this.clientService.addNewNote(original, payload, acc))
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,
                        "Update operation was not successful.");
            return new Response<>(genericError.OK, "Update was successful.");
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PostMapping("/updateNote")
    public Response<Client> updateNote(@RequestParam("client") String clientID, @RequestParam("message") String payload,
            @RequestParam("note_id") String noteID) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        Account boss;
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            boss = this.accountService.getBoss(acc);
            // if (acc.getLevel() == 4)
            // // must not be T4 user
            // return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank
            // to do this.");
            Client original = this.clientService.findClient(acc, clientID);
            if (original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested client does not exist.");
            if (!original.getCompanyId().equals(boss.getCompany()))
                return new Response<>(genericError.ILLEGAL_DOMAIN, "You don't have access to this data.");
            Note note = new Note();
            note.setId(noteID);
            int index = original.getNotes().indexOf(note);
            if (index == -1)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "Provided Note ID is not valid.");
            note = original.getNotes().get(index);

            if (!this.clientService.updateNote(original, note, payload, acc))
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,
                        "Update operation was not successful.");
            return new Response<>(genericError.OK, "Update was successful.");

        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");

    }

    @ResponseBody
    @PostMapping("/deleteNote")
    public Response<Client> deleteNote(@RequestParam("client") String clientID,
            @RequestParam("note_id") String noteID) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        Account boss;
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            if (acc.getLevel() == 4)
                return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank to do this.");
            boss = this.accountService.getBoss(acc);
            // if (acc.getLevel() == 4)
            // // must not be T4 user
            // return new Response<>(genericError.LEVEL_3_REQUIRED, "You need a higher rank
            // to do this.");
            Client original = this.clientService.findClient(acc, clientID);
            if (original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested client does not exist.");
            if (!original.getCompanyId().equals(boss.getCompany()))
                return new Response<>(genericError.ILLEGAL_DOMAIN, "You don't have access to this data.");
            Note note = new Note();
            note.setId(noteID);
            int index = original.getNotes().indexOf(note);
            if (index == -1)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "Provided Note ID is not valid.");
            note = original.getNotes().get(index);

            if (!this.clientService.deleteNote(original, noteID))
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,
                        "Delete operation was not successful.");
            return new Response<>(genericError.OK, "Deletion was successful.");
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    //This method is only for testing purposes of reports
    @GetMapping("/report/{format}")
    public String generateReport(@PathVariable String format) throws FileNotFoundException, JRException {

        //TO-DO
        //SESSION VALIDATE
        return this.reportService.exportReport(format);
    }


   

}