package com.nikee.universal.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.account.Expenses;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.service.interfaces.IExpenseService;
import com.nikee.universal.structure.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/expense")
public class ExpenseController {

    @Autowired
    private IExpenseService expenseService;
    @Autowired
    private HttpServletRequest httpServletRequest; 

    /**
     * Saves an expense to the db
     * @return String Message only, change accordingly in the future
     */
    @PostMapping("/create")
    public Response<Expenses> addExpense(@RequestBody Expenses expense){
        HttpSession session = this.httpServletRequest.getSession();
        Object object = session.getAttribute("account");
        Expenses exp = new Expenses();
        List<Expenses> listExp = new ArrayList<>();
        if(object != null){
            Account acc = (Account) object;
            exp = expenseService.add(acc, expense);
            if(exp != null){
                listExp.add(exp);
                return new Response<Expenses>(genericError.OK,"Expense Added..!", listExp);
            }                
        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED,"Please Login..!");   
    }

    /*Find all expenses for the company*/
    @ResponseBody
    @GetMapping("/findAll")
    public Response<Expenses> findAllExp(){
        HttpSession session = this.httpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        Account boss;
        Expenses exp = new Expenses();
        List<Expenses> listExp = new ArrayList<>();
        if(obj != null){
            Account acc = (Account) obj;
            if(acc.getLevel() == 1){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            }
            else{
                listExp = this.expenseService.findAllExpenses(acc);                
                if(listExp == null)
                    return new Response<>(genericError.ILLEGAL_ARGUMENT, "No records found..!");
                return new Response<Expenses>(genericError.OK, listExp.size() +" Records Found..!", listExp);            
            } 
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }
    


}