package com.nikee.universal.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.nikee.universal.dao.interfaces.IAccount;
import com.nikee.universal.entity.Access;
import com.nikee.universal.entity.Account;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.service.interfaces.IAccessService;
import com.nikee.universal.structure.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/access")
public class AccessController {
    
    @Autowired
    private HttpServletRequest HttpServletRequest;

    @Autowired
    private IAccessService accessService;

    @Autowired
    private IAccount accountDao;

    @ResponseBody
    @PostMapping("/create")
    public Response<Access> create(@RequestBody Access access) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if(acc.getLevel() > 2){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must be a level 1 or level 2 user to do this.");
            }
            if(acc.getLevel() == 2 && !acc.getCompany().equals(accountDao.findById(access.getAccountId()).getCompany())) {
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must stay at the same company with the account which you want to manage the access!");
            }
 
            Access original = this.accessService.find(access);
            if(original != null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The Access you want to create was already created please try update request..!", original);

            this.accessService.create(access);;

            return new Response<Access>(genericError.OK, "Access created..!", access);
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PostMapping("/delete")
    public Response<Access> clean(@RequestBody Access access) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;

            if(acc.getLevel() > 2){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must be a level 1 or level 2 user to do this.");
            }
            if(acc.getLevel() == 2 && !acc.getCompany().equals(accountDao.findById(access.getAccountId()).getCompany())) {
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must stay at the same company with the account which you want to manage the access!");
            }

            Access original = this.accessService.find(access);
            if(original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested record does not exist..!");

            if(!this.accessService.delete(access))
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,"Deleting record was not successful..!");

            return new Response<Access>(genericError.OK, "Deleting record was successful..!");
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PutMapping("/update")
    public Response<Access> update(@RequestBody Access access){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){
            Account acc = (Account) obj;
            
            if(acc.getLevel() > 2){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must be a level 1 or level 2 user to do this.");
            }
            if(acc.getLevel() == 2 && !acc.getCompany().equals(accountDao.findById(access.getAccountId()).getCompany())) {
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must stay at the same company with the account which you want to manage the access!");
            }

            Access original = this.accessService.find(access);
            if(original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested record does not exist..!");

            if(!this.accessService.update(access))
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,"Updating was not successful..!");

            return new Response<Access>(genericError.OK, "Updating was successful..!", this.accessService.find(access));   
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }

    @ResponseBody
    @GetMapping("/viewMyAccess")
    public Response<Access> find(){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){
            Account acc = (Account) obj;

            Access access = new Access(acc.getId(), null);  // Temp access obj with current account's id to find acurret account's access

            Access original = this.accessService.find(access);  //by access._Id which is the accountId in account class
            if(original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested record does not exist..!");

            List<Access> list = new ArrayList<>();
            list.add(original);

            return new Response<>(genericError.OK, list);
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }
    
    @ResponseBody
    @GetMapping("/viewAll")
    public Response<Access> findAll(){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){
            Account acc = (Account) obj;
            if(acc.getLevel() > 2){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must be a level 1 or level 2 user to do this operation..!");
            }
            
            List<Access> list = new ArrayList<>();
            list = this.accessService.findAll();

            if(list == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "No record found..!");

            // T1 manager can view all the other's access
            if(acc.getLevel() == 1)
                return new Response<>(genericError.OK, list.size() +" Records Found..!", list);

            // T2 user can only view the account access in his/her company
            Iterator<Access> iter = list.iterator();
            while (iter.hasNext()) {
                Access access = iter.next();
                if (!acc.getCompany().equals(accountDao.findById(access.getAccountId()).getCompany())) {
                    iter.remove();
                }
            }

            return new Response<>(genericError.OK, list.size() +" Records Found..!", list);            
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }

}
