package com.nikee.universal.controller;

import java.io.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.nikee.universal.config.UploadSetting;
import com.nikee.universal.entity.Account;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.service.interfaces.IReportService;
import com.nikee.universal.structure.Response;
import com.nikee.universal.support.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.sf.jasperreports.engine.JRException;

@RestController
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private HttpServletRequest HttpServletRequest;
    @Autowired
    private IReportService reportServcie;
    @Autowired
    private TimerTask timerTask;

    @Autowired
    private UploadSetting setting;

    /* wenzel */
    @GetMapping("/gst/{format}")
    public ResponseEntity<InputStreamResource> gstControlReport(@PathVariable String format) throws IOException {

        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            try {
                // generate report
                reportServcie.getGstReport(acc, format);

            } catch (FileNotFoundException | JRException e) {
                e.printStackTrace();
            }
            FileSystemResource file = new FileSystemResource(
                setting.getDefReportPath() + "\\" + acc.getCompany() + "\\GSTControlReport." + format);  
            HttpHeaders headers = new HttpHeaders();  
            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");  
            headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getFilename()));  
            headers.add("Pragma", "no-cache");  
            headers.add("Expires", "0");

            return ResponseEntity  
                .ok()  
                .headers(headers)  
                .contentLength(file.contentLength())  
                .contentType(MediaType.parseMediaType("application/octet-stream"))  
                .body(new InputStreamResource(file.getInputStream()));
        } else
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();

    }

    @GetMapping("/cashflow/{format}")
    public ResponseEntity<InputStreamResource> cashFlowReport(@PathVariable String format) throws IOException {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            try {
                reportServcie.getCashFlowReport(acc, format);
            } catch (FileNotFoundException | JRException e) {
                e.printStackTrace();
            }
            FileSystemResource file = new FileSystemResource(
                setting.getDefReportPath() + "\\" + acc.getCompany() + "\\CashFlow." + format);  
            HttpHeaders headers = new HttpHeaders();  
            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");  
            headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getFilename()));  
            headers.add("Pragma", "no-cache");  
            headers.add("Expires", "0");

            return ResponseEntity  
                .ok()  
                .headers(headers)  
                .contentLength(file.contentLength())  
                .contentType(MediaType.parseMediaType("application/octet-stream"))  
                .body(new InputStreamResource(file.getInputStream()));
        } else
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        //     return new Response<>(genericError.OK, "CashFlow Report Generated Successfully..!");

        // } else
        //     return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please Log In..!");

    }

    @GetMapping("/autoExport/{format}")
    public Response<String> autoExport(@PathVariable String format) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;

            timerTask.startCron(acc, format);

            return new Response<>(genericError.OK, "Automatically Reports Export Started Successfully..!");

        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please Log In..!");

    }

}