package com.nikee.universal.controller;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.nikee.universal.dao.CompanyDAO;
import com.nikee.universal.dao.OwnershipTranshferTokenRepository;
import com.nikee.universal.dao.interfaces.IAccount;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Attendance;
import com.nikee.universal.entity.Company;
import com.nikee.universal.entity.Mail;
import com.nikee.universal.entity.OwnershipTransferToken;
import com.nikee.universal.entity.account.CalendarEvent;
import com.nikee.universal.entity.license.License;
import com.nikee.universal.errors.LicenseExpiredError;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.service.EmailService;
import com.nikee.universal.service.LicenseService;
import com.nikee.universal.service.interfaces.IAccountService;
import com.nikee.universal.service.interfaces.IAttendanceService;
import com.nikee.universal.structure.Response;
import com.nikee.universal.support.Wrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    IAccountService service;
    @Autowired
    HttpServletRequest HttpServletRequest;
    @Autowired
    OwnershipTranshferTokenRepository tokenRepository;
    @Autowired
    private EmailService emailService;
    @Autowired
    private LicenseService licenseService;
    @Autowired
    private IAccount accountDAO;
    @Autowired
    private CompanyDAO companyDAO;

    @Autowired
    private IAttendanceService attendanceService;

    Logger logger = LoggerFactory.getLogger(AccountController.class);

    @ResponseBody
    @GetMapping("/test")
    /**
     * call this to generate the first user with highest rank
     * Change email and password to desired ones
     */
    public String test() {
        Account acc = new Account();
        acc.setEmail("support@nikeecloud.com");
        acc.setPassword("password");
        acc.setLevel(1);
        return this.service.test(acc);
    }

    @GetMapping("/purchase")
    @ResponseBody
    public Response purchase(@RequestParam String ownerID,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) ZonedDateTime extendTo) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() != 1)
                return new Response<>(genericError.LEVEL_1_REQUIRED, "You must be a super manager to do this.");
            {// main body here
                Account target = this.accountDAO.findById(ownerID);
                if (target == null)
                    return new Response<>(genericError.ILLEGAL_ARGUMENT, "Specified account does not exist.");
                try {
                    License license = this.licenseService.purchase(target.getCompany(), extendTo);
                    return new Response<>(genericError.OK,
                            "License of UID " + ownerID + " expiration date extended to " + extendTo.toString(),
                            license);
                } catch (IllegalArgumentException e) {
                    return new Response<>(genericError.ILLEGAL_ARGUMENT, e.getMessage());
                }
            }
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @RequestMapping("/java4s-spring-boot-ex-tomcat")
    public String customerInformation() {
        return "Hey, I am from external tomcat";
    }

    @ResponseBody
    @GetMapping("/findStaff")
    public Response findMyStaff() {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            return new Response<Account>(genericError.OK, this.service.findAllEmployeeByBoss(service.getBoss(acc)));

        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");

    }

    @PostMapping("/transferCallback")
    /**
     * callback for angular page. Submit the token and the password of the invitee
     * email, to login or create account
     */
    @ResponseBody
    public Response transferCallback(@RequestParam String token, @RequestParam String password,
            RedirectAttributes attr) {
        OwnershipTransferToken transferToken = this.tokenRepository.findByToken(token);
        String url = this.HttpServletRequest.getScheme() + "://" + this.HttpServletRequest.getServerName() + ":"
                + this.HttpServletRequest.getServerPort() + "/";

        if (transferToken == null) { // cannot find a match of the given token in database
            attr.addAttribute("message", "We cannot find your token, or your token is invalid.");
            // return "redirect:" + url + "errorPage"; // redirect to error in angular
            return new Response<>(genericError.ILLEGAL_ARGUMENT,
                    "We cannot find your token, or your token is invalid.");
        }

        // step 1: check if the token has expired
        if (transferToken.isExpired()) {
            // if the token has expired, redirect to error page
            // TODO send to error page
            attr.addAttribute("message", "Your token has expired.");
            // return "redirect:" + url + "errorPage"; // redirect to error in angular
            return new Response<>(genericError.ILLEGAL_ARGUMENT, "Your token has expired.");
        }

        // step 2: check if the account already exists
        Account invitee = this.service.findByEmail(transferToken.getTo());
        if (invitee == null) {
            // invitee account does not exist, should be created
            invitee = new Account();
            invitee.setPassword(Account.hashPassword(password));
            invitee.setEmail(transferToken.getTo());
        } else {
            // invitee account exists.

            // double check if the account is t1 or t2
            if (invitee.getLevel() < 3) {
                attr.addAttribute("message", "The invited account must not be a business owner or super manager.");
                // return "redirect:" + url + "errorPage";
                return new Response<>(genericError.LEVEL_3_REQUIRED,
                        "The invited account must not be a business owner or super manager.");
            }

            // check password
            if (!Account.validatePassword(invitee.getPassword(), password)) {
                attr.addAttribute("message", "The provided password of the existing account is not correct.");
                // return "redirect:" + url + "errorPage";
                return new Response<>(genericError.INCORRECT_PASSWORD,
                        "The provided password of the existing account is not correct.");
            }
        }
        // by now, the account will have email & password fields at least.
        // step 3: create account
        this.service.transferOwnership(transferToken.getFrom(), invitee);
        this.tokenRepository.delete(transferToken);
        return new Response<>(genericError.OK, "Transaction successful.");
        // return "redirect:" + url + ""; // redirect to login or sign up page in
        // angular
    }

    /**
     * Call this to initialise
     */
    @ResponseBody
    @PostMapping("/transfer")
    public Response transferOwnership(@RequestParam String email, @RequestParam String password) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account transferFrom = (Account) obj; // who is transfering
            if (transferFrom.getLevel() != 2)
                return new Response<>(genericError.LEVEL_2_REQUIRED,
                        "You must be a business owner to transfer ownership."); // throw an error. you must be level 2
                                                                                // user to transfer ownership
                                                                                // TODO send invitation to the address
            if (!Account.validatePassword(transferFrom.getPassword(), password))
                return new Response<>(genericError.INCORRECT_PASSWORD, "Incorrect password.");
            Account user = this.service.findByEmail(email);
            if (user != null && user.getLevel() < 3)
                return new Response<>(genericError.LEVEL_3_REQUIRED,
                        "The invitee must not be an existing business owner or super manager.");
            if (user == null) {
                user = new Account();
                user.setFirstName("Sir/Ma'am").setLastName("");
            } else { // if the target user already exist
                if (!this.service.getBoss(user.getEmployer()).equals(transferFrom))
                    return new Response<>(genericError.ILLEGAL_DOMAIN,
                            "You cannot transfer the ownership to a member of another business.");
            }

            // save the tokens
            OwnershipTransferToken token = new OwnershipTransferToken();
            token.setId(transferFrom.getEmail());
            token.setToken(UUID.randomUUID().toString());
            token.setFrom(transferFrom);
            token.setTo(email);
            token.setExpiryDate(30);
            tokenRepository.save(token);

            Mail mail = new Mail();
            mail.setFrom("no-reply@nikeeworld.com");
            mail.setTo(email);
            mail.setSubject("Ownership transfer request");
            Map<String, Object> model = new HashMap<>();
            model.put("token", token);
            model.put("user", user);
            model.put("signature", "https://nikeeworld.com");
            // String url = this.HttpServletRequest.getScheme() + "://" +
            // this.HttpServletRequest.getServerName() + ":"
            // + this.HttpServletRequest.getServerPort();
            String url = "http://localhost:8000/#!/reset";
            model.put("resetUrl", url + "?token=" + token.getToken());
            mail.setModel(model);
            emailService.sendEmail(mail, EmailService.TRANSFER_OWNERSHIP);
            return new Response<>(genericError.OK, "An invitation has been sent to " + email
                    + " to transfer your ownership. You can still withdraw the invitation before the effect is in place.");
        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PostMapping("/invite")
    public Response invite(@RequestParam("email") String email, @RequestParam("password") String password,
            @RequestParam(name = "level", defaultValue = "4") int level) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account invitor = (Account) obj; // who is sending invitation
            if (invitor.getLevel() != 2 && invitor.getLevel() != 3)
                return new Response<>(genericError.LEVEL_2_REQUIRED,
                        "You must be a business owner or manager to transfer ownership.");
            if (!Account.validatePassword(invitor.getPassword(), password))
                return new Response<>(genericError.INCORRECT_PASSWORD, "Incorrect password.");
            Account user = this.service.findByEmail(email);
            if (user != null)
                return new Response<>(genericError.ILLEGAL_DOMAIN, "You cannot invite an existing account.");
            if (level != 4 && level != 3)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "Level can only be 3 or 4.");
            if (invitor.getLevel() >= level)
                level = 4;

            // save the tokens
            OwnershipTransferToken token = new OwnershipTransferToken();
            token.setId(invitor.getEmail());
            token.setToken(UUID.randomUUID().toString());
            token.setFrom(invitor);
            token.setTo(email);
            token.setExpiryDate(60 * 24);
            token.setLevel(level);
            tokenRepository.save(token);

            Mail mail = new Mail();
            mail.setFrom("no-reply@nikeeworld.com");
            mail.setTo(email);
            String name = this.companyDAO.find(service.getBoss(invitor).getCompany()).getBusinessName();
            mail.setSubject("Invitation to join" + name == null ? "" : " " + name);
            Map<String, Object> model = new HashMap<>();
            model.put("token", token);
            model.put("signature", "https://nikeeworld.com");
            // String url = this.HttpServletRequest.getScheme() + "://" +
            // this.HttpServletRequest.getServerName() + ":"
            // + this.HttpServletRequest.getServerPort();
            String url = "http://localhost:8000/#!/reset";
            model.put("resetUrl", url + "?token=" + token.getToken());
            mail.setModel(model);
            emailService.sendEmail(mail, EmailService.INVITATION);
            return new Response<>(genericError.OK, "An invitation has been sent to " + email
                    + " to transfer your ownership. You can still withdraw the invitation before the effect is in place.");
        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PostMapping("/inviteCallback")
    public Response<Account> inviteCallback(@RequestParam("token") String token, @RequestBody Account toCreate) {
        OwnershipTransferToken transferToken = this.tokenRepository.findByToken(token);
        if (transferToken == null) { // cannot find a match of the given token in database
            // return "redirect:" + url + "errorPage"; // redirect to error in angular
            return new Response<>(genericError.ILLEGAL_ARGUMENT,
                    "We cannot find your token, or your token is invalid.");
        }
        // step 1: check if the token has expired
        if (transferToken.isExpired()) {
            // if the token has expired, redirect to error page
            return new Response<>(genericError.ILLEGAL_ARGUMENT, "Your token has expired.");
        }
        // step 2: check if the account already exists
        Account invitee = this.service.findByEmail(transferToken.getTo());
        Account invitor = this.service.findByEmail(transferToken.getFrom().getEmail());
        Account boss = this.service.getBoss(invitor);
        if (invitee == null) {
            // invitee account does not exist, should be created

            toCreate.setEmail(transferToken.getTo());
            toCreate.setId(null);
            toCreate.setDiscarded(false);
            toCreate.setLevel(transferToken.getLevel());
            toCreate.setCompany(null);
            toCreate.setEmployer(boss);
        } else {
            return new Response<>(genericError.INVALID_EMAIL, "Account already exists. Please log instead.");
        }
        // by now, the account will have email & password fields at least.
        // step 3: create account
        Account created;
        if (toCreate.getLevel() == 3) {
            created = this.service.createT3Account(toCreate, invitor);
            this.tokenRepository.delete(transferToken);
        } else {
            created = this.service.createT4Account(toCreate, invitor);
            this.tokenRepository.delete(transferToken);
        }
        List<Account> list = new ArrayList<>();
        list.add(created);
        return new Response<>(genericError.OK, "Account created.", list);
    }

    @ResponseBody
    @GetMapping("/login")
    /**
     * Login with email and password. The session will be injected with account
     * instance with name 'account' if the logging in succeeded, or the session will
     * be destroyed otherwise.
     */
    public Response login(@RequestParam("email") String email, @RequestParam("password") String password,
            HttpServletRequest request) {
        HttpSession session = HttpServletRequest.getSession();
        Map<String, Object> result = this.service.login(email, password);
        Exception e = null;
        Account account = null;
        int status = (int) result.get("status_code");
        if (status == 200) {
            account = (Account) result.get("payload");
            License license = account.getLevel() != 1 ? licenseService.myLicense(account) : null;
            if (account.getLevel() > 2) // lv 3 or 4
            {
                // only allow T3 or T4 to login if their host company holds a valid license
                if (!license.isExpired()) {
                    session.setAttribute("account", account);
                    session.setAttribute("license", license);

                    // Record attendance if the user successfully logged in
                    Attendance attendance = attendanceService.findById(account.getId());
                    if(attendance == null) {
                        attendanceService.create(new Attendance(account.getId()));
                        attendance = attendanceService.findById(account.getId());
                    }
                    attendance.setLogInTime(ZonedDateTime.now());
                    attendanceService.update(attendance);

                } else {
                    status = genericError.LICENSE_EXPIRED;
                    e = new LicenseExpiredError(license);
                }

            } else {
                if (account.getLevel() == 2) // lv2
                {
                    session.setAttribute("account", account);
                    session.setAttribute("license", license);

                    // Record attendance if the user successfully logged in
                    Attendance attendance = attendanceService.findById(account.getId());
                    if(attendance == null) {
                        attendanceService.create(new Attendance(account.getId()));
                        attendance = attendanceService.findById(account.getId());
                    }
                    attendance.setLogInTime(ZonedDateTime.now());
                    attendanceService.update(attendance);

                    if (license.isExpired()) {
                        e = new LicenseExpiredError(license);
                    }

                } else if (account.getLevel() == 1) {
                    session.setAttribute("account", account);
                }
            }

        } else {
            // destroy session if login attempt fails
            if (session != null) {
                session.invalidate();
            }
        }
        List<Object> list = new ArrayList<>();
        list.add(result.get("payload"));
        logger.info("Session " + session.getId() + " connected.");
        return new Response<Object>(status, list, e);
    }

    /**
     * Allow client to sign out by prividing his email and comparing it with session
     * detail
     */
    @ResponseBody
    @PostMapping("/signOut")
    public Response signOut(@RequestParam("email") String email) {
        HttpSession session = HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj == null)
            logger.info("Sign out operation -> session account attr is null!");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getEmail().equals(email)) {

                // Record attendance if the user successfully logged out
                Attendance attendance = attendanceService.findById(acc.getId());
                attendance.setLogOutTime(ZonedDateTime.now());
                attendanceService.update(attendance);

                session.invalidate();
                logger.warn("Session " + session.getId() + " discarded.");
                return new Response<>(200, "You have been logged out.");
            }
        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Something went wrong when signing off.");
    }

    @ResponseBody
    @PostMapping("/changePassword")
    /**
     * Change the password of the account logged in. Will be signed off when the
     * password is changed successfully.
     */
    public Response changePassword(@RequestParam("new_password") String newPwd) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (this.service.changePassword(newPwd, acc)) {
                // succ
                session.invalidate();
                logger.warn("Session " + session.getId() + " discarded.");
                return new Response<>(200, "password of account " + acc.getEmail() + " changed. Please log in again.");
            } else {
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED, "password change failed.");
                // change not acknoledged
            }
        }
        // current session do not have account info, which will only be set when log in.
        // This means the client haven't log in.
        // TODO redirect to log in.
        return new Response(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PostMapping("/updateProfile")
    public Response updateProfile(@RequestBody Account account) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            Account saved = this.service.updateProfile(account, acc);
            session.setAttribute("account", saved);
            return new Response<>(200, "Profile detail  modification of " + acc.getEmail() + " conducted.");
        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");

    }

    @ResponseBody
    @GetMapping("/me")
    public Response myProfile() {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            List<Object> list = new ArrayList<>();
            list.add(obj);
            return new Response<Object>(200, list);
        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PostMapping("/createT2")
    public Response createT2Account(@RequestBody Wrapper<Account, Company> wrapper) {
        Account acc = wrapper.getPayload1();
        Company com = wrapper.getPayload2();
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            if (((Account) obj).getLevel() == 1) {
                try {
                    Account created = this.service.createT2Account(acc, com);
                    List<Account> list = new ArrayList<>();
                    list.add(created);
                    return new Response<Account>(200, "Account created.", list);
                } catch (DuplicateKeyException e) {
                    logger.warn("DuplicateKey ", e);
                    return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,
                            "Failed to insert data. May be related to duplicated email? Check attached error for details.",
                            e);
                }

            } else
                return new Response<>(genericError.LEVEL_1_REQUIRED, "You do not have permission to do this.");
        }

        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");

    }

    @ResponseBody
    @PostMapping("/createT3")
    public Response createT3Account(@RequestBody Account account) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account boss = (Account) obj;
            if (boss.getLevel() != 2)
                return new Response<>(genericError.LEVEL_2_REQUIRED,
                        "You must be the business owner to create a manager account.");
            try {
                Account created = this.service.createT3Account(account, boss);
                List<Account> list = new ArrayList<>();
                list.add(created);
                return new Response<Account>(200, "Account created.", list);
            } catch (DuplicateKeyException e) {
                logger.warn("DuplicateKey ", e);
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,
                        "Failed to insert data. May be related to duplicated email? Check attached error for details.",
                        e);
            }

        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PostMapping("/createT4")
    public Response createT4Account(@RequestBody Account account) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account operator = (Account) obj;
            if (operator.getLevel() != 2 && operator.getLevel() != 3)
                return new Response<>(genericError.LEVEL_3_REQUIRED,
                        "You must be the business owner or a manager to create an employee account.");
            try {
                Account created = this.service.createT4Account(account, operator);
                List<Account> list = new ArrayList<>();
                list.add(created);
                return new Response<Account>(200, "Account created.", list);
            } catch (DuplicateKeyException e) {
                logger.warn("DuplicateKey ", e);
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,
                        "Failed to insert data. May be related to duplicated email? Check attached error for details.",
                        e);
            }

        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }


    /*
     * 
     * @param calendarEvent --> CalendarEvent object sent from frontend, taske as a @RequestBody
     * At the moment any account have access to create calendar event
     * @return <Response> Message (Success or Not)
     * Calender event will be saved to Account's list of Calender events
     * Not to a seperate table
     */

    @ResponseBody
    @PostMapping("/addcalendar")
    public Response<Account> addCalendarEvent(@RequestBody CalendarEvent calendarEvent) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (!this.service.addNewCalendarEvent(calendarEvent, acc))
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,"Update operation was not successful.");
            return new Response<>(genericError.OK, "New Calendar Event Added Successfully!");//modidfy this return 
            //to send the newwwly created calendar object
            //so, it can be bound to ui and update instantly
            //after this, reloadAccountData() can be deleted
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    /**
     * When account saved a calendar event, it needs to refresh ui with newly created Calendar event 
     * Calendar ID is required for update an existing calendar event (Note:calendar ID created when inserting to DB)
     * For that send Account object which includes list of calendar events for that account
     * @return Response which includes Account data (updated)
     */
    @GetMapping("/reload")
    public Account reloadAccountData(@RequestParam("email") String email){        
        //HttpSession session = HttpServletRequest.getSession();
        return service.findByEmail(email);
        
    }


    /**
     * Update Calendar event
     * @param calendar event id
     * @return a response (success or not)
     */
    @ResponseBody
    @PutMapping("/updatecalendar")
    public Response<Account> updateCalendarEvent(@RequestBody CalendarEvent updatedcalendarEvent){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Exception ex = null;
            ArrayList<CalendarEvent> listCalendarEvents = new ArrayList<>();
            ArrayList<Account> listAcc = new ArrayList<>();
            Account acc = new Account();
            if(service.updateCalendar(updatedcalendarEvent)){         
                listCalendarEvents.add(updatedcalendarEvent);
                acc.setCalendarEvents(listCalendarEvents);
                listAcc.add(acc);
                return new Response<>(genericError.OK,listAcc, ex);
            }
            return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,"Update operation was not successful.");
        } else
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");

    }


    public Response forgetPassword() {
        // TODO forget password
        return null;
    }
}
