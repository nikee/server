package com.nikee.universal.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Mail;
import com.nikee.universal.entity.employee.Employee;
import com.nikee.universal.entity.leave.Leave;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.service.EmailService;
import com.nikee.universal.service.interfaces.IAccessService;
import com.nikee.universal.service.interfaces.IAccountService;
import com.nikee.universal.service.interfaces.ILeaveService;
import com.nikee.universal.structure.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/leave")
public class LeaveMgtController {


    @Autowired
    private HttpServletRequest HttpServletRequest;
    @Autowired
    private ILeaveService leaveService;

    @Autowired
    private IAccountService accountService;

    @Autowired
    private EmailService emailService;
    
    @Autowired
    private IAccessService accessService;

    @ResponseBody
    @PostMapping("/requestleave")
    public Response<Leave> create(@RequestBody Leave leave) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            //All Users except Root (T2,T3,T4,) can perform this operation
            if(acc.getLevel() == 1){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            }

            Leave created = this.leaveService.addLeave(acc, leave);
            List<Leave> list = new ArrayList<>();
            list.add(created);

            Account boss = accountService.getBoss(acc);
            Mail mail = new Mail();
            mail.setFrom("no-reply@nikeeworld.com");
            mail.setTo(boss.getEmail());
            mail.setSubject("Request Leave");

            Map<String, Object> model = new HashMap<>();
            model.put("user", boss);
            model.put("acc", acc);
            model.put("leave", leave);
            model.put("signature", "no-reply@nikeeworld.com");
            mail.setModel(model);
            emailService.sendEmail(mail, "email-leave-template");
            
            return new Response<Leave>(genericError.OK, "Leave created..! Leave ID: " +  created.getId(), list);
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }


    @ResponseBody
    @PutMapping("/update")
    public Response<Leave> update(@RequestBody Leave leave){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){
            Account acc = (Account) obj;
            if(acc.getLevel() == 1){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            }
            if(acc.getLevel() > 2 && accessService.identifyAccess(acc.getId(), "leave") == 0){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You do not have permission to do this..!");
            }
            else{
                Leave original = this.leaveService.findLeave(leave);//by empId and leaveID
                if(original == null)
                    return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested Levae does not exists..!");
                if(!this.leaveService.update(leave))
                    return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,"Update operation was not successful..!");

                Account boss = accountService.getBoss(acc);
                Mail mail = new Mail();
                mail.setFrom("no-reply@nikeeworld.com");
                mail.setTo(boss.getEmail());
                mail.setSubject("Request Leave");
    
                Map<String, Object> model = new HashMap<>();
                model.put("user", boss);
                model.put("acc", acc);
                model.put("leave", leave);
                model.put("signature", "no-reply@nikeeworld.com");
                mail.setModel(model);
                emailService.sendEmail(mail, "email-leave-template");

                return new Response<Leave>(genericError.OK, "Update was successful..!");                
            }
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }

    @ResponseBody
    @GetMapping("/find")
    public Response<Leave> find(@RequestBody Leave leave){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){
            Account acc = (Account) obj;
            if(acc.getLevel() > 2 && accessService.identifyAccess(acc.getId(), "leave") == 0){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You do not have permission to do this..!");
            }
            else{
                Leave original = this.leaveService.findLeave(leave);//by empId and leaveID
                if(original == null)
                    return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested Levae does not exists..!");
                List<Leave>list = new ArrayList<>();
                list.add(original);
                return new Response<>(genericError.OK, list);            
            }
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }

    /*Find all leaves for the employee*/
    @ResponseBody
    @GetMapping("/findAll/{empId}")
    public Response<Leave> find(@PathVariable String empId){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){
            Account acc = (Account) obj;
            if(acc.getLevel() > 2 && accessService.identifyAccess(acc.getId(), "leave") == 0){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You do not have permission to do this..!");
            }
            else{
                List<Leave> list = new ArrayList<>();
                list = this.leaveService.findAllLeaves(empId);//by empId and leaveID
                if(list == null)
                    return new Response<>(genericError.ILLEGAL_ARGUMENT, "No records found..!");
                return new Response<>(genericError.OK, list.size() +" Records Found..!", list);            
            } 
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }

    @ResponseBody
    @PutMapping("/grant")
    public Response<Leave> grantLeave(@RequestBody Leave leave){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){
            Account acc = (Account) obj;

            if(acc.getLevel() > 2 && accessService.identifyAccess(acc.getId(), "leave") == 0){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You do not have permission to do this..!");
            }
            else{
                Leave original = this.leaveService.findLeave(leave);//by empId and leaveID
                if(original == null)
                    return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested Levae does not exists..!");
                if(!this.leaveService.grantLeave(leave))
                    return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,"Grant was not successful..!");

                Employee emp = leave.getEmployee();
                Mail mail = new Mail();
                mail.setFrom("no-reply@nikeeworld.com");
                mail.setTo(emp.getEmail());
                mail.setSubject("Request Leave Granted");
    
                Map<String, Object> model = new HashMap<>();
                model.put("user", emp);
                model.put("leave", leave);
                model.put("boss", acc);
                model.put("signature", "no-reply@nikeeworld.com");
                mail.setModel(model);
                emailService.sendEmail(mail, "email-leaveGranted-template");

                return new Response<Leave>(genericError.OK, "Update was successful..!");                              
            }
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }


    
}