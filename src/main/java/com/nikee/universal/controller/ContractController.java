package com.nikee.universal.controller;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.mongodb.client.result.DeleteResult;
import com.nikee.universal.dao.interfaces.IAccount;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.contract.Deal;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.service.interfaces.IAccountService;
import com.nikee.universal.service.interfaces.IContractService;
import com.nikee.universal.structure.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/deal")
public class ContractController {
    @Autowired
    private IContractService serviceContract;
    @Autowired
    private IAccount daoAccount;
    @Autowired
    private IAccountService serviceAccount;
    @Autowired
    private HttpServletRequest HttpServletRequest;

    @ResponseBody
    @PostMapping("/create")
    public Response<Deal> create(@RequestBody Deal deal) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (!this.serviceContract.validate(deal, acc))
                return new Response<>(genericError.ILLEGAL_ARGUMENT,
                        "Request data failed in validation. Please review your request.");
            Deal created = this.serviceContract.create(deal, acc);
            List<Deal> list = new ArrayList<>();
            list.add(created);
            return new Response<>(200, "Deal created.", list);
        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @PostMapping("/update")
    public Response<Deal> update(@RequestBody Deal deal) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            // validate the balance of input deal
            if (!this.serviceContract.validate(deal, acc))
                return new Response<>(genericError.ILLEGAL_ARGUMENT,
                        "Request data failed in validation. Please review your request.");
            // get original data in DB
            if (deal.getId() == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "Please provide contract id.");
            Deal origin = this.serviceContract.findById(deal.getId());
            if (origin == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT,
                        "Request data failed in validation. Must update an existing entry.");
            // check if the user is t2 or t3
            // t4 can only update his own record
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            if (acc.getLevel() == 4 && !origin.getCreatedBy().equals(acc.getId()))
                return new Response<>(genericError.ILLEGAL_DOMAIN, "You cannot modify this record.");
            // be t2 or t3 or t4 with permission
            Deal modified = this.serviceContract.save(this.serviceContract.prepareToSave(deal, origin, acc), acc);
            List<Deal> list = new ArrayList<>();
            list.add(modified);
            return new Response<>(200, "Deal modified.", list);
        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @GetMapping("/find")
    public Response<Deal> findAllInBusiness() {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            List<Deal> list = this.serviceContract.findByBusiness(this.serviceAccount.getBoss(acc));
            return new Response<>(200, "All transaction records of your business fetched.", list);
        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @GetMapping("/{id}")
    public Response<Deal> findById(@PathVariable String id) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            try {
                Deal result = this.serviceContract.findById(id);
                if (result == null)
                    return new Response<>(200, "No record found."); // cannot find the specified document
                if (acc.getId().equals(result.getCreatedBy()))
                // requestor is the creator -> grant access
                {
                    List<Deal> list = new ArrayList<>();
                    list.add(result);
                    return new Response<>(200, "Document with ID " + id + " fetched.", list);
                } else {
                    Account creator = this.daoAccount.findById(result.getCreatedBy());
                    if (this.serviceAccount.getBoss(creator).getId().equals(this.serviceAccount.getBoss(acc).getId())) {
                        // same domain
                        List<Deal> list = new ArrayList<>();
                        list.add(result);
                        return new Response<>(200, "Document with ID " + id + " fetched.", list);
                    } else {
                        // different domains
                        return new Response<>(genericError.ILLEGAL_DOMAIN, "You cannot access this document.");
                    }

                }
            } catch (IllegalArgumentException e) {
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The input ID is not valid.");
            }
        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @GetMapping("/client/{id}")
    public Response<Deal> findByClientId(@PathVariable String id) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            try {
                List<Deal> deals = this.serviceContract.findByClient(id);
                return new Response<>(genericError.OK, deals);
            } catch (IllegalArgumentException e) {
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The input ID is not valid.");
            }
        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @DeleteMapping("/{id}")
    public Response delete(@PathVariable("id") String id) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            try {
                Deal toDelete = this.serviceContract.findById(id);
                if (toDelete == null)
                    return new Response<>(200, "No record found."); // cannot find the specified document
                DeleteResult result;
                if (toDelete.getCreatedBy().equals(acc.getId()))
                // requestor is the creator -> grant access
                {
                    result = this.serviceContract.remove(toDelete);
                } else {
                    Account creator = this.daoAccount.findById(toDelete.getCreatedBy());
                    if (this.serviceAccount.getBoss(creator).getId().equals(this.serviceAccount.getBoss(acc).getId())) {
                        // same domain
                        result = this.serviceContract.remove(toDelete);
                    } else {
                        // different domains
                        return new Response<>(genericError.ILLEGAL_DOMAIN, "You cannot access this document.");
                    }

                }
                if (result.wasAcknowledged()) {
                    return new Response<>(genericError.OK, "Document with ID " + toDelete.getId() + " deleted.");
                } else {
                    return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,
                            "Deletion of document with ID " + toDelete.getId() + " not confirmed.");
                }
            } catch (IllegalArgumentException e) {
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The input ID is not valid.");
            }
        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    /**
     * 
     * Tick off one particular installment as paid
     * 
     * @param id
     * @param index
     * @return
     */
    @PostMapping("/{id}/{index}")
    @ResponseBody
    public Response<Deal> tick(@PathVariable("id") String id, @PathVariable("index") int index) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            if (acc.getLevel() == 1)
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must not be a super manager to do this.");
            try {
                Deal deal = this.serviceContract.findById(id);
                if (deal == null) {
                    return new Response<>(genericError.ILLEGAL_ARGUMENT, "Cannot find the document specified.");
                }
                if (deal.getInstallments().size() <= index) {
                    return new Response<>(genericError.ILLEGAL_ARGUMENT, "Index not valid.");
                }
                deal.confirm(index, ZonedDateTime.now(), acc.getId());
                Deal updated = this.serviceContract.save(deal, acc);
                List<Deal> list = new ArrayList<>();
                list.add(updated);
                return new Response<>(genericError.OK,
                        "Contract ID " + id + ", installment index " + index + ": payment confirmed. ", list);

            } catch (IllegalArgumentException e) {
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The input ID is not valid.");
            }
        }
        return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");

    }

}