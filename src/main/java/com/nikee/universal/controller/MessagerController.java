package com.nikee.universal.controller;

import com.nikee.universal.dao.MessagerDAO;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.account.Message.Envelope;
import com.nikee.universal.entity.account.Message.MailList;
import com.nikee.universal.entity.account.Message.Msg;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.service.MessagerService;
import com.nikee.universal.structure.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/message")
public class MessagerController {

    @Autowired
    private MessagerService messager;
    @Autowired
    private MessagerDAO dao;
    @Autowired
    private HttpServletRequest HttpServletRequest;

    Logger logger = LoggerFactory.getLogger(MessagerController.class);

    @ResponseBody
    @PostMapping("/send")
    public Response send(@RequestBody Envelope envelope, @RequestParam int scope, @RequestParam String to) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            {// main body here
                Envelope e = envelope;
                if (e.getBody() == null)
                    return new Response<>(genericError.ILLEGAL_ARGUMENT, "Please enter mail content.");
                e.setFrom(acc.getId());
                if (scope == 1) { // PRIVATE
                    messager.preparePrivate(e, to);
                } else if (scope == 2) { // PUBLIC
                    messager.preparePublic(e, to);
                } else if (scope == 3) { // GLOBAL
                    messager.prepareGlobal(e);
                } else {
                    return new Response<>(genericError.ILLEGAL_ARGUMENT, "Invalid scope number.");
                }
                if (messager.validateScope(e)) {
                    e = messager.persist(e);
                    return new Response<>(genericError.OK, "Message sent", e);
                } else {
                    return new Response<>(genericError.ILLEGAL_DOMAIN, "Failed to pass authentication.");
                }
            }
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    /**
     * GET mail box
     * 
     * @return
     */
    @ResponseBody
    @GetMapping("/inbox")
    public Response getInbox() {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            {// main body here
                MailList ml = this.dao.findById(acc.getId());
                return new Response<>(genericError.OK, "Inbox fetched.", ml);
            }
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    // save mail list
    @ResponseBody
    @PostMapping("/inbox")
    public Response postInbox(@RequestBody MailList ml) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            {// main body here
                ml.setId(acc.getId());
                ml = this.dao.save(ml);
                return new Response<>(genericError.OK, "Inbox saved.", ml);
            }
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");

    }

    // get envelope
    @ResponseBody
    @GetMapping("/envelope")
    public Response getEnvelopes(@RequestParam List<String> list) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            {// main body here
                List<Envelope> enList = this.dao.getEnvelopes(list);
                List<Envelope> res = new ArrayList<>();
                enList.forEach(en -> {
                    if ((en.getFlag() == Envelope.PRIVATE && en.getTo().equals(acc.getId()))
                            || (en.getFlag() == Envelope.PUBLIC && en.getDomain().equals(acc.getCompany()))
                            || en.getFlag() == Envelope.GLOBAL) {
                        res.add(en);
                    }
                });
                return new Response<>(genericError.OK, "Envelopes fetched.", res);
            }
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    // get msg -> get mail detail
    @ResponseBody
    @GetMapping("/content")
    public Response getContents(@RequestParam List<String> list) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;
            {// main body here
                List<Msg> msgList = this.dao.getContents(list);
                if (acc.getLevel() == 1)
                    return new Response<>(genericError.OK, "Envelopes fetched.", msgList);
                List<Msg> res = new ArrayList<>();
                msgList.forEach(en -> {
                    if (en.getScope() == null || en.getScope().equals(acc.getCompany())) {
                        res.add(en);
                    }
                });
                return new Response<>(genericError.OK, "Envelopes fetched.", res);
            }
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

}