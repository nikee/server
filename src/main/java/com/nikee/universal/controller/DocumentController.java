package com.nikee.universal.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.nikee.universal.config.UploadSetting;
import com.nikee.universal.entity.Account;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.structure.Response;
import com.nikee.universal.support.RandomString;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping("/doc")
public class DocumentController {

    @Autowired
    private UploadSetting setting;
    private RandomString gen = new RandomString(10);
    @Autowired
    HttpServletRequest HttpServletRequest;

    Logger logger = LoggerFactory.getLogger(DocumentController.class);

    private String prefixGen() {
        LocalDateTime ldt = LocalDateTime.now();
        StringBuilder sb = new StringBuilder();
        sb.append(ldt.getYear()).append(String.format("%02d", ldt.getMonthValue()))
                .append(String.format("%02d", ldt.getDayOfMonth())).append(String.format("%02d", ldt.getHour()))
                .append(String.format("%02d", ldt.getMinute())).append(String.format("%02d", ldt.getSecond()));
        return sb.toString();
    }

    @GetMapping("/view")
    public ArrayList<String> viewDocument() {
        HttpSession session = HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj == null) {
            return null;
        }

        setting.setWindowsPath(setting.getDefUploadPath());

        ArrayList<String> files = new ArrayList<String>();
        File file = new File(setting.getPath());
        File[] tempList = file.listFiles();
    
        for (int i = 0; i < tempList.length; i++) {
            if (tempList[i].isFile()) {
                files.add(tempList[i].toString());
            }
        }
        return files;
    }

    

    @PostMapping(value = "/file")
    @ResponseBody
    public Response<String> postFile(@RequestParam("file") MultipartFile file) {
        HttpSession session = HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj == null) {
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please Log In..!");
        }
        /* Wenzel */
        Account acc = (Account) obj;
        if (acc.getLevel() > 2)
            return new Response<>(genericError.LEVEL_2_REQUIRED,
                    "You must be the business owner to upload the file.");
                    
        setting.setWindowsPath(setting.getDefUploadPath());

        File fileMkdir = new File(setting.getPath());
        if (!fileMkdir.exists()) {
            fileMkdir.mkdir();
        }
        String fileName = file.getOriginalFilename();
        int index = fileName.lastIndexOf(".");
        String extension = fileName.substring(index);
        fileName = fileName.substring(0, index) + " - " + this.prefixGen() + extension; 

        // Name the file with original name plus time and random String
        // fileName = this.prefixGen() + gen.nextString() + extension; 
        // File temp = new File(fileMkdir.getPath() + "/" + fileName);
        // int counter = 0;
        // while (temp.exists()) {
        //     fileName = this.prefixGen() + gen.nextString() + extension;
        //     temp = new File(fileMkdir.getPath() + "/" + fileName);
        //     counter++;
        //     if (counter > 10)
        //         return null;
        // }

        try {
            String fullPath = fileMkdir.getPath() + "/" + fileName;
            FileOutputStream os = new FileOutputStream(fullPath);
            InputStream in = file.getInputStream();
            int b = 0;
            while ((b = in.read()) != -1) { // Read File
                os.write(b);
            }
            os.flush(); // Close Stream - ධාරාව වසන්න
            in.close();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new Response<>(genericError.OK, "Files Uploaded Successfully..!");
        
    }

    @GetMapping("/download/{filename}")
    public ResponseEntity<InputStreamResource> gstControlReport(@PathVariable String filename) throws IOException {

        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        FileSystemResource file = new FileSystemResource(setting.getDefUploadPath() + "\\" + filename);
        HttpHeaders headers = new HttpHeaders();  
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");  
        headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getFilename()));  
        headers.add("Pragma", "no-cache");  
        headers.add("Expires", "0");

        return ResponseEntity  
            .ok()  
            .headers(headers)  
            .contentLength(file.contentLength())  
            .contentType(MediaType.parseMediaType("application/octet-stream"))  
            .body(new InputStreamResource(file.getInputStream()));
        
    }

}