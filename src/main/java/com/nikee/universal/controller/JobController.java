package com.nikee.universal.controller;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Job;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.service.interfaces.IAccessService;
import com.nikee.universal.service.interfaces.IJobService;
import com.nikee.universal.structure.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/job")
public class JobController {
    
    @Autowired
    private HttpServletRequest HttpServletRequest;

    @Autowired
    private IJobService jobService;

    @Autowired
    private IAccessService accessService;

    private void AutoDiscardOverdue() {
        for (Job job : jobService.findAllJob()) {
            if (job.getEndDate() == null)
                continue;
            if (ZonedDateTime.now().isAfter(job.getEndDate())) {
                jobService.discardJob(job);
            }
        }
    }

    @ResponseBody
    @PostMapping("/postJob")
    public Response<Job> create(@RequestBody Job job) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;

            if(acc.getLevel() > 2 && accessService.identifyAccess(acc.getId(), "job") == 0){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "Only the manager and HR staff can post the job ad..!");
            }
            
            job.setAccountId(acc.getId());
            this.jobService.postJob(job);

            return new Response<Job>(genericError.OK, "Job posted..!", job);
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @GetMapping("/find")
    public Response<Job> find(@RequestBody Job job){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){

            AutoDiscardOverdue();
            
            Job original = this.jobService.findJob(job);  //by job._Id
            if(original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested Job does not exists..!");

            List<Job> list = new ArrayList<>();
            list.add(original);

            return new Response<>(genericError.OK, list);            
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }

    @ResponseBody
    @GetMapping("/findAll")
    public Response<Job> findAll(){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){

            AutoDiscardOverdue();
            
            List<Job> list = new ArrayList<>();
            list = this.jobService.findAllJob();

            if(list == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "No records found..!");

            return new Response<>(genericError.OK, list.size() +" Records Found..!", list);            
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }

    @ResponseBody
    @PutMapping("/update")
    public Response<Job> update(@RequestBody Job job){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){
            Account acc = (Account) obj;

            if(acc.getLevel() != 2 && acc.getId() != job.getAccountId()){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You can only update the job ad posted by yourself or need a higher rank to perform this operation..!");
            }

            AutoDiscardOverdue();

            Job original = this.jobService.findJob(job);
            if(original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested Job does not exists..!");

            if(!this.jobService.updateJob(job))
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,"Update was not successful..!");

            return new Response<Job>(genericError.OK, "Update was successful..!", this.jobService.findJob(job));   
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }

    @ResponseBody
    @PutMapping("/discard")
    public Response<Job> discardJob(@RequestBody Job job){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){
            Account acc = (Account) obj;

            if(acc.getLevel() != 2 && acc.getId() != job.getAccountId()){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You can only discard the job ad posted by yourself or need a higher rank to perform this operation..!");
            }
            
            Job original = this.jobService.findJob(job);
            if(original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested Levae does not exists..!");

            if(!this.jobService.discardJob(job))
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,"Discard was not successful..!");

            return new Response<Job>(genericError.OK, "Discard was successful..!", this.jobService.findJob(job));                              
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }
}
