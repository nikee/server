package com.nikee.universal.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.nikee.universal.dao.interfaces.IAccount;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Attendance;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.service.interfaces.IAttendanceService;
import com.nikee.universal.structure.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/attendance")
public class AttendanceController {
    
    @Autowired
    private HttpServletRequest HttpServletRequest;
    
    @Autowired
    private IAttendanceService attendanceService;

    @Autowired
    private IAccount accountDao;

    @ResponseBody
    @GetMapping("/viewMyAttendance")
    public Response<Attendance> find(){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){
            Account acc = (Account) obj;

            // Find by attendance._Id which is the accountId
            Attendance original = this.attendanceService.findById(acc.getId());  
            if(original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested record does not exist..!");

            List<Attendance> list = new ArrayList<>();
            list.add(original);

            return new Response<>(genericError.OK, list);
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }

    @ResponseBody
    @GetMapping("/viewAll")
    public Response<Attendance> findAll(){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){
            Account acc = (Account) obj;
            if(acc.getLevel() > 2){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You must be a level 1 or level 2 user to do this operation..!");
            }
            
            List<Attendance> list = new ArrayList<>();
            list = this.attendanceService.findAll();

            if(list == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "No record found..!");

            // T1 manager can view all the other's access
            if(acc.getLevel() == 1)
                return new Response<>(genericError.OK, list.size() +" Records Found..!", list);

            // T2 user can only view the account access in his/her company
            Iterator<Attendance> iter = list.iterator();
            while (iter.hasNext()) {
                Attendance attendance = iter.next();
                if (!acc.getCompany().equals(accountDao.findById(attendance.getAccountId()).getCompany())) {
                    iter.remove();
                }
            }

            return new Response<>(genericError.OK, list.size() +" Records Found..!", list);            
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }
}
