package com.nikee.universal.controller;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Stock;
import com.nikee.universal.errors.genericError;
import com.nikee.universal.service.interfaces.IAccessService;
import com.nikee.universal.service.interfaces.IStockService;
import com.nikee.universal.structure.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/stock")
public class StockController {

    @Autowired
    private HttpServletRequest HttpServletRequest;

    @Autowired
    private IStockService stockService;

    @Autowired
    private IAccessService accessService;

    private void AutoExpireOverdue() {
        for (Stock stock : stockService.findAllStock()) {
            if (stock.getExpired() == null)
                continue;
            if (ZonedDateTime.now().isAfter(stock.getExpireDate())) {
                stockService.expireStock(stock);
            }
        }
    }

    @ResponseBody
    @PostMapping("/createStock")
    public Response<Stock> create(@RequestBody Stock stock) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;

            if(acc.getLevel() > 2 && accessService.identifyAccess(acc.getId(), "stock") == 0){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "Only the manager and Stock Management staff can create a new stock..!");
            }
            
            stock.setAccountId(acc.getId());
            this.stockService.createStock(stock);

            return new Response<Stock>(genericError.OK, "Stock created..!", stock);
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }

    @ResponseBody
    @GetMapping("/find")
    public Response<Stock> find(@RequestBody Stock stock){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){
            Account acc = (Account) obj;

            if(acc.getLevel() > 2 && accessService.identifyAccess(acc.getId(), "stock") == 0){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "Only the manager and Stock Management staff can create a new stock..!");
            }

            AutoExpireOverdue();
            
            Stock original = this.stockService.findStock(stock);
            if(original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested Stock does not exists..!");

            List<Stock> list = new ArrayList<>();
            list.add(original);

            return new Response<>(genericError.OK, list);            
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }
    
    @ResponseBody
    @GetMapping("/findAll")
    public Response<Stock> findAll(){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){
            Account acc = (Account) obj;

            if(acc.getLevel() > 2 && accessService.identifyAccess(acc.getId(), "stock") == 0){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "Only the manager and Stock Management staff can create a new stock..!");
            }

            AutoExpireOverdue();
            
            List<Stock> list = new ArrayList<>();
            list = this.stockService.findAllStock();

            if(list == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "No records found..!");

            return new Response<>(genericError.OK, list.size() +" Records Found..!", list);            
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }

    @ResponseBody
    @PutMapping("/update")
    public Response<Stock> update(@RequestBody Stock stock){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){
            Account acc = (Account) obj;

            if(acc.getLevel() != 2 && acc.getId() != stock.getAccountId()){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You can only update the stock detail created by yourself or need a higher rank to perform this operation..!");
            }

            AutoExpireOverdue();

            Stock original = this.stockService.findStock(stock);
            if(original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested Stock does not exists..!");

            if(!this.stockService.updateStock(stock))
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,"Update was not successful..!");

            return new Response<Stock>(genericError.OK, "Update was successful..!", this.stockService.findStock(stock));   
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }

    @ResponseBody
    @PutMapping("/setExpired")
    public Response<Stock> setExpired(@RequestBody Stock stock){
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if(obj != null){
            Account acc = (Account) obj;

            if(acc.getLevel() != 2 && acc.getId() != stock.getAccountId()){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You can only set the stock expired which created by yourself or need a higher rank to perform this operation..!");
            }
            
            Stock original = this.stockService.findStock(stock);
            if(original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested Stock does not exists..!");

            if(!this.stockService.expireStock(stock))
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,"Set expired was not successful..!");

            return new Response<Stock>(genericError.OK, "Set expired was successful..!", this.stockService.findStock(stock));                              
        }
        else{
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first..!");
        }
    }

    @ResponseBody
    @PostMapping("/cleanStock")
    public Response<Stock> clean(@RequestBody Stock stock) {
        HttpSession session = this.HttpServletRequest.getSession();
        Object obj = session.getAttribute("account");
        if (obj != null) {
            Account acc = (Account) obj;

            if(acc.getLevel() != 2 && acc.getId() != stock.getAccountId()){
                return new Response<>(genericError.LEVEL_2_REQUIRED, "You can only clean the stock which created by yourself or need a higher rank to perform this operation..!");
            }

            Stock original = this.stockService.findStock(stock);
            if(original == null)
                return new Response<>(genericError.ILLEGAL_ARGUMENT, "The requested Stock does not exists..!");

            if(!this.stockService.cleanStock(stock))
                return new Response<>(genericError.DATABASE_CHANGE_NOT_CONFIRMED,"Clean stock was not successful..!");

            return new Response<Stock>(genericError.OK, "Clean stock was successful..!");
        } else
            return new Response<>(genericError.SESSION_NOT_AUTHORIZED, "Please log in first.");
    }
}
