package com.nikee.universal.entity;

import java.io.Serializable;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "access")
public class Access implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    @Id
    private String accountId;
    private Map<String, Integer> access;  // String: the accessName, Integer: 0 means no permission

    public Access(String accountId, Map<String, Integer> access) {
        this.accountId = accountId;
        this.access = access;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Map<String, Integer> getAccess() {
        return access;
    }

    public void setAccess(Map<String, Integer> access) {
        this.access = access;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Access other = (Access) obj;
        if (accountId == null) {
            if (other.accountId != null)
                return false;
        } else if (!accountId.equals(other.accountId))
            return false;
        if (access == null) {
            if (other.access != null)
                return false;
        } else if (!access.equals(other.access))
            return false;
        return true;
    }
}
