package com.nikee.universal.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "stock")
public class Stock implements Serializable {

    /**
	 *
	 */
	private static final long serialVersionUID = 1L;
    
    @Id
    private String id;
    private String accountId;
    private ZonedDateTime manufactureDate;
    private ZonedDateTime expireDate;
    private String itemName;
    private String itemType;
    private Boolean expired;
    private String description;
    private Float price;
    private Integer quantity;
    private Float weight;
    private String contactInfo;

    public Stock(String id, String accountId, ZonedDateTime manufactureDate, ZonedDateTime expireDate, String itemName,
    String itemType, Boolean expired, String description, Float price, Integer quantity, Float weight, String contactInfo) {
        this.id = id;
        this.accountId = accountId;
        this.manufactureDate = manufactureDate;
        this.expireDate = expireDate;
        this.itemName = itemName;
        this.itemType = itemType;
        this.expired = expired;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
        this.weight = weight;
        this.contactInfo = contactInfo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public ZonedDateTime getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(ZonedDateTime manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public ZonedDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(ZonedDateTime expireDate) {
        this.expireDate = expireDate;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public Boolean getExpired() {
        return expired;
    }

    public void setExpired(Boolean expired) {
        this.expired = expired;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }
    
    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Stock other = (Stock) obj;
        if (accountId == null) {
            if (other.accountId != null)
                return false;
        } else if (!accountId.equals(other.accountId))
            return false;
        if (expireDate == null) {
            if (other.expireDate != null)
                return false;
        } else if (!expireDate.equals(other.expireDate))
            return false;
        if (expired == null) {
            if (other.expired != null)
                return false;
        } else if (!expired.equals(other.expired))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (itemName == null) {
            if (other.itemName != null)
                return false;
        } else if (!itemName.equals(other.itemName))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (manufactureDate == null) {
            if (other.manufactureDate != null)
                return false;
        } else if (!manufactureDate.equals(other.manufactureDate))
            return false;
        if (itemType == null) {
            if (other.itemType != null)
                return false;
        } else if (!itemType.equals(other.itemType))
            return false;
        if (price == null) {
            if (other.price != null)
                return false;
        } else if (!price.equals(other.price))
            return false;
        if (quantity == null) {
            if (other.quantity != null)
                return false;
        } else if (!quantity.equals(other.quantity))
            return false;    
        if (weight == null) {
            if (other.weight != null)
                return false;
        } else if (!weight.equals(other.weight))
            return false;   
        if (contactInfo == null) {
            if (other.contactInfo != null)
                return false;
        } else if (!contactInfo.equals(other.contactInfo))
            return false;   
        return true;
    }
}
