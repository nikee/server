package com.nikee.universal.entity.client;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mongodb.lang.NonNull;
import com.nikee.universal.entity.Account;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "client")
public class Client implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -9203093528251647135L;
    @Id
    private String id;
    @Indexed(unique = false)
    private String companyId;
    private ZonedDateTime createdTimeStamp; // when was this record created
    private String inCharge; // the staff in charge
    @Indexed(unique = false)
    private String firstName;
    @Indexed(unique = false)
    private String lastName;
    @Indexed(unique = false)
    private String email;
    @Indexed(unique = false)
    private String phone;
    private List<Note> notes;
    private List<Action> actions;
    private String category;

    /**
    * 
    */
    public Client() {
        this.notes = new ArrayList<>();
        this.actions = new ArrayList<>();
        this.createdTimeStamp = ZonedDateTime.now();
    }

    /**
     * Mark createTime as now
     * 
     * @return this
     */
    public Client markTimeStamp() {
        this.createdTimeStamp = ZonedDateTime.now();
        return this;
    }

    /***
     * attempt to change one Note in the list. LastModifiedAt and LastModifiedBy
     * will be automatically changed.
     * 
     * @param index    index of the note in the list
     * @param modifier who is trying to modify. Will be marked in the lastModifiedBy
     *                 field.
     * @param sumary   The new text
     * @return true if the change is successful, or false otherwise.
     */
    public boolean changeNote(int index, @NonNull Account modifier, String sumary) {
        if (this.notes.size() <= index) // when size = 10, last index should be 9. index should be no bigger or equals
                                        // to size
            return false;
        ZonedDateTime now = ZonedDateTime.now();
        Note note = this.notes.get(index);
        note.setLastModifiedAt(now);
        note.setLastModifiedBy(modifier.getId());
        note.setSummary(sumary);
        this.notes.set(index, note);
        return true;
    }

    public boolean deleteNote(int index) {
        try {
            this.notes.remove(index);
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
        return true;
    }

    /**
     * Add a new Note into the note list.
     * 
     * @param creator the creator of the note
     * @param summary
     * @return
     */
    public Client addNote(@NonNull Account creator, String summary) {
        ZonedDateTime now = ZonedDateTime.now();
        Note note = new Note();
        note.setLastModifiedBy(creator.getId());
        note.setLastModifiedAt(now);
        note.setCreatedAt(now);
        note.setSummary(email);
        this.notes.add(note);
        return this;
    }

    public Client assignAction(@NonNull Account who, String description, List<Account> toWho, ZonedDateTime deadline) {
        Map<String, Boolean> map = new HashMap<>();
        toWho.stream().forEach(a -> map.put(a.getId(), Boolean.valueOf(false)));
        Action action = new Action(description, map, deadline, who.getId());
        this.actions.add(action);
        return this;
    }

    public boolean deleteAction(int index) {
        try {
            this.actions.remove(index);
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
        return true;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the createdTimeStamp
     */
    public ZonedDateTime getCreatedTimeStamp() {
        return createdTimeStamp;
    }

    /**
     * @param createdTimeStamp the createdTimeStamp to set
     */
    public void setCreatedTimeStamp(ZonedDateTime createdTimeStamp) {
        this.createdTimeStamp = createdTimeStamp;
    }

    /**
     * @return the inCharge
     */
    public String getInCharge() {
        return inCharge;
    }

    /**
     * @param inCharge the inCharge to set
     */
    public void setInCharge(String inCharge) {
        this.inCharge = inCharge;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName.trim();
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName.trim();
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email.trim();
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone.trim();
    }

    /**
     * @return the notes
     */
    public List<Note> getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    /**
     * @return the actions
     */
    public List<Action> getActions() {
        return actions;
    }

    /**
     * @param actions the actions to set
     */
    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Client))
            return false;
        Client other = (Client) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the companyId
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId the companyId to set
     */
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


}