package com.nikee.universal.entity.client;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * This class defines
 */
public class Action implements Serializable {
    /**
     *
     */
    private String id;
    private static final long serialVersionUID = 4647948268532467815L;
    private String description; // description of what to do
    private Map<String, Boolean> checkList; // person involved and if they have finished
    private ZonedDateTime deadline; // deadline of the task
    private String assignedBy; // who created this assignment

    /***
     * tick off the checkbox of a person. Tolerate if the person is not in the map
     * or has already ticked off
     * 
     * @param who
     */
    public void tick(String who) {
        this.checkList.replace(who, Boolean.valueOf(true));
    }

    public void unTick(String who) {
        this.checkList.replace(who, Boolean.valueOf(false));
    }

    /**
     * Add someone into the checklist with a False value for this checkbox.
     * 
     * @param who
     * @return
     */
    public Action enlist(String who) {
        this.checkList.putIfAbsent(who, Boolean.valueOf(false));
        return this;
    }

    /**
     * Check if this action is overdue. Will return false(not overdue) if all
     * checkboxex have been checked.
     * 
     * @return True if it is overdue, or false otherwise.
     */
    public boolean isOverDued() {
        if (this.isAllFinished()) // if all checkboxs have been ticked, there is no need to proceed the check
                                  // anymore
            return false;
        ZonedDateTime now = ZonedDateTime.now();
        return now.isAfter(deadline);
    }

    /**
     * check if all checkboxex have been checked.
     * 
     * @return
     */
    public boolean isAllFinished() {
        return this.checkList.values().stream().allMatch(b -> b.booleanValue() == true);
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the checkList
     */
    public Map<String, Boolean> getCheckList() {
        return checkList;
    }

    /**
     * @param checkList the checkList to set
     */
    public void setCheckList(Map<String, Boolean> checkList) {
        this.checkList = checkList;
    }

    /**
     * @return the deadline
     */
    public ZonedDateTime getDeadline() {
        return deadline;
    }

    /**
     * @param deadline the deadline to set
     */
    public void setDeadline(ZonedDateTime deadline) {
        this.deadline = deadline;
    }

    /**
     * @param description
     * @param checkList
     * @param deadline
     * @param assignedBy
     */
    public Action(String description, Map<String, Boolean> checkList, ZonedDateTime deadline, String assignedBy) {
        this.description = description;
        this.checkList = checkList;
        this.deadline = deadline;
        this.assignedBy = assignedBy;
    }

    /**
     * @param description
     * @param deadline
     * @param assignedBy
     */
    public Action(String description, ZonedDateTime deadline, String assignedBy) {
        this.description = description;
        this.deadline = deadline;
        this.checkList = new HashMap<>();
        this.assignedBy = assignedBy;
    }

    /**
     * 
     */
    public Action() {
        this.checkList = new HashMap<>();
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the assignedBy
     */
    public String getAssignedBy() {
        return assignedBy;
    }

    /**
     * @param assignedBy the assignedBy to set
     */
    public void setAssignedBy(String assignedBy) {
        this.assignedBy = assignedBy;
    }

    /**
     * equals methond taking checkList into consideration
     * 
     * @param obj
     * @return
     */
    public boolean equalsIncludeCheckList(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Action))
            return false;
        Action other = (Action) obj;
        if (assignedBy == null) {
            if (other.assignedBy != null)
                return false;
        } else if (!assignedBy.equals(other.assignedBy))
            return false;
        if (deadline == null) {
            if (other.deadline != null)
                return false;
        } else if (!deadline.isEqual(other.deadline))
            return false;
        if (checkList == null) {
            if (other.checkList != null)
                return false;
        } else if (!checkList.equals(other.checkList))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        return true;

    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Action))
            return false;
        Action other = (Action) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

}