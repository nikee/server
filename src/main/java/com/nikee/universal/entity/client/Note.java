package com.nikee.universal.entity.client;

import java.io.Serializable;
import java.time.ZonedDateTime;

public class Note implements Serializable {
    /**
     *
     */
    private String id;
    private static final long serialVersionUID = -3028762119869687909L;
    private ZonedDateTime createdAt; // when was this record created
    private ZonedDateTime lastModifiedAt; // when was this record last modified.
    private String lastModifiedBy; // who was the last person modifying this record. If never modified, this would
                                   // be the creator.
    private String summary; // string text summary of the event

    /**
     * 
     */
    public Note() {
        this.createdAt = ZonedDateTime.now();
        this.lastModifiedAt = createdAt;
    }

    /**
     * @param createdAt
     * @param lastModifiedAt
     * @param lastModifiedBy
     * @param summary
     */
    public Note(ZonedDateTime createdAt, ZonedDateTime lastModifiedAt, String lastModifiedBy, String summary) {
        this.createdAt = createdAt;
        this.lastModifiedAt = lastModifiedAt;
        this.lastModifiedBy = lastModifiedBy;
        this.summary = summary;
    }

    /**
     * @return the createdAt
     */
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the lastModifiedAt
     */
    public ZonedDateTime getLastModifiedAt() {
        return lastModifiedAt;
    }

    /**
     * @param lastModifiedAt the lastModifiedAt to set
     */
    public void setLastModifiedAt(ZonedDateTime lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }

    /**
     * @return the lastModifiedBy
     */
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    /**
     * @param lastModifiedBy the lastModifiedBy to set
     */
    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    /**
     * @return the summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     * @param summary the summary to set
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Note))
            return false;
        Note other = (Note) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    /**
     * @param lastModifiedBy
     * @param summary
     */
    public Note(String lastModifiedBy, String summary) {
        this.lastModifiedBy = lastModifiedBy;
        this.summary = summary;
        this.lastModifiedAt = ZonedDateTime.now();
        this.createdAt = ZonedDateTime.now();
    }

}