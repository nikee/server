package com.nikee.universal.entity.license;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class License implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 5473956013796990435L;
    @Id
    private String id;
    private String grantTo; // company ID
    private ZonedDateTime expireDateTime;
    private List<Extension> purchaseHistory;

    /**
     * check if the license has expired
     * 
     * @return true if expire date is null or is before current time
     */
    public boolean isExpired() {
        if (expireDateTime == null)
            return true;
        return expireDateTime.isBefore(ZonedDateTime.now());

    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the grantTo
     */
    public String getGrantTo() {
        return grantTo;
    }

    /**
     * @param grantTo the grantTo to set
     */
    public void setGrantTo(String grantTo) {
        this.grantTo = grantTo;
    }

    /**
     * @return the expireDateTime
     */
    public ZonedDateTime getExpireDateTime() {
        return expireDateTime;
    }

    /**
     * @param expireDateTime the expireDateTime to set
     */
    public void setExpireDateTime(ZonedDateTime expireDateTime) {
        this.expireDateTime = expireDateTime;
    }

    /**
     * @return the purchaseHistory
     */
    public List<Extension> getPurchaseHistory() {
        return purchaseHistory;
    }

    /**
     * @param purchaseHistory the purchaseHistory to set
     */
    public void setPurchaseHistory(List<Extension> purchaseHistory) {
        this.purchaseHistory = purchaseHistory;
    }

    /**
     * Add a purchase history into the record
     * 
     * @param extension
     */
    public void addPurchase(Extension extension) {
        this.purchaseHistory.add(extension);
    }

    /**
     * 
     */
    public License() {
        this.purchaseHistory = new ArrayList<>();
    }
}