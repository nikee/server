package com.nikee.universal.entity.license;

import java.io.Serializable;
import java.time.ZonedDateTime;

public class Extension implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8330038449540836201L;
    private ZonedDateTime extendTo;
    private ZonedDateTime operationDate;

    /**
     * @return the extendTo
     */
    public ZonedDateTime getExtendTo() {
        return extendTo;
    }

    /**
     * @param extendTo the extendTo to set
     */
    public void setExtendTo(ZonedDateTime extendTo) {
        this.extendTo = extendTo;
    }

    /**
     * @return the operationDate
     */
    public ZonedDateTime getOperationDate() {
        return operationDate;
    }

    /**
     * @param operationDate the operationDate to set
     */
    public void setOperationDate(ZonedDateTime operationDate) {
        this.operationDate = operationDate;
    }

    /**
     * 
     */
    public Extension() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((extendTo == null) ? 0 : extendTo.hashCode());
        result = prime * result + ((operationDate == null) ? 0 : operationDate.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Extension other = (Extension) obj;
        if (extendTo == null) {
            if (other.extendTo != null)
                return false;
        } else if (!extendTo.equals(other.extendTo))
            return false;
        if (operationDate == null) {
            if (other.operationDate != null)
                return false;
        } else if (!operationDate.equals(other.operationDate))
            return false;
        return true;
    }

    /**
     * @param extendTo
     * @param operationDate
     */
    public Extension(ZonedDateTime extendTo, ZonedDateTime operationDate) {
        this.extendTo = extendTo;
        this.operationDate = operationDate;
    }
}