package com.nikee.universal.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.nikee.universal.entity.account.CalendarEvent;
import com.nikee.universal.entity.account.Events;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.crypto.bcrypt.BCrypt;

@Document(collection = "account")
@CompoundIndexes({ @CompoundIndex(name = "full_name", def = "{'firstName':1,'lastName':1}") })
public class Account implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2718828982383271327L;
    @Id
    private String id;
    private String firstName;
    private String lastName;
    private String password;
    @Indexed
    private int level;
    // lv1 -> 4: ROOT, owner, manager(of one company), common employees
    private String company;
    @Transient
    private Company companyBody;
    private String mobile;
    @DBRef
    @Indexed
    private Account employer; // who is your boss?
    @Indexed(unique = true)
    private String email;
    private String secondaryEmail;
    private String secondaryContactNumber;
    @Indexed
    private boolean discarded; // true if this account is discarded
    private List<Events> calendar;
    private List<CalendarEvent> calendarEvents;

    public static String hashPassword(String plainPassword) {
        return BCrypt.hashpw(plainPassword, BCrypt.gensalt());
    }

    /**
     * 
     * @param hashed
     * @param plain
     * @return reutrn true if matches, or false otherwise
     */
    public static boolean validatePassword(String hashed, String plain) {
        return BCrypt.checkpw(plain, hashed);
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public Account setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public Account setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public Account setPassword(String password) {
        this.password = password;
        return this;
    }

    /**
     * @return the level
     */
    public int getLevel() {
        return level;
    }

    /**
     * @param level the level to set
     */
    public Account setLevel(int level) {
        this.level = level;
        return this;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public Account setCompany(String company) {
        this.company = company;
        return this;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public Account setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public Account setEmail(String email) {
        this.email = email;
        return this;
    }

    /**
     * @param firstName
     * @param lastName
     * @param password
     * @param level
     * @param company
     * @param mobile
     * @param email
     */
    public Account(String firstName, String lastName, String password, int level, String company, String mobile,
            String email) {
        this();
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.level = level;
        this.company = company;
        this.mobile = mobile;
        this.email = email;
    }

    /**
     * 
     */
    public Account() {
        this.calendar = new ArrayList<>();
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the secondaryEmail
     */
    public String getSecondaryEmail() {
        return secondaryEmail;
    }

    /**
     * @param secondaryEmail the secondaryEmail to set
     */
    public Account setSecondaryEmail(String secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
        return this;
    }

    /**
     * @return the secondaryContactNumber
     */
    public String getSecondaryContactNumber() {
        return secondaryContactNumber;
    }

    /**
     * @param secondaryContactNumber the secondaryContactNumber to set
     */
    public Account setSecondaryContactNumber(String secondaryContactNumber) {
        this.secondaryContactNumber = secondaryContactNumber;
        return this;
    }

    /**
     * @return the discarded
     */
    public boolean isDiscarded() {
        return discarded;
    }

    /**
     * @param discarded the discarded to set
     */
    public Account setDiscarded(boolean discarded) {
        this.discarded = discarded;
        return this;
    }

    /**
     * @return the employer
     */
    public Account getEmployer() {
        return employer;
    }

    /**
     * @param employer the employer to set
     */
    public Account setEmployer(Account employer) {
        this.employer = employer;
        return this;
    }

    @PersistenceConstructor
    /**
     * @param id
     * @param firstName
     * @param lastName
     * @param password
     * @param level
     * @param company
     * @param mobile
     * @param employer
     * @param email
     * @param secondaryEmail
     * @param secondaryContactNumber
     * @param discarded
     */
    public Account(String id, String firstName, String lastName, String password, int level, String company,
            String mobile, Account employer, String email, String secondaryEmail, String secondaryContactNumber,
            boolean discarded) {
        this();
        this.setId(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.level = level;
        this.company = company;
        this.mobile = mobile;
        this.employer = employer;
        this.email = email;
        this.secondaryEmail = secondaryEmail;
        this.secondaryContactNumber = secondaryContactNumber;
        this.discarded = discarded;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Account other = (Account) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    /**
     * @return the calendar
     */
    public List<Events> getCalendar() {
        return calendar;
    }

    /**
     * @param calendar the calendar to set
     */
    public void setCalendar(List<Events> calendar) {
        this.calendar = calendar;
    }

    /**
     * @return the companyBody
     */
    public Company getCompanyBody() {
        return companyBody;
    }

    /**
     * @param companyBody the companyBody to set
     */
    public Account setCompanyBody(Company companyBody) {
        this.companyBody = companyBody;
        return this;
    }

    

    public List<CalendarEvent> getCalendarEvents() {
        return calendarEvents;
    }

    public void setCalendarEvents(List<CalendarEvent> calendarEvents) {
        this.calendarEvents = calendarEvents;
    }

}