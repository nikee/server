package com.nikee.universal.entity.account;

import java.io.Serializable;
import java.time.ZonedDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document("expenses")
public class Expenses implements Serializable{


    private static final long serialVersionUID = 6206891913965850087L;
    @Id
    private String id;
    private String item;
    private Double amount;
    private ZonedDateTime date;
    private String madeBy; //this is the employee who made the expense 
    private String enteredBy; //this is the employee who entered the record into the system
    private String company;
    private String clientId;//could be internal employee if the expense is a wage
    private int hours;//optional, remove if need
    private Double gst;
    private String notes;

    public Expenses(String id, String item, Double amount, ZonedDateTime date, String madeBy, String enteredBy,
            String company, String clientId, int hours, Double gst, String notes) {
        this.id = id;
        this.item = item;
        this.amount = amount;
        this.date = date;
        this.madeBy = madeBy;
        this.enteredBy = enteredBy;
        this.company = company;
        this.clientId = clientId;
        this.hours = hours;
        this.gst = gst;
        this.notes = notes;
    }

    

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public String getMadeBy() {
        return madeBy;
    }

    public void setMadeBy(String madeBy) {
        this.madeBy = madeBy;
    }

    public String getEnteredBy() {
        return enteredBy;
    }

    public void setEnteredBy(String enteredBy) {
        this.enteredBy = enteredBy;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public Double getGst() {
        return gst;
    }

    public void setGst(Double gst) {
        this.gst = gst;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((amount == null) ? 0 : amount.hashCode());
        result = prime * result + ((clientId == null) ? 0 : clientId.hashCode());
        result = prime * result + ((company == null) ? 0 : company.hashCode());
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((enteredBy == null) ? 0 : enteredBy.hashCode());
        result = prime * result + ((gst == null) ? 0 : gst.hashCode());
        result = prime * result + hours;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((item == null) ? 0 : item.hashCode());
        result = prime * result + ((madeBy == null) ? 0 : madeBy.hashCode());
        result = prime * result + ((notes == null) ? 0 : notes.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Expenses other = (Expenses) obj;
        if (amount == null) {
            if (other.amount != null)
                return false;
        } else if (!amount.equals(other.amount))
            return false;
        if (clientId == null) {
            if (other.clientId != null)
                return false;
        } else if (!clientId.equals(other.clientId))
            return false;
        if (company == null) {
            if (other.company != null)
                return false;
        } else if (!company.equals(other.company))
            return false;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (enteredBy == null) {
            if (other.enteredBy != null)
                return false;
        } else if (!enteredBy.equals(other.enteredBy))
            return false;
        if (gst == null) {
            if (other.gst != null)
                return false;
        } else if (!gst.equals(other.gst))
            return false;
        if (hours != other.hours)
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (item == null) {
            if (other.item != null)
                return false;
        } else if (!item.equals(other.item))
            return false;
        if (madeBy == null) {
            if (other.madeBy != null)
                return false;
        } else if (!madeBy.equals(other.madeBy))
            return false;
        if (notes == null) {
            if (other.notes != null)
                return false;
        } else if (!notes.equals(other.notes))
            return false;
        return true;
    }

    public Expenses() {
    }

    


    
    

   
    






    
}