package com.nikee.universal.entity.account.Message;

import java.io.Serializable;
import java.time.ZonedDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Envelope implements Serializable {
    /**
    	 *
    	 */
    private static final long serialVersionUID = -3577275321036188101L;
    @Id
    private String id;
    private String from;
    private String to;
    private String domain; // identify the companyId
    private ZonedDateTime when;
    private String title;
    @Transient
    private Msg body;
    private String bodyId;
    private int flag;
    private boolean unread;

    public static int PRIVATE = 1; // private message, one to one
    public static int PUBLIC = 2; // public message, company scope broadcast
    public static int GLOBAL = 3; // system message, broadcast to everyone

    /**
     * 
     */
    public Envelope() {
        unread = true;
    }

    public Envelope setPrivate() {
        this.flag = PRIVATE;
        return this;
    }

    /**
     * 
     * @param to send to
     * @return
     */
    public Envelope setPrivate(String to) {
        setPrivate();
        this.to = to;
        return this;
    }

    public Envelope setPublic() {
        this.flag = PUBLIC;
        return this;
    }

    /**
     * 
     * @param companyID
     * @return
     */
    public Envelope setPublic(String companyID) {
        setPublic();
        this.domain = companyID;
        return this;
    }

    public Envelope setGlobal() {
        this.flag = GLOBAL;
        return this;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return the when
     */
    public ZonedDateTime getWhen() {
        return when;
    }

    /**
     * @param when the when to set
     */
    public void setWhen(ZonedDateTime when) {
        this.when = when;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the flag
     */
    public int getFlag() {
        return flag;
    }

    /**
     * @param flag the flag to set
     */
    public void setFlag(int flag) {
        this.flag = flag;
    }

    /**
     * @return the unread
     */
    public boolean isUnread() {
        return unread;
    }

    /**
     * @param unread the unread to set
     */
    public void setUnread(boolean unread) {
        this.unread = unread;
    }

    /**
     * @return the body
     */
    public Msg getBody() {
        return body;
    }

    /**
     * @param body the body to set
     */
    public void setBody(Msg body) {
        this.body = body;
    }

    /**
     * @return the bodyId
     */
    public String getBodyId() {
        return bodyId;
    }

    /**
     * @param bodyId the bodyId to set
     */
    public void setBodyId(String bodyId) {
        this.bodyId = bodyId;
    }

    /**
     * @return the domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain the domain to set
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((body == null) ? 0 : body.hashCode());
        result = prime * result + ((bodyId == null) ? 0 : bodyId.hashCode());
        result = prime * result + ((domain == null) ? 0 : domain.hashCode());
        result = prime * result + flag;
        result = prime * result + ((from == null) ? 0 : from.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        result = prime * result + ((to == null) ? 0 : to.hashCode());
        result = prime * result + (unread ? 1231 : 1237);
        result = prime * result + ((when == null) ? 0 : when.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Envelope other = (Envelope) obj;
        if (body == null) {
            if (other.body != null)
                return false;
        } else if (!body.equals(other.body))
            return false;
        if (bodyId == null) {
            if (other.bodyId != null)
                return false;
        } else if (!bodyId.equals(other.bodyId))
            return false;
        if (domain == null) {
            if (other.domain != null)
                return false;
        } else if (!domain.equals(other.domain))
            return false;
        if (flag != other.flag)
            return false;
        if (from == null) {
            if (other.from != null)
                return false;
        } else if (!from.equals(other.from))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        if (to == null) {
            if (other.to != null)
                return false;
        } else if (!to.equals(other.to))
            return false;
        if (unread != other.unread)
            return false;
        if (when == null) {
            if (other.when != null)
                return false;
        } else if (!when.equals(other.when))
            return false;
        return true;
    }

}