package com.nikee.universal.entity.account.Message;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class MailList {
    @Id
    private String id;
    private Map<String, Boolean> inbox;

    /**
     * Add an envelope ID into mailbox
     * 
     * @param id the ID of envelope
     */
    public void add(String id) {
        this.inbox.put(id, true);
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the inbox
     */
    public Map<String, Boolean> getInbox() {
        return inbox;
    }

    /**
     * @param inbox the inbox to set
     */
    public void setInbox(Map<String, Boolean> inbox) {
        this.inbox = inbox;
    }

    /**
     * 
     */
    public MailList() {
        this.inbox = new HashMap<>();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MailList other = (MailList) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    /**
     * @param id
     */
    public MailList(String id) {
        this();
        this.id = id;
    }

}