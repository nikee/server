package com.nikee.universal.entity.account;

import java.io.Serializable;
import java.time.ZonedDateTime;


public class CalendarEvent implements Serializable{
   

    /**
     *serialVersionUID
     *This is used during the deserialization of an object,
     *to ensure that a loaded class is compatible with the serialized object. 
     *If no matching class is found, an InvalidClassException is thrown.
     *https://dzone.com/articles/what-is-serialversionuid
     */
    private static final long serialVersionUID = -7132262986519521486L;
    private String id;
    private String title;
    private ZonedDateTime start;
    private ZonedDateTime end;
    private ZonedDateTime createdAt;
    private ZonedDateTime lastModifiedAt;
    private String groupId;
    private String description;

    public CalendarEvent() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ZonedDateTime getStart() {
        return start;
    }

    public void setStart(ZonedDateTime start) {
        this.start = start;
    }

    public ZonedDateTime getEnd() {
        return end;
    }

    public void setEnd(ZonedDateTime end) {
        this.end = end;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getLastModifiedAt() {
        return lastModifiedAt;
    }

    public void setLastModifiedAt(ZonedDateTime lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }

    






}