package com.nikee.universal.entity.account;

import java.io.Serializable;

public class LatePaymentPolicy implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2250870028918973418L;
    // how many days is tolerated
    private int tolerateDate;
    private float amount;

    /**
     * @return the tolerateDate
     */
    public int getTolerateDate() {
        return tolerateDate;
    }

    /**
     * @param tolerateDate the tolerateDate to set
     */
    public void setTolerateDate(int tolerateDate) {
        this.tolerateDate = tolerateDate;
    }

    /**
     * @return the amount
     */
    public float getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(float amount) {
        this.amount = amount;
    }

    /**
     * 
     */
    public LatePaymentPolicy() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Float.floatToIntBits(amount);
        result = prime * result + tolerateDate;
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof LatePaymentPolicy))
            return false;
        LatePaymentPolicy other = (LatePaymentPolicy) obj;
        if (Float.floatToIntBits(amount) != Float.floatToIntBits(other.amount))
            return false;
        if (tolerateDate != other.tolerateDate)
            return false;
        return true;
    }
}