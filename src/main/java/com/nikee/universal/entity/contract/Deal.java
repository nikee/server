package com.nikee.universal.entity.contract;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import com.nikee.universal.entity.account.Tax;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("deal")
public class Deal {
    @Transient
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Id
    private String id;
    @Transient
    private String id_hex;
    @Indexed
    private String client;
    @Indexed
    private String service;
    private float proposal;
    private float adjustment;
    private float depositRate;
    private float onceOff;
    private List<Installment> installments;
    private String note;
    @Transient
    private float sum;
    private String createdBy;
    private String lastModifiedBy;
    private ZonedDateTime lastModifiedAt;
    private ZonedDateTime issuedAt;
    private Refund refund;
    private Tax appliedTax;

    /**
     * init a refund, do necessary change to both the refund and the deal
     * 
     * @param who
     * @return
     */
    public Deal initRefund(String who) {
        ZonedDateTime now = ZonedDateTime.now();
        this.markChange(who, now);
        this.refund.init(who, now);
        return this;
    }

    public Deal init(String who) {
        ZonedDateTime now = ZonedDateTime.now();
        this.createdBy = who;
        this.issuedAt = now;
        this.markChange(who, now);
        return this;
    }

    /**
     * Do refund (if exist)
     * 
     * @param who
     * @param when when was the payment made
     */
    public Deal doRefund(String who, ZonedDateTime when) {
        if (refund != null) {
            refund.pay(who, when);
            markChange(who);
        }

        return this;
    }

    public Deal markChange(String who) {
        ZonedDateTime now = ZonedDateTime.now();
        this.lastModifiedBy = who;
        this.lastModifiedAt = now;
        return this;
    }

    public Deal markChange(String who, ZonedDateTime now) {
        this.lastModifiedBy = who;
        this.lastModifiedAt = now;
        return this;
    }

    /**
     * validate if the data violate basic constraint
     * 
     * @return return true if passed the check, otherwise false.
     */
    private boolean validate() {
        // proposal >0
        if (proposal < 0)
            return false;

        // proposal + adjustment > 0
        if (proposal + adjustment < 0)
            return false;
        // 0 <= deposit Rate <=1
        if (depositRate < 0 || depositRate > 1)
            return false;
        // onceOff >=0
        if (onceOff < 0)
            return false;
        // onceOff
        // each installment >0
        if (installments.stream().anyMatch(i -> i.getAmount() <= 0))
            return false;
        if (refund != null) {
            if (refund.getAmount() > proposal + adjustment || refund.getAmount() <= 0)
                return false;
        }

        return true;
    }

    /**
     * check if the installment plan plus once off payment plus deposit matches the
     * proposal amount plus adjustment. If the data violate data constraint(using
     * method validate()), then return false.
     * 
     * Passing this test means the deal is a logical one.
     * 
     * @return true if the balance is 0
     */
    public boolean isBalanced() {
        if (!validate())
            return false;
        logger.debug("passed validator");
        sum = 0;
        this.installments.stream().forEach(i -> sum += i.getAmount());
        return sum + onceOff == (proposal + adjustment) * (1 - depositRate);
    }

    public float getInstallmentSum() {
        sum = 0;
        this.installments.stream().forEach(i -> sum += i.getAmount());
        return sum;
    }

    /**
     * get the installment amount remaining unpaid, including not due and overdue
     * 
     * @return
     */
    public float getPendingInstallmentAmount() {
        sum = 0;
        this.installments.stream().filter(i -> i.getPaymentDate() == null).forEach(i -> sum += i.getAmount());
        return sum;
    }

    /**
     * Confirm that one installment payment has been made
     * 
     * @param index index of installment
     * @param date  when was it made
     * @param who   confirmed by who
     */
    public Deal confirm(int index, ZonedDateTime date, String who) {
        Installment i = this.installments.get(index);
        i.confirm(date, who);
        this.installments.set(index, i);
        markChange(who);
        return this;

    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the client
     */
    public String getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * @return the service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service the service to set
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return the proposal
     */
    public float getProposal() {
        return proposal;
    }

    /**
     * @param proposal the proposal to set
     */
    public void setProposal(float proposal) {
        this.proposal = proposal;
    }

    /**
     * @return the adjustment
     */
    public float getAdjustment() {
        return adjustment;
    }

    /**
     * @param adjustment the adjustment to set
     */
    public void setAdjustment(float adjustment) {
        this.adjustment = adjustment;
    }

    /**
     * @return the depositRate
     */
    public float getDepositRate() {
        return depositRate;
    }

    /**
     * @param depositRate the depositRate to set
     */
    public void setDepositRate(float depositRate) {
        this.depositRate = depositRate;
    }

    /**
     * @return the onceOff
     */
    public float getOnceOff() {
        return onceOff;
    }

    /**
     * @param onceOff the onceOff to set
     */
    public void setOnceOff(float onceOff) {
        this.onceOff = onceOff;
    }

    /**
     * @return the installments
     */
    public List<Installment> getInstallments() {
        return installments;
    }

    /**
     * @param installments the installments to set
     */
    public void setInstallments(List<Installment> installments) {
        this.installments = installments;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * 
     */
    public Deal() {
        this.installments = new ArrayList<>();
    }

    /**
     * compare all field except refund
     */
    public boolean isChanged(Deal other) {
        if (Float.floatToIntBits(adjustment) != Float.floatToIntBits(other.adjustment))
            return false;
        if (client == null) {
            if (other.client != null)
                return false;
        } else if (!client.equals(other.client))
            return false;
        if (createdBy == null) {
            if (other.createdBy != null)
                return false;
        } else if (!createdBy.equals(other.createdBy))
            return false;
        if (Float.floatToIntBits(depositRate) != Float.floatToIntBits(other.depositRate))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (installments == null) {
            if (other.installments != null)
                return false;
        } else if (!installments.equals(other.installments))
            return false;
        if (issuedAt == null) {
            if (other.issuedAt != null)
                return false;
        } else if (!issuedAt.isEqual(other.issuedAt))
            return false;
        if (lastModifiedAt == null) {
            if (other.lastModifiedAt != null)
                return false;
        } else if (!lastModifiedAt.isEqual(other.lastModifiedAt))
            return false;
        if (lastModifiedBy == null) {
            if (other.lastModifiedBy != null)
                return false;
        } else if (!lastModifiedBy.equals(other.lastModifiedBy))
            return false;
        if (logger == null) {
            if (other.logger != null)
                return false;
        } else if (!logger.equals(other.logger))
            return false;
        if (note == null) {
            if (other.note != null)
                return false;
        } else if (!note.equals(other.note))
            return false;
        if (Float.floatToIntBits(onceOff) != Float.floatToIntBits(other.onceOff))
            return false;
        if (Float.floatToIntBits(proposal) != Float.floatToIntBits(other.proposal))
            return false;
        if (service == null) {
            if (other.service != null)
                return false;
        } else if (!service.equals(other.service))
            return false;
        return true;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the issuedAt
     */
    public ZonedDateTime getIssuedAt() {
        return issuedAt;
    }

    /**
     * @param issuedAt the issuedAt to set
     */
    public void setIssuedAt(ZonedDateTime issuedAt) {
        this.issuedAt = issuedAt;
    }

    /**
     * @return the sum
     */
    public float getSum() {
        return sum;
    }

    /**
     * @param sum the sum to set
     */
    public void setSum(float sum) {
        this.sum = sum;
    }

    /**
     * @return the lastModifiedBy
     */
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    /**
     * @param lastModifiedBy the lastModifiedBy to set
     */
    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    /**
     * @return the lastModifiedAt
     */
    public ZonedDateTime getLastModifiedAt() {
        return lastModifiedAt;
    }

    /**
     * @param lastModifiedAt the lastModifiedAt to set
     */
    public void setLastModifiedAt(ZonedDateTime lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }

    /**
     * @return the refund
     */
    public Refund getRefund() {
        return refund;
    }

    /**
     * @param refund the refund to set
     */
    public void setRefund(Refund refund) {
        this.refund = refund;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Deal))
            return false;
        Deal other = (Deal) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    /**
     * @return the appliedTax
     */
    public Tax getAppliedTax() {
        return appliedTax;
    }

    /**
     * @param appliedTax the appliedTax to set
     */
    public void setAppliedTax(Tax appliedTax) {
        this.appliedTax = appliedTax;
    }

}