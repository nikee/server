package com.nikee.universal.entity.contract;

import java.time.ZonedDateTime;

public class Installment {

    // amount to pay for this installment
    private float amount;
    // when is this due
    private ZonedDateTime dueDate;
    // actual date it is paid
    private ZonedDateTime paymentDate;
    // account that confirmed the payment has been made
    private String confirmedBy;

    protected void confirm(ZonedDateTime date, String who) {
        this.paymentDate = date;
        this.confirmedBy = who;
    }

    /**
     * @return the amount
     */
    public float getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(float amount) {
        this.amount = amount;
    }

    /**
     * @return the dueDate
     */
    public ZonedDateTime getDueDate() {
        return dueDate;
    }

    /**
     * @param dueDate the dueDate to set
     */
    public void setDueDate(ZonedDateTime dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * @return the paymentDate
     */
    public ZonedDateTime getPaymentDate() {
        return paymentDate;
    }

    /**
     * @param paymentDate the paymentDate to set
     */
    public void setPaymentDate(ZonedDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     * @return the confirmedBy
     */
    public String getConfirmedBy() {
        return confirmedBy;
    }

    /**
     * @param confirmedBy the confirmedBy to set
     */
    public void setConfirmedBy(String confirmedBy) {
        this.confirmedBy = confirmedBy;
    }

    /**
     * 
     */
    public Installment() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Float.floatToIntBits(amount);
        result = prime * result + ((dueDate == null) ? 0 : dueDate.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Installment))
            return false;
        Installment other = (Installment) obj;
        if (Float.floatToIntBits(amount) != Float.floatToIntBits(other.amount))
            return false;
        if (dueDate == null) {
            if (other.dueDate != null)
                return false;
        } else if (!dueDate.isEqual(other.dueDate))
            return false;
        return true;
    }

}