package com.nikee.universal.entity.contract;

import java.time.ZonedDateTime;

public class Refund {

    public static boolean PAID = true;
    public static boolean PENDING = false;
    private boolean isPaid;
    private float amount;
    private String createdBy;
    private ZonedDateTime createdAt;
    private String lastModifiedBy;
    private ZonedDateTime lastModifiedAt;
    private ZonedDateTime paymentDate;

    protected void pay(String who) {
        pay(who, ZonedDateTime.now());
    }

    /**
     * mark the refund has been paid
     * 
     * @param who
     * @param now when the payment was made
     */
    protected void pay(String who, ZonedDateTime now) {
        this.isPaid = PAID;
        this.paymentDate = now;
        mark(who);
    }

    /**
     * mark a change has been made
     * 
     * @param who
     */
    protected void mark(String who) {
        mark(who, ZonedDateTime.now());
    }

    protected void mark(String who, ZonedDateTime now) {
        this.lastModifiedBy = who;
        this.lastModifiedAt = now;
    }

    /**
     * set up init status of refund
     * 
     * @param who
     * @return
     */
    public Refund init(String who) {
        ZonedDateTime now = ZonedDateTime.now();
        return init(who, now);
    }

    public Refund init(String who, ZonedDateTime now) {
        this.createdAt = now;
        this.isPaid = PENDING;
        this.createdBy = who;
        mark(who);
        return this;
    }

    /**
     * @return the isPaid
     */
    public boolean isPaid() {
        return isPaid;
    }

    /**
     * @param isPaid the isPaid to set
     */
    public void setPaid(boolean isPaid) {
        this.isPaid = isPaid;
    }

    /**
     * @return the amount
     */
    public float getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(float amount) {
        this.amount = amount;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the createdAt
     */
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the lastModifiedBy
     */
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    /**
     * @param lastModifiedBy the lastModifiedBy to set
     */
    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    /**
     * @return the lastModifiedAt
     */
    public ZonedDateTime getLastModifiedAt() {
        return lastModifiedAt;
    }

    /**
     * @param lastModifiedAt the lastModifiedAt to set
     */
    public void setLastModifiedAt(ZonedDateTime lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }

    /**
     * @return the paymentDate
     */
    public ZonedDateTime getPaymentDate() {
        return paymentDate;
    }

    /**
     * @param paymentDate the paymentDate to set
     */
    public void setPaymentDate(ZonedDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Float.floatToIntBits(amount);
        result = prime * result + ((createdAt == null) ? 0 : createdAt.hashCode());
        result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
        result = prime * result + (isPaid ? 1231 : 1237);
        result = prime * result + ((lastModifiedAt == null) ? 0 : lastModifiedAt.hashCode());
        result = prime * result + ((lastModifiedBy == null) ? 0 : lastModifiedBy.hashCode());
        result = prime * result + ((paymentDate == null) ? 0 : paymentDate.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Refund))
            return false;
        Refund other = (Refund) obj;
        if (Float.floatToIntBits(amount) != Float.floatToIntBits(other.amount))
            return false;
        if (createdAt == null) {
            if (other.createdAt != null)
                return false;
        } else if (!createdAt.isEqual(other.createdAt))
            return false;
        if (createdBy == null) {
            if (other.createdBy != null)
                return false;
        } else if (!createdBy.equals(other.createdBy))
            return false;
        if (isPaid != other.isPaid)
            return false;
        if (lastModifiedAt == null) {
            if (other.lastModifiedAt != null)
                return false;
        } else if (!lastModifiedAt.isEqual(other.lastModifiedAt))
            return false;
        if (lastModifiedBy == null) {
            if (other.lastModifiedBy != null)
                return false;
        } else if (!lastModifiedBy.equals(other.lastModifiedBy))
            return false;
        if (paymentDate == null) {
            if (other.paymentDate != null)
                return false;
        } else if (!paymentDate.isEqual(other.paymentDate))
            return false;
        return true;
    }

    /***
     * equals ignore last modified date & person
     * 
     * @param obj
     * @return
     */
    public boolean equalsIgnoreLM(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Refund))
            return false;
        Refund other = (Refund) obj;
        if (Float.floatToIntBits(amount) != Float.floatToIntBits(other.amount))
            return false;
        if (createdAt == null) {
            if (other.createdAt != null)
                return false;
        } else if (!createdAt.isEqual(other.createdAt))
            return false;
        if (createdBy == null) {
            if (other.createdBy != null)
                return false;
        } else if (!createdBy.equals(other.createdBy))
            return false;
        if (isPaid != other.isPaid)
            return false;
        if (paymentDate == null) {
            if (other.paymentDate != null)
                return false;
        } else if (!paymentDate.isEqual(other.paymentDate))
            return false;
        return true;
    }

    /**
     * 
     */
    public Refund() {
    }

    /**
     * construct a Refund instance to be added
     * 
     * @param amount
     * @param createdBy
     */
    public Refund(float amount, String createdBy) {
        this.amount = amount;
        this.createdBy = createdBy;
        this.isPaid = PENDING;
        ZonedDateTime now = ZonedDateTime.now();
        this.createdAt = now;
        this.lastModifiedAt = now;
        this.lastModifiedBy = createdBy;
    }

}