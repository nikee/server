package com.nikee.universal.entity;

import java.io.Serializable;
import java.util.List;

import com.nikee.universal.entity.account.Branch;
import com.nikee.universal.entity.account.LatePaymentPolicy;
import com.nikee.universal.entity.account.Tax;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Company implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 7270099243759736933L;
    @Id
    private String id;
    private String businessName;
    private long ABN;
    private long ACN;
    private List<Branch> branches;
    private String logo;
    private String telephone;
    private String email;
    private String ownerName;
    private Tax tax;
    private LatePaymentPolicy policy;

    /**
     * 
     */
    public Company() {
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the businessName
     */
    public String getBusinessName() {
        return businessName;
    }

    /**
     * @param businessName the businessName to set
     */
    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    /**
     * @return the aBN
     */
    public long getABN() {
        return ABN;
    }

    /**
     * @param aBN the aBN to set
     */
    public void setABN(long aBN) {
        ABN = aBN;
    }

    /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param telephone the telephone to set
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the ownerName
     */
    public String getOwnerName() {
        return ownerName;
    }

    /**
     * @param ownerName the ownerName to set
     */
    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    /**
     * @return the policy
     */
    public LatePaymentPolicy getPolicy() {
        return policy;
    }

    /**
     * @param policy the policy to set
     */
    public void setPolicy(LatePaymentPolicy policy) {
        this.policy = policy;
    }

    /**
     * @return the tax
     */
    public Tax getTax() {
        return tax;
    }

    /**
     * @param tax the tax to set
     */
    public void setTax(Tax tax) {
        this.tax = tax;
    }

    /**
     * @return the aCN
     */
    public long getACN() {
        return ACN;
    }

    /**
     * @param aCN the aCN to set
     */
    public void setACN(long aCN) {
        ACN = aCN;
    }

    /**
     * @return the branches
     */
    public List<Branch> getBranches() {
        return branches;
    }

    /**
     * @param branches the branches to set
     */
    public void setBranches(List<Branch> branches) {
        this.branches = branches;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Company other = (Company) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

}