package com.nikee.universal.entity.employee;

import java.io.Serializable;

public class EmpAddress implements Serializable {
    
    /**
     *
     */
    private static final long serialVersionUID = -6840091977695630094L;
    private String unitNo;
    private String addressLine1;
    private String suburb;
    private int postCode;
    private String state;
    private String country;


    public EmpAddress(String unitNo, String addressLine1, String suburb, int postCode, String state, String country) {
        this.unitNo = unitNo;
        this.addressLine1 = addressLine1;
        this.suburb = suburb;
        this.postCode = postCode;
        this.state = state;
        this.country = country;
    }

    public String getUnitNo() {
        return unitNo;
    }

    public void setUnitNo(String unitNo) {
        this.unitNo = unitNo;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    

}