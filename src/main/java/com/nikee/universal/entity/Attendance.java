package com.nikee.universal.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "attendance")
public class Attendance implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    @Id
    private String accountId;
    private List<List<ZonedDateTime>> logTimeList;

    public Attendance(String accountId) {
        this.accountId = accountId;
        this.logTimeList = new ArrayList<>();
    }

    public String getAccountId() {
        return this.accountId;
    }

    public List<List<ZonedDateTime>> getLogTimeList() {
        return this.logTimeList;
    }

    public void setLogInTime(ZonedDateTime logInTime) {
        List<ZonedDateTime> logTime = new ArrayList<>();
        logTime.add(logInTime);
        this.logTimeList.add(logTime);
        if(logTimeList.size() > 20)
            logTimeList.remove(0);
    }

    public void setLogOutTime(ZonedDateTime logOutTime) {
        this.logTimeList.get(logTimeList.size()-1).add(logOutTime);
    }
}
