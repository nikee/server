package com.nikee.universal.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.nikee.universal.entity.account.Tax;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "services")
public class Services implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -461168623257461963L;
    @Id
    private String id;
    private String companyId;
    @Indexed
    private float price;
    @DBRef
    private Set<Services> subService;
    @DBRef
    private Set<Account> staffs;
    private String name;
    private List<String> locations;
    @Transient
    private Services parent;
    private Tax appliedTax;

    public Services() {
        this.subService = new HashSet<>();
        this.locations = new ArrayList<>();
        this.staffs = new HashSet<>();
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the price
     */
    public float getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(float price) {
        this.price = price;
    }

    /**
     * @return the subService
     */
    public Set<Services> getSubService() {
        return subService;
    }

    /**
     * @param subService the subService to set
     */
    public void setSubService(Set<Services> subService) {
        this.subService = subService;
    }

    /**
     * @return the staffs
     */
    public Set<Account> getStaffs() {
        return staffs;
    }

    /**
     * @param staffs the staffs to set
     */
    public void setStaffs(Set<Account> staffs) {
        this.staffs = staffs;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the locations
     */
    public List<String> getLocations() {
        return locations;
    }

    /**
     * @param locations the locations to set
     */
    public void setLocations(List<String> locations) {
        this.locations = locations;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the parent
     */
    public Services getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(Services parent) {
        this.parent = parent;
    }

    public boolean addSubService(Services toAdd) {
        return this.subService.add(toAdd);
    }

    public void clearSubService() {
        this.subService = new HashSet<>();
    }

    public boolean removeSubService(Services toRemove) {
        return this.subService.remove(toRemove);
    }
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Services))
            return false;
        Services other = (Services) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    /**
     * @return the appliedTax
     */
    public Tax getAppliedTax() {
        return appliedTax;
    }

    /**
     * @param appliedTax the appliedTax to set
     */
    public void setAppliedTax(Tax appliedTax) {
        this.appliedTax = appliedTax;
    }

    /**
     * @return the companyId
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId the companyId to set
     */
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

}