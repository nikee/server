package com.nikee.universal.entity.leave;

import java.io.Serializable;

public class LeaveType implements Serializable{

    private static final long serialVersionUID = -6307260887342607244L;

    private String id;
    private String type;

    public LeaveType(String id, String type) {
        this.id = id;
        this.type = type;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    


    
}