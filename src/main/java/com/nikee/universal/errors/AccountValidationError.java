package com.nikee.universal.errors;

public class AccountValidationError extends genericError {

    public AccountValidationError(String msg, int errorCode) {
        super(msg, errorCode);
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    

}