package com.nikee.universal.errors;

import com.nikee.universal.entity.license.License;

public class LicenseExpiredError extends genericError {

    private License ExpiredLicense;

    public LicenseExpiredError(String msg, int errorCode) {
        super(msg, errorCode);
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * @return the expiredLicense
     */
    public License getExpiredLicense() {
        return ExpiredLicense;
    }

    /**
     * @param expiredLicense the expiredLicense to set
     */
    public void setExpiredLicense(License expiredLicense) {
        ExpiredLicense = expiredLicense;
    }

    /**
     * @param msg
     * @param errorCode
     * @param expiredLicense
     */
    public LicenseExpiredError(String msg, int errorCode, License expiredLicense) {
        super(msg, errorCode);
        ExpiredLicense = expiredLicense;
    }

    public LicenseExpiredError(License expiredLicense) {
        super("License has expired.", genericError.LICENSE_EXPIRED);
        ExpiredLicense = expiredLicense;
    }

}