package com.nikee.universal.errors;

public class TransactionFailureException extends genericError {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public TransactionFailureException(String msg, int errorCode) {
        super(msg, errorCode);

    }

}