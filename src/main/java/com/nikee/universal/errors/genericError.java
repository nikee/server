package com.nikee.universal.errors;

public abstract class genericError extends RuntimeException {

    public final static int LEVEL_NUMBER_INVALID = -1;
    public final static int LEVEL_1_REQUIRED = 1;
    public final static int LEVEL_2_REQUIRED = 2;
    public final static int LEVEL_3_REQUIRED = 3;
    public final static int INCORRECT_PASSWORD = 6;
    public final static int INVALID_EMAIL = 4; // email format is not correct
    public final static int USER_NOT_FOUND = 5; // Cannot find any match of user from the database with given
                                                // cridentials
    public final static int SESSION_NOT_AUTHORIZED = 10; // you can not operate this session due to failure to pass some
                                                         // security checks.
    public final static int LICENSE_EXPIRED = 11;
    public final static int DATABASE_CHANGE_NOT_CONFIRMED = 100; // issue raised by UpdateResult.wasAcknoledged.
    public final static int ILLEGAL_ARGUMENT = 501;
    public final static int ILLEGAL_DOMAIN = 502; // trying to operate another business's data

    public final static int OK = 200;

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private int errorCode;

    public genericError(String msg, int errorCode) {
        super(msg);
        this.errorCode = errorCode;
    }

    /**
     * @return the errorCode
     */
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

}