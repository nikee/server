package com.nikee.universal.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "upload")
public class UploadSetting {

    private String linuxPath;
    private String windowsPath;
    private boolean enableWindows; // true -> windows


    private static String defReportPath = "C:\\Users\\Administrator\\Documents\\work\\Nikee\\Reports";
    // private static String defReportPath = "E:\\Nikee Temp Projects\\Reports";

    private static String defUploadPath = "C:\\Users\\Administrator\\Documents\\work\\Nikee\\Upload";
    // private static String defUploadPath = "E:\\Nikee Temp Projects";

    /**
     * @return the linuxPath
     */
    public String getLinuxPath() {
        return linuxPath;
    }

    /**
     * @param linuxPath the linuxPath to set
     */
    public void setLinuxPath(String linuxPath) {
        this.linuxPath = linuxPath;
    }

    /**
     * @return the windowsPath
     */
    public String getWindowsPath() {
        return windowsPath;
    }

    /**
     * @param windowsPath the windowsPath to set
     */
    public void setWindowsPath(String windowsPath) {
        this.windowsPath = windowsPath;
    }

    /**
     * @return the enableWindows
     */
    public boolean isEnableWindows() {
        return enableWindows;
    }

    /**
     * @param enableWindows the enableWindows to set
     */
    public void setEnableWindows(boolean enableWindows) {
        this.enableWindows = enableWindows;
    }

    /* Wenzel */
    public String getDefReportPath() {
        return defReportPath;
    }

    public String getDefUploadPath() {
        return defUploadPath;
    }

    /**
     * generate the right path based on configuration
     * 
     * @return
     */
    public String getPath() {
        return isEnableWindows() ? getWindowsPath() : getLinuxPath();
    }

    public String getAddResourceLocationsPath() {
        if (isEnableWindows()) {
            return "file:///" + getWindowsPath();
        } else {
            return "file:" + getLinuxPath();
        }
    }
}