package com.nikee.universal.config;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;

import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
class MongoConfig extends AbstractMongoClientConfiguration {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Value("${spring.data.mongodb.database}")
	private String dbName;

	// @Value("${spring.data.mongodb.host}")
	// private String dbHost;

	// @Value("${spring.data.mongodb.port}")
	// private int dbPort;

	@Value("${spring.data.mongodb.uri}")
	private String uri;

	@Override
	protected Collection<String> getMappingBasePackages() {
		java.util.List<String> packages = new ArrayList<>(1);
		packages.add("com.nikee.universal.entity");
		return packages;
	}

	@Bean
	public CorsFilter corsFilter() {

		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		// config.addAllowedOrigin("http://localhost:8000");
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("HEAD");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("DELETE");
		config.addAllowedMethod("PATCH");
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}

	@Bean
	MongoTransactionManager transactionManager(MongoDbFactory dbFactory) {
		return new MongoTransactionManager(dbFactory);
	}

	@Override
	public MongoClient mongoClient() {
		// String uri = "mongodb://" + dbHost + ":" + dbPort + "/" + dbName + "";
		String uri = this.uri;
		logger.warn("Mongo Connection String: \n" + uri);
		ConnectionString str = new ConnectionString(uri);
		MongoClientSettings settings = MongoClientSettings.builder().applyConnectionString(str).build();
		return MongoClients.create(settings);
	}

	@Override
	protected String getDatabaseName() {
		return dbName;
	}

	public class ZonedDateTimeReadConverter implements Converter<Date, ZonedDateTime> {
		@Override
		public ZonedDateTime convert(Date date) {
			return date.toInstant().atZone(ZoneOffset.UTC);
		}
	}

	public class ZonedDateTimeWriteConverter implements Converter<ZonedDateTime, Date> {
		@Override
		public Date convert(ZonedDateTime zonedDateTime) {
			return Date.from(zonedDateTime.toInstant());
		}
	}

	@Bean
	public MongoCustomConversions customConversions() {
		List<Converter<?, ?>> converters = new ArrayList<>();
		converters.add(new ZonedDateTimeReadConverter());
		converters.add(new ZonedDateTimeWriteConverter());
		return new MongoCustomConversions(converters);
	}

}
