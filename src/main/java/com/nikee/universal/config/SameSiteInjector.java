package com.nikee.universal.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.session.web.http.DefaultCookieSerializer;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class SameSiteInjector {
  private ApplicationContext applicationContext;

//  public SameSiteInjector(ApplicationContext applicationContext) {
//    this.applicationContext = applicationContext;
//  }

  @EventListener
  public void onApplicationEvent(ContextRefreshedEvent event) {
    DefaultCookieSerializer cookieSerializer = applicationContext.getBean(DefaultCookieSerializer.class);
    // Used to be "None", but SameSite cookies have been updated resently and will need "Secure" when using "None"
    // Author: Wenzel
    cookieSerializer.setSameSite("Lax");
    // cookieSerializer.setUseSecureCookie(true);
  }


}