package com.nikee.universal;

import com.nikee.universal.config.UploadSetting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableScheduling
@EnableConfigurationProperties({ UploadSetting.class })
// extending SpringBootServletInitializer and overriding
// SpringApplicationBuilder allow spring to set application as servlet so can be
// deployed as a war file on Tomcat.
// Tomcat 7.0 does not support this. Recommend using Tomcat 8.5
public class UniversalApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(UniversalApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(UniversalApplication.class, args);
	}

}
