package com.nikee.universal.dao;

import com.nikee.universal.entity.Company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

@Repository
public class CompanyDAO {

    @Autowired
    private MongoOperations template;

    public Company create(Company company) {
        Company c = company;
        c.setId(null);
        return this.template.insert(c);
    }

    public Company find(String id) {
        return this.template.findById(id, Company.class);
    }

    public Company save(Company company) {
        return this.template.save(company);
    }

}