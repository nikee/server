package com.nikee.universal.dao;

import com.nikee.universal.entity.PasswordResetToken;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasswordResetTokenRepository extends MongoRepository<PasswordResetToken, ObjectId> {

    PasswordResetToken findByToken(String token);

}