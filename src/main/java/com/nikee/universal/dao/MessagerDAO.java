package com.nikee.universal.dao;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import com.nikee.universal.dao.interfaces.IAccount;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Mail;
import com.nikee.universal.entity.account.Message.Envelope;
import com.nikee.universal.entity.account.Message.MailList;
import com.nikee.universal.entity.account.Message.Msg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Repository
public class MessagerDAO {

    Logger logger = LoggerFactory.getLogger(MessagerDAO.class);

    @Autowired
    private MongoOperations template;
    @Autowired
    private MailListRepository repo;
    @Autowired
    private IAccount accountDao;

    /**
     * Post the envelope to destination mailboxs
     * 
     * @param e
     */
    public void addToInbox(Envelope e) {
        if (e.getFlag() == 1) {
            MailList m = getMailList(e.getTo());
            m.add(e.getId());
            this.template.save(m);
            logger.info("Deliver Envelope " + e.getId() + " to Mailbox of user " + e.getTo());
        } else if (e.getFlag() == 2) {
            List<MailList> list = getMailListByCompany(e.getDomain());
            list.forEach(ml -> {
                ml.add(e.getId()); // log the envelope ID into inbox for each MailList instance
            });
            repo.saveAll(list);
            logger.info("Deliver Envelope " + e.getId() + " to Mailbox of all " + list.size() + " users in company "
                    + e.getDomain());
        } else if (e.getFlag() == 3) {
            List<MailList> list = getAllMailLists();
            list.forEach(ml -> {
                ml.add(e.getId()); // log the envelope ID into inbox for each MailList instance
            });
            repo.saveAll(list);
            logger.info("Broadcast Envelope " + e.getId() + " to all " + list.size() + " users. ");
        }
    }

    private List<MailList> getAllMailLists() {
        List<Account> accounts = accountDao.getAll();
        List<String> ids = accounts.stream().map(a -> a.getId()).collect(Collectors.toList());
        List<MailList> list = this.template.findAll(MailList.class);
        ids.forEach(id -> {
            MailList _m = new MailList(id);
            if (!list.contains(_m)) {
                list.add(_m);
            }
        });
        // list should contain an instance of mailList for each ID by now
        return list;
    }

    /**
     * get a list containing maillist instances for each account in one company; if
     * one does not have one in database, create one
     * 
     * @param companyID
     * @return
     */
    private List<MailList> getMailListByCompany(String companyID) {
        List<Account> accounts = accountDao.findByCompanyID(companyID);
        List<String> ids = accounts.stream().map(a -> a.getId()).collect(Collectors.toList());
        Query q = query(where("_id").in(ids));
        List<MailList> list = this.template.find(q, MailList.class);
        ids.forEach(id -> {
            MailList _m = new MailList(id);
            if (!list.contains(_m)) {
                list.add(_m);
            }
        });
        // list should contain an instance of mailList for each ID by now
        return list;
    }

    private MailList getMailList(String id) {
        MailList m = this.template.findById(id, MailList.class);
        if (m == null)
            m = new MailList(id);
        return m;
    }

    /**
     * Send
     * 
     * @param collection
     * @param id
     * @return
     */
    public Envelope send(Envelope envelope) {
        Envelope e = this.template.save(envelope);
        return e;
    }

    public Envelope findByID(String id) {
        return this.template.findById(id, Envelope.class);
    }

    public Msg findMsgByID(String id) {
        return this.template.findById(id, Msg.class);
    }

    public Msg save(Msg msg) {
        msg.setId(null);
        return this.template.save(msg);
    }

    public MailList save(MailList ml) {
        return repo.save(ml);
    }

    public MailList findById(String id) {
        Optional<MailList> opt = repo.findById(id);
        return opt.isPresent() ? opt.get() : null;
    }

    public List<Envelope> getEnvelopes(List<String> ids) {
        Query q = query(where("_id").in(ids));
        return this.template.find(q, Envelope.class);
    }

    public List<Msg> getContents(List<String> ids) {
        Query q = query(where("_id").in(ids));
        return this.template.find(q, Msg.class);
    }

}