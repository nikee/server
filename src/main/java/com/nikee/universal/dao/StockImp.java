package com.nikee.universal.dao;

import java.util.List;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.nikee.universal.dao.interfaces.IStock;
import com.nikee.universal.entity.Stock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class StockImp implements IStock {

    @Autowired
    private MongoOperations template;

    @Override
    public void createStock(Stock stock) {
        this.template.save(stock);
    }

    @Override
    public Stock findStock(Stock stock) {
        try {
            Criteria criteria = Criteria.where("_id").is(stock.getId());
            Query query = new Query(criteria);        
            return this.template.find(query, Stock.class).get(0);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    @Override
    public boolean updateStock(Stock stock) {
        Criteria criteria = Criteria.where("_id").is(stock.getId());
        Query query = new Query(criteria);
        Update update = new Update();
        update.set("manufactureDate", stock.getManufactureDate());
        update.set("expireDate", stock.getExpireDate());
        update.set("itemName", stock.getItemName());
        update.set("itemType", stock.getItemType());
        update.set("expired", stock.getExpired());
        update.set("description", stock.getDescription());
        update.set("price", stock.getPrice());
        update.set("quantity", stock.getQuantity());
        update.set("weight", stock.getWeight());
        update.set("contactInfo", stock.getContactInfo());
        UpdateResult result = this.template.updateFirst(query, update, Stock.class);
        return result.wasAcknowledged();
    }

    @Override
    public List<Stock> findAllStock() {
        return this.template.findAll(Stock.class);
    }

    @Override
    public boolean expireStock(Stock stock) {
        Criteria criteria = Criteria.where("_id").is(stock.getId());
        Query query = new Query(criteria);
        Update update = new Update();
        update.set("expired", true);
        UpdateResult result = this.template.updateFirst(query, update, Stock.class);
        return result.wasAcknowledged();
    }
    
    @Override
    public boolean cleanStock(Stock stock) {
        Criteria criteria = Criteria.where("_id").is(stock.getId());
        Query query = new Query(criteria);
        DeleteResult result = template.remove(query, Stock.class);
        return result.wasAcknowledged();
    }
}
