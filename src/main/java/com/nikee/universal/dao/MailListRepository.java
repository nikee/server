package com.nikee.universal.dao;

import com.nikee.universal.entity.account.Message.MailList;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MailListRepository extends MongoRepository<MailList, String> {

}