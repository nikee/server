package com.nikee.universal.dao;

import java.util.List;

import com.mongodb.client.result.UpdateResult;
import com.nikee.universal.dao.interfaces.ILeave;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.leave.Leave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class LeaveImpl implements ILeave {

    @Autowired
    private MongoOperations template;

    @Override
    public Leave addLeave(Account acc, Leave leave) {
        Leave obj = this.template.save(leave); 
        return obj;
    }

    @Override
    // find leave by employee and leave Id
    public Leave findLeave(Leave leave) {
        Query query = new Query();
        query.addCriteria(new Criteria().andOperator(Criteria.where("employee._id").is(leave.getEmployee().getId()),
                Criteria.where("_id").is(leave.getId())));
        return this.template.find(query, Leave.class).get(0);
    }

    @Override
    public boolean updateLeave(Leave leave) {
        Criteria criteria = Criteria.where("_id").is(leave.getId());
        Query query = new Query(criteria);
        Update update = new Update();
        update.set("startDate", leave.getStartDate());
        update.set("endDate", leave.getEndDate());
        update.set("reason", leave.getReason());
        update.set("type", leave.getType());
        UpdateResult result = this.template.updateFirst(query, update, Leave.class);
        return result.wasAcknowledged();
    }

    @Override
    public List<Leave> findAllLeaves(String empId) {
        
        Criteria cr = Criteria.where("employee._id").is(empId);
        Query query = new Query(cr);        
        return this.template.find(query, Leave.class);
    }

    @Override
    public boolean grantLeave(Leave leave) {
        Criteria criteria = Criteria.where("_id").is(leave.getId());
        Query query = new Query(criteria);
        Update update = new Update();
        update.set("granted", leave.getGranted());
        UpdateResult result = this.template.updateFirst(query, update, Leave.class);
        return result.wasAcknowledged();
    }
    
}