package com.nikee.universal.dao;

import java.util.List;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.nikee.universal.dao.interfaces.IAttendance;
import com.nikee.universal.entity.Attendance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class AttendanceImpl implements IAttendance {

    @Autowired
    private MongoOperations template;
    
    @Override
    public void create(Attendance attendance) {
        this.template.save(attendance); 
    }

    @Override
    public boolean delete(Attendance attendance) {
        Criteria criteria = Criteria.where("_id").is(attendance.getAccountId());
        Query query = new Query(criteria);
        DeleteResult result = template.remove(query, Attendance.class);
        return result.wasAcknowledged();
    }

    @Override
    public boolean update(Attendance attendance) {
        Criteria criteria = Criteria.where("_id").is(attendance.getAccountId());
        Query query = new Query(criteria);
        Update update = new Update();
        update.set("logTimeList", attendance.getLogTimeList());
        UpdateResult result = this.template.updateFirst(query, update, Attendance.class);
        return result.wasAcknowledged();
    }

    @Override
    public Attendance find(Attendance attendance) {
        return this.template.findById(attendance.getAccountId(), Attendance.class);
    }

    @Override
    public List<Attendance> findAll() {
        return this.template.findAll(Attendance.class);
    }
    
}
