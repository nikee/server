package com.nikee.universal.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.lang.Nullable;
import com.nikee.universal.dao.interfaces.IService;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Services;
import com.nikee.universal.errors.TransactionFailureException;
import com.nikee.universal.errors.genericError;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ServiceImpl implements IService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * NOTE: DONT FORGET TO IMPLEMENT CHANGE OWNER OF SERVICE METHOD IN ACCOUNT DAO!
     */

    @Autowired
    private MongoOperations template;

    @Override
    @Transactional
    public Services Amend(Services toCreate) {
        return this.template.save(toCreate);
    }

    @Override
    @Transactional
    public Services Amend(Services toCreate, String parentService) {
        Services result = this.template.save(toCreate); // create service and grab its id
        Services parent = this.template.findById(parentService, Services.class);
        if (parent == null)
            return null;
        Set<Services> set = parent.getSubService() == null ? new HashSet<Services>() : parent.getSubService();
        set.add(result);
        Query query = query(where("_id").is(parent.getId()));
        Update update = new Update();
        update.set("subService", set);
        this.template.updateFirst(query, update, Services.class);
        return result;
    }

    @Override
    public List<Services> findAllByOwner(Account owner) {
        Query query = query(where("companyId").is(owner.getCompany()));
        return this.template.find(query, Services.class);
    }

    @Override
    public List<Services> findByName(Account owner, String name) {
        Query query = query(where("companyId").is(owner.getCompany())).addCriteria(where("name").is(name));
        return this.template.find(query, Services.class);
    }

    @Override
    @Transactional(rollbackFor = TransactionFailureException.class)
    public void cut(Services toBeCut) throws TransactionFailureException {
        // 1. remove toBeCut from parent's subService
        // 2. paste children into parent's subService
        // 3. save daddy
        // final. delete toBeCut
        Services me = this.template.findById(toBeCut.getId(), Services.class);
        if (me == null)
            return;
        Services daddy = this.whosYourDaddy(me.getId());
        if (daddy != null) {
            // 1.
            Set<Services> daddySet = daddy.getSubService();
            daddySet.remove(me);
            // 2.
            daddySet.addAll(me.getSubService());

            daddy.setSubService(daddySet);
            // 3.
            Services saved = this.template.save(daddy);
            if (!saved.getSubService().containsAll(me.getSubService()))
                throw new TransactionFailureException("Parent node failed to obtain new children nodes",
                        genericError.DATABASE_CHANGE_NOT_CONFIRMED);
        }
        // final.
        Query query = query(where("_id").is(me.getId()));
        DeleteResult result = this.template.remove(query, Services.class);
        if (!result.wasAcknowledged())
            throw new TransactionFailureException("The deletion of node is not acknoledged.",
                    genericError.DATABASE_CHANGE_NOT_CONFIRMED);
    }

    @Override
    @Transactional
    public void delete(Services toBeDeleted) throws TransactionFailureException {
        Services me = toBeDeleted;
        // this.template.findById(toBeDeleted.getId(), Services.class);
        // cascade delete
        Set<Services> childSet = me.getSubService() == null ? new HashSet<>() : me.getSubService(); // get childset from
                                                                                                    // the node to be
                                                                                                    // removed
        Set<String> ids = new HashSet<>();
        Services daddy = this.whosYourDaddy(me.getId());
        this.deleteMany(childSet, ids);
        ids.add(me.getId());// delete the named node as well
        Query query = query(where("_id").in(ids));
        DeleteResult result = this.template.remove(query, Services.class);
        if (!result.wasAcknowledged())
            throw new TransactionFailureException("The deletion of node is not acknoledged.",
                    genericError.DATABASE_CHANGE_NOT_CONFIRMED);
        if (daddy != null) // take care of parent subservice
        {
            childSet = daddy.getSubService();
            childSet.remove(me);
            daddy.setSubService(childSet);
            this.template.save(daddy);

        }
    }

    private void deleteMany(Set<Services> set, Set<String> finalLog) {
        if (set.size() == 0)
            return;
        Set<Services> fullInstances = this.fetchMany(set); // load full data of input param in case they were lazily //
                                                           // loaded
        Set<Services> nextToRemove = new HashSet<>();
        fullInstances.forEach(s -> {
            if (s.getSubService() != null)
                nextToRemove.addAll(s.getSubService());
        }); // collect all child nodes info for next //
            // round
        Set<String> ids = fullInstances.stream().map(s -> s.getId()).collect(Collectors.toSet()); // id to delete
        finalLog.addAll(ids);
        this.deleteMany(nextToRemove, finalLog);
    }

    private Set<Services> fetchMany(Set<Services> idSet) {
        Set<String> set = idSet.stream().map(s -> s.getId()).collect(Collectors.toSet());
        Query query = query(where("_id").in(set));
        List<Services> result = this.template.find(query, Services.class);
        Set<Services> out = new HashSet<>(result);
        return out;
    }

    @Override
    @Transactional
    public void insert(Services original, Services toBeInserted) {
        Services daddy = this.whosYourDaddy(original.getId());
        Services _origin = this.template.findById(original.getId(), Services.class);
        Services inserted = toBeInserted;
        inserted.clearSubService();
        inserted.addSubService(original);
        inserted.setId(null);
        inserted.setCompanyId(_origin.getCompanyId());
        inserted = this.template.save(inserted); // save the new node first to create id
        if (daddy != null)// if daddy is not null, update the subService field and save
        {
            Set<Services> dadSet = daddy.getSubService();
            logger.info("parent node set size was " + dadSet.size());
            if (dadSet.add(inserted)) // if dad does not already have the element, then save it
            {
                // save
                dadSet.remove(original);
                logger.info("after removal, the set size is " + dadSet.size());
                daddy.setSubService(dadSet);
                this.template.save(daddy);
            }

        } else {
            logger.info("daddy is null");
        }

    }

    @Override
    public Services update(Services service) {
        Services _service = this.template.findById(service.getId(), Services.class);
        Services input = service;
        input.setCompanyId(_service.getCompanyId());
        input.setSubService(_service.getSubService());
        return this.template.save(input);
    }

    @Override
    public void setRoot(Services service) {
        Services daddy = this.whosYourDaddy(service.getId());
        if (daddy == null)
            return;
        if (daddy.removeSubService(service))
            this.template.save(daddy);
    }

    @Override
    @Nullable
    public Services whosYourDaddy(String id) {
        List<Services> holder = new ArrayList<>();
        Services service = this.template.findById(id, Services.class);
        holder.add(service);
        Query query = query(
                where("subService").elemMatch(where("$id").is(new ObjectId(id)).and("$ref").is("services")));
        Query q2 = query(where("subService").elemMatch(where("_id").is(new ObjectId(id))));
        // Document doc = new Document();
        // check these
        // https://www.cnblogs.com/exmyth/p/10772087.html
        // https://stackoverflow.com/questions/46612203/how-to-run-mongodb-native-query-with-mongodb-date-function-in-spring-data-mongod
        // this.template.executeCommand(doc);

        List<Services> list = this.template.find(query, Services.class);
        List<Services> l2 = this.template.find(q2, Services.class);
        if (l2.size() + list.size() > 1) {
            logger.warn("Length of result is abnormal when finding service node parent for ID " + id + " ." + l2.size()
                    + list.size() + " found, 0 or 1 expected. Size of Q1: " + list.size() + ", size of Q2: "
                    + l2.size());
        }
        list.addAll(l2);
        if (list.size() > 1) {
            logger.warn("The service with ID " + service.getId() + " has more than one parent!");
            throw new RuntimeException("Specified service entry has more than one parent.");
        }
        if (list.size() == 0)
            return null;
        return list.get(0);
    }

    @Override
    @Nullable
    public Services getInstance(String id) {
        ObjectId oid = new ObjectId(id);
        return this.template.findById(oid, Services.class);
    }

}