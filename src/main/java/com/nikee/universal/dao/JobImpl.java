package com.nikee.universal.dao;

import java.util.List;

import com.mongodb.client.result.UpdateResult;
import com.nikee.universal.dao.interfaces.IJob;
import com.nikee.universal.entity.Job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class JobImpl implements IJob {
    @Autowired
    private MongoOperations template;

    @Override
    public void postJob(Job job) {
        this.template.save(job); 
    }

    @Override
    public Job findJob(Job job) {
        try {
            Criteria criteria = Criteria.where("_id").is(job.getId());
            Query query = new Query(criteria);        
            return this.template.find(query, Job.class).get(0);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    @Override
    public boolean updateJob(Job job) {
        Criteria criteria = Criteria.where("_id").is(job.getId());
        Query query = new Query(criteria);
        Update update = new Update();
        update.set("postDate", job.getPostDate());
        update.set("endDate", job.getEndDate());
        update.set("discarded", job.getDiscarded());
        update.set("jobTitle", job.getJobTitle());
        update.set("jobType", job.getJobType());
        update.set("description", job.getDescription());
        update.set("salary", job.getSalary());
        update.set("company", job.getCompany());
        update.set("contactInfo", job.getContactInfo());
        UpdateResult result = this.template.updateFirst(query, update, Job.class);
        return result.wasAcknowledged();
    }

    @Override
    public List<Job> findAllJob() {
        return this.template.findAll(Job.class);
    }

    @Override
    public boolean discardJob(Job job) {
        Criteria criteria = Criteria.where("_id").is(job.getId());
        Query query = new Query(criteria);
        Update update = new Update();
        update.set("discarded", true);
        UpdateResult result = this.template.updateFirst(query, update, Job.class);
        return result.wasAcknowledged();
    }

}
