package com.nikee.universal.dao;

import java.util.List;

import com.nikee.universal.dao.interfaces.IExpense;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.account.Expenses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class ExpenseImpl implements IExpense {

    @Autowired
    private MongoOperations operations;

    @Override
    public Expenses addExpense(Expenses expense) {
        return operations.insert(expense);
    }    

    /* Find all expenses for the company*/
    @Override
    public List<Expenses> findAllExpenses(Account acc) {
        Query q = new Query();
        q.addCriteria(Criteria.where("company").is(acc.getCompany()));
        return this.operations.find(q, Expenses.class);
    }
    
}