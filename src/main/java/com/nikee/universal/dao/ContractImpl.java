package com.nikee.universal.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.mongodb.client.result.DeleteResult;
import com.nikee.universal.dao.interfaces.IAccount;
import com.nikee.universal.dao.interfaces.IContract;
import com.nikee.universal.dao.interfaces.IService;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Services;
import com.nikee.universal.entity.contract.Deal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

import ch.qos.logback.core.net.server.Client;

@Repository
public class ContractImpl implements IContract {
    Logger logger = LoggerFactory.getLogger(ContractImpl.class);

    @Autowired
    private MongoOperations template;

    // @Autowired
    // private MongoTemplate mongoTemplate;

    @Autowired
    private IService serviceDao;
    @Autowired
    private IAccount accountDao;

    @Override
    public Deal create(Deal deal) {
        deal.setId(null);
        return this.template.insert(deal);
    }

    @Override
    public Deal findById(String id) {
        return this.template.findById(id, Deal.class);
    }

    @Override
    public List<Deal> findByClient(String clientId) {
        Query q = query(where("client").is(clientId));
        return this.template.find(q, Deal.class);
    }

    @Override
    public List<Deal> findByService(String serviceId) {
        Query q = query(where("service").is(serviceId));
        return this.template.find(q, Deal.class);
    }

    @Override
    public List<Deal> findByBusiness(String ownerId) {
        Account owner = this.accountDao.findById(ownerId);
        if (owner == null)
            return new ArrayList<>();
        List<Services> list = this.serviceDao.findAllByOwner(owner);
        List<String> _objIds = list.stream().map(s -> s.getId()).collect(Collectors.toList());
        Query q = query(where("service").in(_objIds));
        return this.template.find(q, Deal.class);
    }

    @Override
    public List<Deal> findByBusiness(Account owner) {
        List<Services> list = this.serviceDao.findAllByOwner(owner);
        List<String> _objIds = list.stream().map(s -> s.getId()).collect(Collectors.toList());
        Query q = query(where("service").in(_objIds));
        return this.template.find(q, Deal.class);
    }

    @Override
    public Deal save(Deal deal) {
        Deal copy = deal;
        Deal _deal = this.findById(deal.getId());
        // if cannot find existing document in DB with given ObjectId, then set ID as
        // null so a new ID will be auto assigned when creating
        if (_deal == null) {
            copy.setId(null);
        }
        return this.template.save(copy);
    }

    @Override
    public DeleteResult remove(Deal deal) {
        return this.template.remove(deal, "deal");
    }

    //Not completed 
    //Need to get client names from ids in the list -> ListDeal
    @Override
    public List<Deal> getDealInfo(List<Deal> listDeal) {

        //AggregationOperation match1;
        List<String> clientIDs = listDeal.stream()
                                    .map(a->a.getClient())
                                    .collect(Collectors.toList());

        Aggregation agg = newAggregation(
            match(Criteria.where("client").is("5e97e96c63a66e2609d5ae95")),
            lookup("client", "client", "_id", "testField123")
        );

        AggregationResults<String> results = template.aggregate(agg, "Deal", String.class);
        List<String> mappedResult = results.getMappedResults();
        return null;
    }

}
