package com.nikee.universal.dao;

import com.nikee.universal.dao.interfaces.ILicense;
import com.nikee.universal.entity.license.License;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Repository
public class LicenseImpl implements ILicense {
    Logger logger = LoggerFactory.getLogger(LicenseImpl.class);

    @Autowired
    private MongoOperations template;

    @Override
    public License findByID(String id) {
        return this.template.findById(id, License.class);
    }

    @Override
    public License save(License license) {
        return this.template.save(license);
    }

    @Override
    public License findByCompany(String companyID) {
        Query query = query(where("grantTo").is(companyID));
        return this.template.findOne(query, License.class);
    }

}