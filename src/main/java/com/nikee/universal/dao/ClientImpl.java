package com.nikee.universal.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.time.ZonedDateTime;
import java.util.List;

import com.mongodb.client.result.UpdateResult;
import com.nikee.universal.dao.interfaces.IClient;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.client.Action;
import com.nikee.universal.entity.client.Client;
import com.nikee.universal.entity.client.Identifier;
import com.nikee.universal.entity.client.Note;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class ClientImpl implements IClient {

    @Autowired
    private MongoOperations template;

    @Override
    public Client create(Client client) {
        Client c = client;
        c.setId(null);
        List<Note> notes = c.getNotes();
        notes.forEach(n -> {
            n.setId(getNewId());
        });
        List<Action> actions = c.getActions();
        actions.forEach(n -> {
            n.setId(getNewId());
        });
        c.setActions(actions);
        c.setNotes(notes);
        Account boss = this.template.findById(client.getInCharge(), Account.class);
        boss = boss.getEmployer() == null ? boss : boss.getEmployer();
        c.setCompanyId(boss.getCompany());
        return this.template.insert(c);
    }

    @Override
    public Client read(String id) {
        return this.template.findById(id, Client.class);
    }

    @Override
    public List<Client> findByCompanyID(String id) {
        Query query = query(where("companyId").is(id));
        return this.template.find(query, Client.class);
    }

    @Override
    public Client update(Client client) {
        return this.template.save(client);
    }

    @Override
    public boolean updateBasic(Client client) {
        Query q = query(where("_id").is(client.getId()));
        Update update = new Update();
        update.set("firstName", client.getFirstName()).set("lastName", client.getLastName())
                .set("email", client.getEmail()).set("phone", client.getPhone());
        UpdateResult result = this.template.updateFirst(q, update, Client.class);
        return result.wasAcknowledged();
    }

    /**
     * generate an unique if for client class interier usage
     * 
     * @return
     */
    public String getNewId() {
        return this.template.insert(new Identifier()).getId();
    }

    /**
     * 
     * @param id
     * @return always reture true
     */
    private boolean removeId(String id) {
        this.template.remove(new Identifier(id));
        return true;
    }

    /**
     * partial update, only change one element
     * 
     * @return boolean
     */
    @Override
    public boolean updateNote(Client client, Note note) {
        Query q2 = query(where("_id").is(client.getId()));
        q2.addCriteria(where("notes._id").is(note.getId()));
        Update update = new Update();
        update.set("notes.$", note);
        UpdateResult result = this.template.updateFirst(q2, update, Client.class);
        return result.wasAcknowledged();
    }

    @Override
    public boolean addNote(Client client, Note newNote) {
        Query q = query(where("_id").is(client.getId()));
        Update update = new Update();
        Note note = newNote;
        note.setId(getNewId());
        update.push("notes", note);
        UpdateResult result = this.template.updateFirst(q, update, Client.class);
        return result.wasAcknowledged();
    }

    @Override
    public boolean removeNote(Client client, String id) {
        Query q = query(where("_id").is(client.getId()));
        Update update = new Update();
        update.pull("notes", query(where("_id").is(id)));
        UpdateResult result = this.template.updateFirst(q, update, Client.class);
        return result.wasAcknowledged() ? removeId(id) : false;
    }

    @Override
    public boolean tickAction(Client client, String id, Account who) {
        Query q = query(where("_id").is(client.getId()));
        Update update = new Update();
        update.set("actions.$[ele].checkList." + who.getId(), true).filterArray("ele._id", id);
        UpdateResult result = this.template.updateFirst(q, update, Client.class);
        return result.wasAcknowledged();
    }

    @Override
    public boolean unTickAction(Client client, String id, Account who) {
        Query q = query(where("_id").is(client.getId()));
        Update update = new Update();
        update.set("actions.$[ele].checkList." + who.getId(), false).filterArray("ele._id", id);
        UpdateResult result = this.template.updateFirst(q, update, Client.class);
        return result.wasAcknowledged();
    }

    @Override
    public boolean assignStaffToAction(Client client, String actionId, Account who) {
        return this.unTickAction(client, actionId, who);
    }

    @Override
    public boolean addAction(Client client, List<Account> enroll, ZonedDateTime deadline, Account assignedBy,
            String desc) {
        Action action = new Action();
        action.setAssignedBy(assignedBy.getId());
        action.setId(getNewId());
        action.setDeadline(deadline);
        action.setDescription(desc);
        enroll.forEach(a -> action.enlist(a.getId()));
        Query q = query(where("_id").is(client.getId()));
        Update update = new Update();
        update.push("actions", action);
        UpdateResult result = this.template.updateFirst(q, update, Client.class);
        return result.wasAcknowledged();
    }

    /**
     * change only the desc, deadline
     * 
     * @param client
     * @param action action containing the ID of action to be modifeid
     * @return
     */
    public boolean updateAction(Client client, Action action) {
        Query q = query(where("_id").is(client.getId()));
        Update update = new Update();
        update.set("actions.$[ele].description", action.getDescription())
                .set("actions.$[ele].deadline", action.getDeadline()).filterArray("ele._id", action.getId());
        UpdateResult result = this.template.updateFirst(q, update, Client.class);
        return result.wasAcknowledged();
    }

    @Override
    public List<Client> getAllClients() {     
        //Query query = query(where("companyId").is(company));
        //return this.template.find(query, Client.class);
        return this.template.findAll(Client.class);
    }

    

    @Override
    public List<Client> getClientsByCompany(String companyID) {
        String company = companyID;
        Query query = query(where("companyId").is(company));
        return this.template.find(query, Client.class);
    }


    

}