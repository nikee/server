package com.nikee.universal.dao;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.List;

import com.mongodb.client.result.UpdateResult;
import com.nikee.universal.dao.interfaces.IAccount;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Services;
import com.nikee.universal.entity.account.CalendarEvent;
import com.nikee.universal.entity.account.Message.MailList;
import com.nikee.universal.entity.client.Client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class AccountImpl implements IAccount {

    /**
     *
     */

    Logger logger = LoggerFactory.getLogger(AccountImpl.class);

    @Autowired
    private MongoOperations template;
    @Autowired
    private MailListRepository repo;

    @Override
    public List<Account> findAccountByEmail(String email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(email));
        query.addCriteria(Criteria.where("discarded").is(false));
        List<Account> list = this.template.find(query, Account.class);
        return list;
    }

    @Override
    public Account findFirstAccountByEmail(String email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(email));
        query.addCriteria(Criteria.where("discarded").is(false));
        List<Account> list = this.template.find(query, Account.class);
        return (list.size() > 0 ? list.get(0) : null);
    }

    @Override
    public UpdateResult changePassword(String email, String newPasswordHashed) {
        Update update = new Update();
        update.set("password", newPasswordHashed);
        Query query = query(where("email").is(email));
        return this.template.updateFirst(query, update, Account.class);
    }

    @Override
    public void signout(Account account) {

    }

    @Override
    public Account updateAccount(Account account) {
        // UpdateResult result = this.template.updateFirst(
        // query(where("email").is(account.getEmail())),update("",""),Account.class);
        return this.template.save(account);
    }

    @Override
    public String createAccountTest(Account account) {
        Account acc = this.template.save(account);
        repo.save(new MailList(acc.getId()));
        return "Succeeded";
    }

    @Override
    /**
     * Will hash the password
     */
    public Account createAccount(Account accountToCreate) throws DuplicateKeyException {
        Account acc = this.template
                .insert(accountToCreate.setPassword(Account.hashPassword(accountToCreate.getPassword())));
        repo.save(new MailList(acc.getId()));
        return acc;
    }

    private void handlePreviousUser(String email, Account to) {
        // this.discardUser(email);
        this.downGradeUser(email, to);

    }

    private void discardUser(String email) {
        Query query = query(where("email").is(email));
        Update update = new Update();
        update.set("discarded", true);
        this.template.updateFirst(query, update, Account.class);
    }

    private void downGradeUser(String email, Account to) {
        Query query = query(where("email").is(email));
        Update update = new Update();
        update.set("employer", to);
        update.set("level", 3);
        this.template.updateFirst(query, update, Account.class);
    }

    @Override
    @Transactional
    /**
     * @return UpdateResult containing how many documents effected redirecting
     *         employer to the new account
     */
    public UpdateResult transferOwnership(Account from, Account to) {
        Update update = new Update();
        update.set("email", to.getEmail()).set("employer", null).set("level", 2).set("company", from.getCompany())
                .set("discarded", false).set("password", to.getPassword()); // set email, employer, level, company and
                                                                            // password
        Query query = query(where("email").is(to.getEmail()));
        this.template.upsert(query, update, Account.class); // creating target t2 account
        Account _to = this.findFirstAccountByEmail(to.getEmail()); // get target t2 account instance
        query = query(where("employer").is(from));
        update = new Update();
        update.set("employer", _to);
        handlePreviousUser(from.getEmail(), _to);
        // this.changeServiceOwner(from, _to);
        // this.changeClientOwner(from, _to);
        return this.template.updateMulti(query, update, Account.class);
    }

    @Deprecated
    /**
     * No longer needed.
     */
    private void changeClientOwner(Account from, Account to) {
        Query q = query(where("BusinessOwnerId").is(from.getId()));
        Update u = new Update();
        u.set("BusinessOwnerId", to.getId());
        UpdateResult r = this.template.updateMulti(q, u, Client.class);
        logger.warn("changeClientOwner: " + r.getModifiedCount() + " documents modified.");
    }

    @Deprecated
    /**
     * No longer needed. A company now holds the same ID.
     * 
     * @param from
     * @param to
     */
    private void changeServiceOwner(Account from, Account to) {
        Criteria criteria = new Criteria();
        // criteria.orOperator(Criteria.where("ownedBy").is(from.getId()),
        // Criteria.where("ownedBy.$id").is(from.getId()).and("$ref").is("services"));
        criteria = Criteria.where("companyId").is(from.getCompany());
        Query q = new Query(criteria);
        Update update = new Update();
        update.set("companyId", to);
        UpdateResult r = this.template.updateMulti(q, update, Services.class);
        logger.warn("changeServiceOwner: " + r.getModifiedCount() + " documents modified.");

    }

    /**
     * find records that is lv2,3 or 4, employed by the input account, and is not
     * discarded
     */
    @Override
    public List<Account> findByEmployer(Account boss) {
        Query query = query(where("employer").is(boss)).addCriteria(where("discarded").is(false))
                .addCriteria(where("level").in(2, 3, 4));
        return this.template.find(query, Account.class);
    }

    @Override
    public Account findById(String id) {
        return this.template.findById(id, Account.class);
    }

    @Override
    public boolean addCalenderEvent(CalendarEvent event, Account acc) {
        Query query = query(where("_id").is(acc.getId()));
        Update update = new Update();
        update.push("calendarEvents", event);
        UpdateResult result = this.template.updateFirst(query, update, Account.class);
        return result.wasAcknowledged();  
    }

    @Override
    public boolean updateCalendar(CalendarEvent updatedCalendarEvent) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is("5e97e89263a66e2609d5ae8e")
                                    .and("calendarEvents")
                                    .elemMatch( Criteria.where("_id").is(updatedCalendarEvent.getId())));
        Update update = new Update();
        //update.push("calendarEvents", updatedCalendarEvent);
        update.set("calendarEvents", updatedCalendarEvent);
        UpdateResult result = this.template.updateFirst(query, update, Account.class);
        return result.wasAcknowledged();

        
    }
    
    public List<Account> findByCompanyID(String companyID) {
        Query q = query(where("company").is(companyID));
        return this.template.find(q, Account.class);
    }

    public List<Account> getAll() {
        return this.template.findAll(Account.class);

    }

}