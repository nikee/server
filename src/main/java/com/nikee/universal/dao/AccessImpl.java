package com.nikee.universal.dao;

import java.util.List;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.nikee.universal.dao.interfaces.IAccess;
import com.nikee.universal.entity.Access;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class AccessImpl implements IAccess {


    @Autowired
    private MongoOperations template;
    
    @Override
    public void create(Access access) {
        this.template.save(access); 
    }

    @Override
    public Access find(Access access) {
        // try {       
        //     return this.template.findById(access.getAccountId(), Access.class);
        // } catch (IndexOutOfBoundsException e) {
        //     return null;
        // }
        return this.template.findById(access.getAccountId(), Access.class);
    }

    @Override
    public boolean update(Access access) {
        Criteria criteria = Criteria.where("_id").is(access.getAccountId());
        Query query = new Query(criteria);
        Update update = new Update();
        update.set("access", access.getAccess());
        UpdateResult result = this.template.updateFirst(query, update, Access.class);
        return result.wasAcknowledged();
    }

    @Override
    public List<Access> findAll() {
        return this.template.findAll(Access.class);
    }

    @Override
    public boolean delete(Access access) {
        Criteria criteria = Criteria.where("_id").is(access.getAccountId());
        Query query = new Query(criteria);
        DeleteResult result = template.remove(query, Access.class);
        return result.wasAcknowledged();
    }
    
}
