package com.nikee.universal.dao;

import java.util.List;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.nikee.universal.dao.interfaces.IEmployee;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.employee.Employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;


import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Repository
public class EmployeeImpl implements IEmployee {

    @Autowired
    private MongoOperations template;

    @Override
    public Employee registerNew(Account acc, Employee employee) {
        Employee emp = employee;
        emp.setId(null);
        return this.template.insert(emp);
    }

    @Override
    public List<Employee> findEmployeeByCompanyId(String companyId) {
        Query query = query(where("company").is(companyId));
        return this.template.find(query, Employee.class);        
    }

    @Override
    public boolean update(Employee emp) {
        Query q = query((where("_id").is(emp.getId())));
        Update update = new Update();
        update.set("firstName", emp.getFirstName())
                .set("lastName", emp.getLastName())
                .set("email", emp.getLastName())
                .set("phone", emp.getLastName())
                .set("department", emp.getLastName())
                .set("company", emp.getLastName())
                .set("designation", emp.getLastName())
                .set("isFullTime", emp.getLastName());
        UpdateResult result = this.template.updateFirst(q, update, Employee.class); 
        return result.wasAcknowledged();
    }

    @Override
    public List<Employee> findAllEmployees(Account acc) {
        Query query = query(where("company").is(acc.getCompany()));
        return this.template.find(query, Employee.class); 
    }

    @Override
    public boolean deleteEmployee(Employee employee) {
        DeleteResult result = this.template.remove(employee);
        return result.wasAcknowledged();
    }
    

}