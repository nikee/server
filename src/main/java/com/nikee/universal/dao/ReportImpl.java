package com.nikee.universal.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.*;

import com.nikee.universal.config.UploadSetting;
import com.nikee.universal.dao.interfaces.IClient;
import com.nikee.universal.dao.interfaces.IContract;
import com.nikee.universal.dao.interfaces.IReport;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Services;
import com.nikee.universal.entity.account.Expenses;
import com.nikee.universal.entity.client.Client;
import com.nikee.universal.entity.contract.Deal;
import com.nikee.universal.service.interfaces.IContractService;
import com.nikee.universal.service.interfaces.IExpenseService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.ResourceUtils;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import net.sf.jasperreports.view.JasperViewer;

@Repository
public class ReportImpl implements IReport {
    @Autowired 
    private MongoOperations template;
    @Autowired 
    private IClient clientDao;
    @Autowired
    private IContract contractDao;
    @Autowired 
    private IExpenseService expenseService;

    @Autowired
    private UploadSetting setting;
    
    @Autowired
    private IContractService serviceContract;

    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public String getGstReport(Account acc, String format) {

        String tmpPathOfGeneratedReports;
        
        //1. get company id
        String companyID = acc.getCompany();
        
        //2. get clients of that company
        List<Client> clients = clientDao.getClientsByCompany(companyID);
        List<Deal> dealList = null;
        Services serviceList = null;


        //*****Below code is important******
        //test
        //lookup - mongo template
        
        // LookupOperation lookupOperation = LookupOperation.newLookup()
        // .from("company")
        // .localField("companyId")
        // .foreignField("_id")
        // .as("company");

        // AggregationOperation match = Aggregation.match(Criteria.where("post").size(1));
        // Aggregation agg = Aggregation.newAggregation(lookupOperation, match);
        // //Aggregation aggregation = Aggregation.newAggregation(Aggregation.match(Criteria.where("_id").is("5e97e89163a66e2609d5ae8d")) , lookupOperation);
        // List<Client> results = mongoTemplate.aggregate(agg, "client", Client.class).getMappedResults();
        //************************ */

        List<Deal> listDeal = contractDao.findByBusiness(acc);

        try {
            File file = ResourceUtils.getFile("classpath:gstControlReport.jrxml");            
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(listDeal);
            
            //to hold report parameter
            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("CollectionBeanParam", dataSource); // CollectionBeanParam => jasper studio table data set 


            //new
            //read jrxml and create jasper design object
            InputStream input = new FileInputStream(file);
            JasperDesign jasperDesign = JRXmlLoader.load(input);

            //new
            //compiling jrxml with help of jasper report class
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());

            //new 
            //call jasper engine to view report (Report Viewer)
            //JasperViewer.viewReport(jasperPrint);

            /* Wenzel */
            // Create company folder for the account to place reports
            File companyFloder = new File(setting.getDefReportPath() + "\\" + acc.getCompany());
            if (!companyFloder.exists()  && !companyFloder .isDirectory()) {
                companyFloder.mkdirs();
            }
            tmpPathOfGeneratedReports = companyFloder.getPath();

            if (format.equalsIgnoreCase("html")) {
                JasperExportManager.exportReportToHtmlFile(jasperPrint, tmpPathOfGeneratedReports + "\\GSTControlReport.html");
            }
            if (format.equalsIgnoreCase("pdf")) {
                JasperExportManager.exportReportToPdfFile(jasperPrint, tmpPathOfGeneratedReports + "\\GSTControlReport.pdf");
            }          
            if (format.equalsIgnoreCase("xls"))  {
                File destFile = new File(tmpPathOfGeneratedReports + "\\GSTControlReport.xls");
                JRXlsExporter exporter = new JRXlsExporter();
                exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(destFile));
                SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
                configuration.setOnePagePerSheet(true);
                exporter.setConfiguration(configuration);
                exporter.exportReport();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return e.toString();
        } catch (JRException e) {
            e.printStackTrace();
            return e.toString();
        }
        return "report generated in " + tmpPathOfGeneratedReports;        
    }
       

    @Override
    public String getCashFlowReport(Account acc, String format) {

        String tmpPathOfGeneratedReports;

        //1.Get Revenue (from Deal collection)        
        List<Deal> listDeal = contractDao.findByBusiness(acc);
        //Get ClientName, ServiceName and name of the deal creator
        List<Deal> newDealList = contractDao.getDealInfo(listDeal);

        //2.Get Expenses (from Expense Collection)
        List<Expenses> listExp = expenseService.findAllExpenses(acc);

        // Load file and compile it
        try {
            File file = ResourceUtils.getFile("classpath:cashflow_1.jrxml");            
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(listDeal);
            JRBeanCollectionDataSource dataSource1 = new JRBeanCollectionDataSource(listExp);
            
            //to hold report parameter
            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("CollectionBeanDataSource", dataSource); // CollectionBeanParam => jasper studio table data set 
            parameters.put("CollectionBeanDataSource1", dataSource1); // CollectionBeanParam => jasper studio table data set 


            //new
            //read jrxml and create jasper design object
            InputStream input = new FileInputStream(file);
            JasperDesign jasperDesign = JRXmlLoader.load(input);

            //new
            //compiling jrxml with help of jasper report class
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());

            //new 
            //call jasper engine to view report (Report Viewer)
            //JasperViewer.viewReport(jasperPrint);

            /* Wenzel */
            // Create company folder for the account to place reports
            File companyFloder = new File(setting.getDefReportPath() + "\\" + acc.getCompany());
            if (!companyFloder.exists()  && !companyFloder .isDirectory()) {
                companyFloder.mkdirs();
            }
            tmpPathOfGeneratedReports = companyFloder.getPath();

            if (format.equalsIgnoreCase("html")) {
                JasperExportManager.exportReportToHtmlFile(jasperPrint, tmpPathOfGeneratedReports + "\\CashFlow.html");
            }
            if (format.equalsIgnoreCase("pdf")) {
                JasperExportManager.exportReportToPdfFile(jasperPrint, tmpPathOfGeneratedReports + "\\CashFlow.pdf");
            }  
            if (format.equalsIgnoreCase("xls"))  {
                File destFile = new File(tmpPathOfGeneratedReports + "\\CashFlow.xls");
                JRXlsExporter exporter = new JRXlsExporter();
                exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(destFile));
                SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
                configuration.setOnePagePerSheet(true);
                exporter.setConfiguration(configuration);
                exporter.exportReport();
            }          
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return e.toString();
        } catch (JRException e) {
            e.printStackTrace();
            return e.toString();
        }
        return "report generated in " + tmpPathOfGeneratedReports;        
    }
    
}