package com.nikee.universal.dao.interfaces;

import java.util.List;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.leave.Leave;

public interface ILeave {

	public Leave addLeave(Account acc, Leave leave);

	public Leave findLeave(Leave leave);

	public boolean updateLeave(Leave leave);

	public List<Leave> findAllLeaves(String empId);

	public boolean grantLeave(Leave leave);

	
    
}