package com.nikee.universal.dao.interfaces;

import com.nikee.universal.entity.Account;

public interface IReport {

	public String getGstReport(Account acc, String format);

	public String getCashFlowReport(Account acc, String format);
    
}