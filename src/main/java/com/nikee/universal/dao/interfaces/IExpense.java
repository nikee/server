package com.nikee.universal.dao.interfaces;

import java.util.List;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.account.Expenses;

public interface IExpense {

	Expenses addExpense(Expenses expense);

	List<Expenses> findAllExpenses(Account acc);
    
}