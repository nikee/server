package com.nikee.universal.dao.interfaces;

import java.util.List;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.employee.Employee;

public interface IEmployee {

	public Employee registerNew(Account acc, Employee employee);

	public List<Employee> findEmployeeByCompanyId(String companyId);

	public boolean update(Employee emp);

	public List<Employee> findAllEmployees(Account acc);

	public boolean deleteEmployee(Employee employee);
    
}