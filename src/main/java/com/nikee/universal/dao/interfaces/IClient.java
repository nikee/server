package com.nikee.universal.dao.interfaces;

import java.time.ZonedDateTime;
import java.util.List;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.client.Action;
import com.nikee.universal.entity.client.Client;
import com.nikee.universal.entity.client.Note;

public interface IClient {

    public Client create(Client client);

    public Client read(String id);

    public List<Client> findByCompanyID(String id);

    public Client update(Client client);

    /**
     * Update firstName, lastName, email and phone of document identified by
     * client.id
     * 
     * @param client
     * @return if the operation is sussessful
     */
    public boolean updateBasic(Client client);

    public String getNewId();

    /**
     * 
     * @param client specifies the client instance we want to update
     * @param note   the changed note
     * @param id     the unique idenftier among client.note and client.action
     * @return
     */
    public boolean updateNote(Client client, Note note);

    /**
     * Add a new note to an existing client profile
     * 
     * @param client
     * @param newNote
     * @return
     */
    public boolean addNote(Client client, Note newNote);

    /**
     * remove a note from an existing client profile
     * 
     * @param client
     * @param id
     * @return
     */
    public boolean removeNote(Client client, String id);

    /**
     * add a new action to an existing client profile
     * 
     * @param client     which client
     * @param enroll     this action will be assigned to these people
     * @param deadline   deadline the action will meet
     * @param assignedBy Who created this action
     * @param desc       String description
     * @return if the request was successful
     */
    public boolean addAction(Client client, List<Account> enroll, ZonedDateTime deadline, Account assignedBy,
            String desc);

    /**
     * Tick the checkbox of one person upon one action. If the record of requested
     * person does not exist in the action, it will be created and assigned with
     * given value.
     * 
     * @param client which client
     * @param id     ActionID
     * @param who    account who is asking
     * @return
     */
    public boolean tickAction(Client client, String id, Account who);

    /**
     * untick the checkbox located by given clientID, actionID and person who is
     * requesting. If the record of requested person does not exist in the action,
     * it will be created and assigned with given value.
     * 
     * @param client
     * @param id
     * @param who
     * @return
     */
    public boolean unTickAction(Client client, String id, Account who);

    /**
     * update an action record. Will only modify the desc and deadline fields in
     * action
     * 
     * @param client
     * @param action the action field containing actionID to locate the record, and
     *               new data in desc & deadline field.
     * @return
     */
    public boolean updateAction(Client client, Action action);

    /**
     * Assign a new staff to one action of a specified client, having hes checkbox
     * set to false. Behave exactly the same way as unTickAction.
     * 
     * @param client   which client
     * @param actionId the actionID
     * @param who      who will be assigned
     * @return
     */
    public boolean assignStaffToAction(Client client, String actionId, Account who);


    //returns clients of a particular company
    public List<Client> getAllClients();

	public List<Client> getClientsByCompany(String companyID);

}