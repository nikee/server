package com.nikee.universal.dao.interfaces;

import java.util.List;

import com.nikee.universal.entity.Stock;

public interface IStock {
    public void createStock(Stock stock);

	public Stock findStock(Stock stock);

	public boolean updateStock(Stock stock);

	public List<Stock> findAllStock();

	public boolean expireStock(Stock stock);

	public boolean cleanStock(Stock stock);

}
