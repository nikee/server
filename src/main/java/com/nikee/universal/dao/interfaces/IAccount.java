package com.nikee.universal.dao.interfaces;

import java.util.List;

import com.mongodb.client.result.UpdateResult;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.account.CalendarEvent;
import com.nikee.universal.entity.account.Events;
import org.springframework.dao.DuplicateKeyException;

public interface IAccount {
    /*
     * DEFINITION Lv 1 : ROOT account, Nikee Lv 2 : Company Owner account, unique
     * for each business Lv 3 : Managers of one particular company Lv 4 : Normal
     * account, defined by owners to be used within owner business.
     * 
     * Company data are stored in owner documents
     */

    public Account createAccount(Account accountToCreate) throws DuplicateKeyException;

    public UpdateResult transferOwnership(Account from, Account to);

    public Account findById(String id);

    /**
     * return a list of accounts found with the given email
     */
    public List<Account> findAccountByEmail(String email);

    public Account findFirstAccountByEmail(String email);

    /**
     * 
     * @param email             the account to be changed
     * @param newPasswordHashed new hashed password\
     * @return
     */
    public UpdateResult changePassword(String email, String newPasswordHashed);

    @Deprecated
    /**
     * Sign out can be handled by controller alone, no need to invoke service or DAO
     * method.
     */
    public void signout(Account account);

    public Account updateAccount(Account account);

    public String createAccountTest(Account account);

    public List<Account> findByEmployer(Account boss);

    public boolean addCalenderEvent(CalendarEvent event, Account acc);

	public boolean updateCalendar(CalendarEvent updatedcalendarEvent);

    

    public List<Account> findByCompanyID(String companyID);

    /**
     * 
     * @return
     */
    public List<Account> getAll();
}