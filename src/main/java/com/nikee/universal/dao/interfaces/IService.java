package com.nikee.universal.dao.interfaces;

import java.util.List;

import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Services;
import com.nikee.universal.errors.TransactionFailureException;

public interface IService {

    public Services getInstance(String id);

    /**
     * find parent of the given service. the original service with parent kept in
     * parent field. The parent field will be null if it is a root element. Will
     * return null if given service does not exist.
     * 
     * @param service the original service with parent kept in parent field. The
     *                parent field will be null if it is a root element. Will return
     *                null if given service does not exist.
     * @return
     */
    public Services whosYourDaddy(String id);

    /**
     * Amend a service to the root of your company
     * 
     * @return amended service
     */
    public Services Amend(Services toCreate);

    /**
     * Amend a service the service parentService, of the company
     * 
     * @param toCreate
     * @param parentService
     * @return
     */
    public Services Amend(Services toCreate, String parentService);

    /**
     * fetch the entire service tree of a company, return null if the owner Account
     * is not a t2 user. return an empty list if the owner is t2 but there is no
     * service in this company.
     * 
     * @param owner
     * @return
     */
    public List<Services> findAllByOwner(Account owner);

    /**
     * search for services of a company with a particular name
     */
    public List<Services> findByName(Account owner, String name);

    /**
     * remove a service from the tree, amending its direct child elements to its
     * parent(Set the child root if there is no parent)
     * 
     * @param toBeCut
     * @return
     */
    public void cut(Services toBeCut) throws TransactionFailureException;

    /**
     * remove a node and all its child nodes
     * 
     * @param toBeDeleted
     */
    public void delete(Services toBeDeleted) throws TransactionFailureException;

    /**
     * Service original is a child of A (A -> original).
     * insert(original,toBeInserted) will set toBeInserted as child of A, and
     * original will be a child of toBeInserted. (A -> toBeInserted -> original)
     * 
     * @param original
     * @param toBeInserted
     */
    public void insert(Services original, Services toBeInserted);

    /**
     * will not modify subService tree, ownedBy and ID
     * 
     * @param service
     * @return updated Service
     */
    public Services update(Services service);

    /**
     * Set the service as root service.
     */
    public void setRoot(Services service);

}