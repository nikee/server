package com.nikee.universal.dao.interfaces;

import java.util.List;

import com.mongodb.client.result.DeleteResult;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.contract.Deal;

public interface IContract {

    public Deal create(Deal deal);

    public Deal findById(String id);

    public List<Deal> findByClient(String clientId);

    public List<Deal> findByService(String serviceId);

    public List<Deal> findByBusiness(String ownerId);

    public List<Deal> findByBusiness(Account owner);

    public Deal save(Deal deal);

    public DeleteResult remove(Deal deal);

	public List<Deal> getDealInfo(List<Deal> listDeal);

}