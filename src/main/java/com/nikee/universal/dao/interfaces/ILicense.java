package com.nikee.universal.dao.interfaces;

import com.nikee.universal.entity.license.License;

public interface ILicense {

    public License findByID(String id);

    public License findByCompany(String companyID);

    public License save(License license);
}