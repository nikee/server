package com.nikee.universal.dao.interfaces;

import java.util.List;

import com.nikee.universal.entity.Attendance;

public interface IAttendance {

    public void create(Attendance attendance);

	public boolean delete(Attendance attendance);

	public boolean update(Attendance attendance);

	public Attendance find(Attendance attendance);

    public List<Attendance> findAll();
    
}
