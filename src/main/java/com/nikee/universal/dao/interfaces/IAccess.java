package com.nikee.universal.dao.interfaces;

import java.util.List;

import com.nikee.universal.entity.Access;

public interface IAccess {
    public void create(Access access);

	public Access find(Access access);

	public boolean update(Access access);

	public List<Access> findAll();

	public boolean delete(Access access);

}
