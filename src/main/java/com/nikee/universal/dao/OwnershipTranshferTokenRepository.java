package com.nikee.universal.dao;

import com.nikee.universal.entity.OwnershipTransferToken;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OwnershipTranshferTokenRepository extends MongoRepository<OwnershipTransferToken, ObjectId> {

    OwnershipTransferToken findByToken(String token);

}