package com.nikee.universal.structure;

import java.util.ArrayList;
import java.util.List;

public class Response<T> {
    private int status_code;
    private String message;
    private List<T> payload;
    private Exception exception;

    public Response() {

    }

    /**
     * @param status_code
     * @param message
     * @param payload
     */
    public Response(int status_code, String message, List<T> payload) {
        this.status_code = status_code;
        this.message = message;
        this.payload = payload;
    }

    public Response(int status_code, String message, T payload) {
        this.status_code = status_code;
        this.message = message;
        this.payload = new ArrayList<T>();
        this.payload.add(payload);
    }

    /**
     * @param status_code
     * @param message
     */
    public Response(int status_code, String message) {
        this.status_code = status_code;
        this.message = message;
    }

    /**
     * @return the status_code
     */
    public int getStatus_code() {
        return status_code;
    }

    /**
     * @param status_code the status_code to set
     */
    public void setStatus_code(int status_code) {
        this.status_code = status_code;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the payload
     */
    public List<T> getPayload() {
        return payload;
    }

    /**
     * @param payload the payload to set
     */
    public void setPayload(List<T> payload) {
        this.payload = payload;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((message == null) ? 0 : message.hashCode());
        result = prime * result + ((payload == null) ? 0 : payload.hashCode());
        result = prime * result + status_code;
        return result;
    }

    /**
     * @param status_code
     * @param payload
     */
    public Response(int status_code, List<T> payload) {
        this.status_code = status_code;
        this.payload = payload;
    }

    /**
     * @return the exception
     */
    public Exception getException() {
        return exception;
    }

    /**
     * @param exception the exception to set
     */
    public void setException(Exception exception) {
        this.exception = exception;
    }

    /**
     * @param status_code
     * @param message
     * @param exception
     */
    public Response(int status_code, String message, Exception exception) {
        this.status_code = status_code;
        this.message = message;
        this.exception = exception;
    }

    /**
     * @param status_code
     * @param message
     * @param payload
     * @param exception
     */
    public Response(int status_code, String message, List<T> payload, Exception exception) {
        this.status_code = status_code;
        this.message = message;
        this.payload = payload;
        this.exception = exception;
    }

    /**
     * @param status_code
     * @param payload
     * @param exception
     */
    public Response(int status_code, List<T> payload, Exception exception) {
        this.status_code = status_code;
        this.payload = payload;
        this.exception = exception;
    }
}