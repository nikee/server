package com.nikee.universal.dao;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nikee.universal.dao.interfaces.IAccount;
import com.nikee.universal.dao.interfaces.IClient;
import com.nikee.universal.dao.interfaces.IService;
import com.nikee.universal.entity.Account;
import com.nikee.universal.entity.Company;
import com.nikee.universal.entity.Mail;
import com.nikee.universal.entity.Services;
import com.nikee.universal.entity.client.Action;
import com.nikee.universal.entity.client.Client;
import com.nikee.universal.entity.client.Note;
import com.nikee.universal.service.EmailService;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.transaction.annotation.Transactional;

// @DataMongoTest
//(excludeAutoConfiguration = EmbeddedMongoAutoConfiguration.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
// @RunWith(SpringRunner.class)
@EnableMongoRepositories(considerNestedRepositories = true)
public class ServiceImplTests {

    // mongod --port 27017 --dbpath "C:\Program Files\MongoDB\Server\4.2\data"
    // --replSet rs0 --bind_ip localhost
    // https://www.c-sharpcorner.com/article/simple-steps-to-create-a-multi-node-mongodb-cluster/

    // cd C:\Program Files\MongoDB\Server\4.2\bin
    // mongo "mongodb://localhost:27017/?replicaSet=rs0"

    // mongod --replSet rs0 --dbpath="C:\data\db1"
    // mongod --replSet rs0 --dbpath="C:\data\db2" --port 27027
    // mongod --replSet rs0 --dbpath="C:\data\db3" --port 27037

    // @Configuration
    // static class Config extends AbstractMongoClientConfiguration {

    // @Value("${spring.data.mongodb.database}")
    // private String dbName;

    // @Value("${spring.data.mongodb.host}")
    // private String dbHost;

    // @Value("${spring.data.mongodb.port}")
    // private int dbPort;

    // @Bean
    // MongoTransactionManager transactionManager(MongoDbFactory dbFactory) {
    // return new MongoTransactionManager(dbFactory);
    // }

    // @Bean
    // public MongoClient mongoClient() {
    // return MongoClients.create(dbHost + dbPort);
    // // return new MongoClient(dbHost, dbPort);
    // }

    // @Bean
    // public MongoDbFactory mongoDbFactory() {
    // return new SimpleMongoClientDbFactory(mongoClient(), dbName);
    // }

    // @Override
    // public String getDatabaseName() {
    // return dbName;
    // }

    // }
    @Autowired
    private IService serviceDAO;

    @Autowired
    private IAccount accountDAO;

    @Autowired
    private IClient clientDao;

    @Autowired
    private EmailService emailService;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @BeforeAll
    static void setup() {
        // "@BeforeAll - executes once before all test methods in this class"
    }

    @BeforeEach
    void init() {
        // @BeforeEach - executes before each test method in this class

    }

    /*
     * @Id private ObjectId id;
     * 
     * @DBRef private Account ownedBy; private float price;
     * 
     * @DBRef private Set<Services> subService;
     * 
     * @DBRef private Set<Account> staffs; private String name; private List<String>
     * locations;
     * 
     * @Transient private Services parent;
     */
    // @Test
    @Transactional
    public void createRoot() {
        Account boss = new Account();
        Company company = new Company();
        company.setOwnerName("Karl");
        boss.setEmail("test1@mail.com");
        boss.setPassword("password");
        boss.setLevel(2);
        boss.setCompany(company.getId());
        boss.setFirstName("Karl").setLastName("Franz").setMobile("123");
        Services s1 = new Services();
        Services s2 = new Services();
        String name = "test root service";
        s1.setName(name);
        s2.setName(name + " 2");
        Account acc = accountDAO.createAccount(boss);
        s1.setCompanyId(acc.getCompany());
        s2.setCompanyId(acc.getCompany());
        List<Services> ls = serviceDAO.findByName(acc, name);
        int count = ls.size();
        ls = serviceDAO.findAllByOwner(acc);
        int countByOwner = ls.size();
        serviceDAO.Amend(s1);
        serviceDAO.Amend(s2);
        ls = serviceDAO.findByName(acc, name);
        assertTrue(ls.size() - count == 1);
        ls = serviceDAO.findAllByOwner(acc);
        // logger.info("find by Owner: previous " + countByOwner + ", current " +
        // ls.size());
        assertTrue(ls.size() - countByOwner == 2);

    }

    // create a client and add a note
    // @Test
    public void testClient() {
        Account inCharge = this.accountDAO.findFirstAccountByEmail("karl.franz@empire.com");
        Client client = new Client();
        client.setFirstName("FN");
        client.setLastName("ln");
        client.setInCharge(inCharge.getId());
        // client.setBusinessOwnerId(inCharge.getId());
        Client result = this.clientDao.create(client);
        Note note = new Note(inCharge.getId(), "test Summary content");
        boolean r = this.clientDao.addNote(client, note);
        assertTrue(r);
        testClientAddAction(result);
        testClientTick(result);
        client = this.clientDao.read(client.getId());
        assertTrue(client.getNotes().size() == 1);
        updateNote(client);
        removeNote(client);
        updateAction(client);

    }

    // @Test
    public void assginStaff() {
        Client client = this.clientDao.read("5e5455b0ccf808388aa8f898");
        Account inCharge = this.accountDAO.findFirstAccountByEmail("karl.franz@empire.com");
        Account inCharge2 = this.accountDAO.findFirstAccountByEmail("t4n2@gmail.com");

        this.clientDao.assignStaffToAction(client, "5e5455b0ccf808388aa8f89a", inCharge);
        this.clientDao.assignStaffToAction(client, "5e5455b0ccf808388aa8f89a", inCharge2);
    }

    // @Test
    public void clientTest2() {
        Account inCharge = this.accountDAO.findFirstAccountByEmail("karl.franz@empire.com");
        Client client = this.clientDao.read("5e4e2bcf56b6731350d803be");
        Action action = client.getActions().get(0);
        action.setDeadline(action.getDeadline().plusDays(10));
        action.setDescription("new Desc");
        this.clientDao.updateAction(client, action);
    }

    // @Test
    // add action
    public void testClientAddAction(Client client) {
        Account inCharge = this.accountDAO.findFirstAccountByEmail("karl.franz@empire.com");
        Account dasVadar = this.accountDAO.findFirstAccountByEmail("das.vadar@sw.com");
        List<Account> list = new ArrayList<>();
        list.add(inCharge);
        list.add(dasVadar);
        ZonedDateTime time = ZonedDateTime.of(2020, 2, 25, 0, 0, 0, 0, ZoneId.systemDefault());
        this.clientDao.addAction(client, list, time, inCharge, "Rat found in city.");
    }

    // @Test
    public void testClientTick(Client client) {
        client = this.clientDao.read(client.getId());
        Account inCharge = this.accountDAO.findFirstAccountByEmail("karl.franz@empire.com");
        this.clientDao.tickAction(client, client.getActions().get(0).getId(), inCharge);
    }

    public void updateNote(Client client) {
        Note note = client.getNotes().get(0);
        note.setSummary("Summary changed");
        assertTrue(this.clientDao.updateNote(client, note));
        client = refresh(client);
        assertTrue(client.getNotes().get(0).getSummary().equals("Summary changed"));

    }

    public void removeNote(Client client) {
        assertTrue(this.clientDao.removeNote(client, client.getNotes().get(0).getId()));
        refresh(client);
        // assertTrue(client.getNotes().size() == 0);
    }

    public void updateAction(Client client) {
        Action action = client.getActions().get(0);
        ZoneId zid = ZoneId.of("Australia/Melbourne");
        action.setDeadline(ZonedDateTime.of(2021, 3, 1, 0, 0, 0, 0, zid));
        action.setDescription("new desc");
        action.setCheckList(null);
        this.clientDao.updateAction(client, action);

    }

    private Client refresh(Client client) {
        return this.clientDao.read(client.getId());
    }

    //@Test
    public void sendEmail() {
        Mail mail = new Mail();
        mail.setFrom("no-reply@nikeeworld.com");
        mail.setTo("zaixiawuming_90@126.com");
        String name = "Test Business Name";
        mail.setSubject("Invitation");
        Map<String, Object> model = new HashMap<>();
        model.put("token", "123");
        model.put("signature", "https://nikeeworld.com");
        // String url = this.HttpServletRequest.getScheme() + "://" +
        // this.HttpServletRequest.getServerName() + ":"
        // + this.HttpServletRequest.getServerPort();
        String url = "http://localhost:8000/#!/reset";
        model.put("resetUrl", url + "?token=");
        mail.setModel(model);
        emailService.sendEmail(mail, EmailService.INVITATION);
    }
}